package com.estsoft.memosquare;

import com.estsoft.memosquare.fragments.WebBrowserMemoListFragment;
import com.estsoft.memosquare.listeners.GetMemoListListener;
import com.estsoft.memosquare.listeners.WebBrowserMemoListFragmentEventListener;
import com.estsoft.memosquare.mvp.models.WebBrowserMemoListModel;
import com.estsoft.memosquare.mvp.models.WebBrowserMemoListModelImpl;
import com.estsoft.memosquare.mvp.presenters.WebBrowserMemoListPresenter;
import com.estsoft.memosquare.mvp.presenters.WebBrowserMemoListPresenterImpl;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class WebBrowserMemoListPresenterTest {

    @Mock
    private static WebBrowserMemoListFragment sWebBrowserMemoListFragment;
    @Mock
    private static WebBrowserMemoListFragmentEventListener sWebBrowserMemoListFragmentEventListener;
    @Mock
    private static WebBrowserMemoListModel sWebBrowserMemoListModel;

    private WebBrowserMemoListPresenter mWebBrowserMemoListPresenter;

    @BeforeClass
    public static void beforeClass() throws Exception {
        sWebBrowserMemoListFragment = new WebBrowserMemoListFragment();
        sWebBrowserMemoListFragment.setWebBrowserMemoListFragmentEventListener(
                sWebBrowserMemoListFragmentEventListener);
        sWebBrowserMemoListModel = new WebBrowserMemoListModelImpl();
    }

    @Before
    public void setUp() throws Exception {
        mWebBrowserMemoListPresenter = new WebBrowserMemoListPresenterImpl(
                sWebBrowserMemoListFragment, sWebBrowserMemoListModel);
    }

    @AfterClass
    public static void afterClass() throws Exception {
        sWebBrowserMemoListFragment = null;
        sWebBrowserMemoListFragmentEventListener = null;
    }

    @After
    public void tearDown() throws Exception {
        mWebBrowserMemoListPresenter = null;
    }

    @Test
    public void onCreateView() throws Exception {
        mWebBrowserMemoListPresenter.onCreateView();

        verify(sWebBrowserMemoListFragment, times(1)).generateAdapterAndSetToRecView(any(ArrayList.class));
    }

    @Test
    public void initializeOffset() throws Exception {
        mWebBrowserMemoListPresenter.initializeOffset();
    }

    @Test
    public void onDestroy() throws Exception {
        mWebBrowserMemoListPresenter.onDestroy();
    }

    @Test
    public void loadPageMemos() {
        mWebBrowserMemoListPresenter.loadPageMemos();

        verify(sWebBrowserMemoListModel, times(1)).getPageMemos(any(Integer.class), any(String.class), any(WebBrowserMemoListPresenterImpl.class));
        verify(sWebBrowserMemoListFragment, times(1)).getUrl();
    }

    @Test
    public void clearMemoList() throws Exception {
        mWebBrowserMemoListPresenter.clearMemoList();
    }

    @Test
    public void setButtonGone() throws Exception {
        mWebBrowserMemoListPresenter.setUpButtonVisibility(true);
    }

    @Test
    public void setButtonVisible() throws Exception {
        mWebBrowserMemoListPresenter.setUpButtonVisibility(false);
    }


}
