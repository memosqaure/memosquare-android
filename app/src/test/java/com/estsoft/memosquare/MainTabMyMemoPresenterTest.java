package com.estsoft.memosquare;


import com.estsoft.memosquare.mvp.models.MainTabMyMemoModel;
import com.estsoft.memosquare.mvp.presenters.MainTabMyMemoPresenterImpl;
import com.estsoft.memosquare.mvp.views.MainTabMyMemoView;

import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.statements.Fail;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class MainTabMyMemoPresenterTest {

    @Mock
    MainTabMyMemoView view;

    private MainTabMyMemoPresenterImpl presenter;

    @Before
    public void setUp() throws Exception {
        presenter = new MainTabMyMemoPresenterImpl(view);
    }

    @Test
    public void onCreateView(){}

    @Test
    public void initializeOffset(){}

    @Test
    public void testIfViewReleaseOnDestroy(){
        fail();
    }

    @Test
    public void removeMymemo(int index){}

    @Test
    public void loadMyMemos(){}

    @Test
    public void setUpButtonVisibility(boolean isTop){}


}


