// 내가 만들어본 자바스크립트

// br -> p 태그로 변환
$('div br').each ( function () {
    if (this.parentNode.nodeName != "P") {
        $.each ([this.previousSibling, this.nextSibling], function () {
            if (this.nodeType === 3) { // 3 == text
                $(this).wrap ('<p></p>');
            }
        } );

        $(this).remove (); //-- Kill the <br>
    }
} );

var isCliping = false;

var highlight = function(event) {

    console.log("content clicked");

    if (!isCliping) {
        console.log("in view mode");
        return;
    } else {
        console.log("in clip mode");
    }

    if ($(this).data("disabled")) {
        console.log("from a tag");
        event.preventDefault();
        event.stopPropagation();
    }

    var target = $(event.target);
    var text = target.html();

    target.css("backgroundColor", "rgb(225, 225, 119)");

    window.INTERFACE.getHighlightedContent(text);
}

var copyImg = function(event) {
    var img = $(event.target);
    var url = img[0].src;
    window.INTERFACE.getImgTag(url);
}

var memosquareCliping = function() {
    isCliping = !isCliping;
    console.log("isCliping changed to " + isCliping);
//    window.INTERFACE.getPageHtml(document.documentElement.outerHTML);

    // 하이라이팅 모드
    if (isCliping) {
        // anchor 태그 비활성화
        $('a').data("disabled", true);

    }
    // 일반 모드
    else {
        $('a').data("disabled", false);
    }
}

// 이벤트 할당
$('a, p, li, h1, h2, h3, h4, h5, h6').bind("click",highlight);
$('body').on('click','img',copyImg);
