package com.estsoft.memosquare.vo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by hokyung on 2017. 1. 17..
 */

public class CategoryListWrapper<model> {

    @SerializedName("category_list")
    private ArrayList<model> mCategoryList;

    public ArrayList<model> getCategoryList() {
        return mCategoryList;
    }

    public void setCategoryList(ArrayList<model> categoryList) {
        mCategoryList = categoryList;
    }

    @Override
    public String toString() {
        return "CategoryListWrapper{" +
                "mCategoryList=" + mCategoryList +
                '}';
    }
}
