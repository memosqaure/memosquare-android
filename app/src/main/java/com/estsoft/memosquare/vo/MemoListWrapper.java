package com.estsoft.memosquare.vo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sun on 2016-10-27.
 * API response: count, next, previous, results로 이루어짐.
 * 그 중 results가 우리가 원하는 model list.
 * results만 꺼내기 위한 class
 */

public class MemoListWrapper<model> {

    @SerializedName("prev")
    private String prev;

    @SerializedName("next")
    private String next;

    @SerializedName("memo_list")
    private ArrayList<model> modellist;

    //Getter&Setter
    public String getPrev() {
        return prev;
    }

    public void setPrev(String prev) {
        this.prev = prev;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public ArrayList<model> getModellist() {
        return modellist;
    }

    public void setModellist(ArrayList<model> modellist) {
        this.modellist = modellist;
    }

    @Override
    public String toString() {
        return "MemoListWrapper{" +
                "prev='" + prev + '\'' +
                ", next='" + next + '\'' +
                ", modellist=" + modellist +
                '}';
    }
}
