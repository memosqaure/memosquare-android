package com.estsoft.memosquare.vo;

import java.io.Serializable;

/**
 * Created by sun on 2016-10-25.
 */

public class MemoVo implements Serializable {
    private int pk;
    private String category_name;
    private String title;
    private String content;
    private String owner;
    private String page;
    private boolean is_liked;
    private int num_likes;
    private int num_comments;
    private boolean is_clipped;
    private int num_clips;
    private boolean is_private;
    private String timestamp;
    private String owner_pic_url;
    private boolean is_mymemo;

    public MemoVo() {
        pk = num_likes = num_comments = num_clips = 0;
        category_name = title = content = owner = page = timestamp = owner_pic_url = "";
        is_liked = is_clipped = is_private = is_mymemo = false;
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public boolean is_like() {
        return is_liked;
    }

    public void setIs_liked(boolean is_liked) {
        this.is_liked = is_liked;
    }

    public int getNum_likes() {
        return num_likes;
    }

    public void setNum_likes(int num_likes) {
        this.num_likes = num_likes;
    }

    public int getNum_comments() {
        return num_comments;
    }

    public void setNum_comments(int num_comments) {
        this.num_comments = num_comments;
    }

    public boolean is_clipped() {
        return is_clipped;
    }

    public void setIs_clipped(boolean is_clipped) {
        this.is_clipped = is_clipped;
    }

    public int getNum_clips() {
        return num_clips;
    }

    public void setNum_clips(int num_clips) {
        this.num_clips = num_clips;
    }

    public boolean is_private() {
        return is_private;
    }

    public void setIs_private(boolean is_private) {
        this.is_private = is_private;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getOwner_pic_url() {
        return owner_pic_url;
    }

    public void setOwner_pic_url(String owner_pic_url) {
        this.owner_pic_url = owner_pic_url;
    }

    public boolean is_mymemo() {
        return is_mymemo;
    }

    public void setIs_mymemo(boolean is_mymemo) {
        this.is_mymemo = is_mymemo;
    }

    @Override
    public String toString() {
        return "MemoVo{" +
                "pk=" + pk +
                ", category_name='" + category_name + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", owner='" + owner + '\'' +
                ", page='" + page + '\'' +
                ", is_liked=" + is_liked +
                ", num_likes=" + num_likes +
                ", num_comments=" + num_comments +
                ", is_clipped=" + is_clipped +
                ", num_clips=" + num_clips +
                ", is_private=" + is_private +
                ", timestamp='" + timestamp + '\'' +
                ", owner_pic_url='" + owner_pic_url + '\'' +
                ", is_mymemo=" + is_mymemo +
                '}';
    }
}
