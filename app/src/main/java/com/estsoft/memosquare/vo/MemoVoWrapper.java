package com.estsoft.memosquare.vo;

import com.google.gson.annotations.SerializedName;

public class MemoVoWrapper<MemoVo> {

    @SerializedName("memo")
    private MemoVo memoVo ;

    public MemoVo getMemoVo() {
        return memoVo;
    }
}
