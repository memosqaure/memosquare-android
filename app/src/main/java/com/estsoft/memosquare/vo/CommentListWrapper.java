package com.estsoft.memosquare.vo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CommentListWrapper<model> {

    @SerializedName("comment_list")
    private ArrayList<model> mCommentList;

    public ArrayList<model> getCommentList() {
        return mCommentList;
    }

    @Override
    public String toString() {
        return "CommentListWrapper{" +
                "mCommentList=" + mCommentList +
                '}';
    }
}
