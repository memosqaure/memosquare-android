package com.estsoft.memosquare.vo;

/**
 * Created by hokyung on 2016. 11. 30..
 */

public class BookmarkVo {
    private String mId;
    private String mName;
    private String mUrl;

    @Override
    public String toString() {
        return "BookmarkVo{" +
                "mId='" + mId + '\'' +
                ", mName='" + mName + '\'' +
                ", mUrl='" + mUrl + '\'' +
                '}';
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }
}
