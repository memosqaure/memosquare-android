package com.estsoft.memosquare.vo;

/**
 * Created by hokyung on 2016. 11. 22..
 * 주소창 내용 보관용 vo
 */

public class UrlVo {
    private String mId;
    private String mTitle;
    private String mUrl;
    private int mCount;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public int getCount() {
        return mCount;
    }

    public void setCount(int count) {
        mCount = count;
    }

    @Override
    public String toString() {
        return "UrlVo{" +
                "mId='" + mId + '\'' +
                ", mTitle='" + mTitle + '\'' +
                ", mUrl='" + mUrl + '\'' +
                ", mCount=" + mCount +
                '}';
    }
}
