package com.estsoft.memosquare.vo;

import java.io.Serializable;

public class CommentVo implements Serializable{

    private int pk;
    private int memo;
    private String owner;
    private String content;
    private String timestamp;
    private int num_likes;
    private String owner_pic_url;
    private boolean is_owner;
    private boolean is_liked;

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public int getMemo() {
        return memo;
    }

    public void setMemo(int memo) {
        this.memo = memo;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getNum_likes() {
        return num_likes;
    }

    public void setNum_likes(int num_likes) {
        this.num_likes = num_likes;
    }

    public String getOwner_pic_url() {
        return owner_pic_url;
    }

    public void setOwner_pic_url(String owner_pic_url) {
        this.owner_pic_url = owner_pic_url;
    }

    public boolean is_owner() {
        return is_owner;
    }

    public void setIs_owner(boolean is_owner) {
        this.is_owner = is_owner;
    }

    @Override
    public String toString() {
        return "CommentVo{" +
                "pk=" + pk +
                ", memo=" + memo +
                ", owner='" + owner + '\'' +
                ", content='" + content + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", num_likes=" + num_likes +
                ", owner_pic_url='" + owner_pic_url + '\'' +
                ", is_owner=" + is_owner +
                '}';
    }
}
