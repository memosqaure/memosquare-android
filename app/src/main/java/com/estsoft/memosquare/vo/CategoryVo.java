package com.estsoft.memosquare.vo;


import java.io.Serializable;

public class CategoryVo implements Serializable{
    private int pk;
    private String name;

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CategoryVo{" +
                "pk=" + pk +
                ", name='" + name + '\'' +
                '}';
    }
}
