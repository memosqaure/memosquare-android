package com.estsoft.memosquare.enums;

public enum MemoListType {
    MY,
    CLIPPED,
    PAGE
}
