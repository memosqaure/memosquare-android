package com.estsoft.memosquare.enums;

public enum MemoAction {
    DELETE,
    CLIP,
    NOTCLIP,
    LOCKTOGGLE,
    LIKE,
    NOTLIKE
}
