package com.estsoft.memosquare;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import timber.log.Timber;



public class MemoSquare extends Application {
    private static MemoSquare instance;
    private Tracker mTracker;


    //Google Analytics
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            //debug일 때는 google analytics안 찍힌다.
            analytics.setDryRun(!BuildConfig.DEBUG);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }


    //Timber
    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            // Release 버전에서의 로그 트리
            Timber.plant(new ReleaseTree());
        }
    }

    // 상용 버전에서의 로그
    private static class ReleaseTree extends Timber.Tree {

        @Override
        protected void log(int priority, String tag, String message, Throwable t) {

            // 디버깅용 로그는 나타내지 않는다
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return;
            }

            // INFO 로그의 경우
            Log.i(tag, message);

            // ERROR와 WARNING
            if (t != null) {
                if (priority == Log.ERROR) {
                    Log.e(tag, message, t);
                } else if (priority == Log.WARN) {
                    Log.w(tag, message, t);
                }
            }
        }
    }

    //retreive the context from anywhere(i.e. interceptor)
    public static Context getContext(){
        return instance;
    }

}
