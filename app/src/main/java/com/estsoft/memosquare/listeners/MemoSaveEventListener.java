package com.estsoft.memosquare.listeners;

import com.estsoft.memosquare.vo.MemoVo;

public interface MemoSaveEventListener {
    void onMemoSaveSuccess(MemoVo mMemoVo);
    void onMemoSaveFail();
}
