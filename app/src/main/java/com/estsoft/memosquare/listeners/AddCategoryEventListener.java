package com.estsoft.memosquare.listeners;

/**
 * Created by hokyung on 2016. 12. 20..
 */

public interface AddCategoryEventListener {
    void add(String name);
}
