package com.estsoft.memosquare.listeners;

/**
 * Created by hokyung on 2017. 1. 2..
 */

public interface SaveMemoButtonEventListener {
    void save(String category);
}
