package com.estsoft.memosquare.listeners;

/**
 * Created by hokyung on 2016. 11. 15..
 */

public interface WebViewFragmentEventListener {
    String getUrl();
    void setHasMemo(boolean hasMemo);
    boolean getHasMemo();
    void changeUrl(String url);
    void setHighlightAvailable(boolean available);
    void clearMemoList();
    void loadMemoList();
    void addHighlightedContents(String content);
    void addImage(String url);
    void changeModeToWebBrowsing();
    void initializeOffset();
}
