package com.estsoft.memosquare.listeners;

import com.estsoft.memosquare.vo.MemoVo;

import java.util.ArrayList;


public interface GetMemoListListener {
    void onGetMemoListSuccess(ArrayList<MemoVo> mMemoList, int offset);
    void onGetMemoListFail();
}
