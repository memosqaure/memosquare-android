package com.estsoft.memosquare.listeners;

/**
 * Created by sun on 2016-11-24.
 */

public interface SendSuggestionEventListener {
    void onSendSuggestionSuccess();
    void onSendSuggestionFail();
}
