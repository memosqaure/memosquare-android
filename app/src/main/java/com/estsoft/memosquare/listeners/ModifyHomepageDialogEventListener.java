package com.estsoft.memosquare.listeners;

/**
 * Created by hokyung on 2016. 11. 30..
 */

public interface ModifyHomepageDialogEventListener {
    void useBasicHomepage();
    void useCurrentHomepage();
}
