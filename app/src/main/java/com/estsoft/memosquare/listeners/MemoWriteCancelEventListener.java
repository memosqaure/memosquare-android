package com.estsoft.memosquare.listeners;

/**
 * Created by hokyung on 2016. 11. 23..
 */

public interface MemoWriteCancelEventListener {
    void cancel();
}
