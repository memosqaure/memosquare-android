package com.estsoft.memosquare.listeners;

/**
 * Created by sun on 2016-11-11.
 */

public interface SignInServerListener {
    void onSignInServerSuccess();
    void onSignInServerFail();
}
