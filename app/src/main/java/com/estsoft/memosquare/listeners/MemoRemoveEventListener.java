package com.estsoft.memosquare.listeners;

/**
 * Created by hokyung on 2016. 11. 14..
 */

public interface MemoRemoveEventListener {
    public void remove();
}
