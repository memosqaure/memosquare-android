package com.estsoft.memosquare.listeners;

/**
 * Created by hokyung on 2016. 11. 30..
 */

public interface WebBrowserOptionClickEventListener {
    void goForward();
    void addBookmark();
    void removeBookmark();
    void refresh();
    void showBookmarks();
    void modifyHomepage();
    String getUrl();
}
