package com.estsoft.memosquare.listeners;

import android.text.Spanned;

/**
 * Created by hokyung on 2017. 1. 9..
 */

public interface GetImageSpanCallbackListener {
    void onGetSpanSuccess(Spanned spanned);
}
