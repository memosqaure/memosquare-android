package com.estsoft.memosquare.listeners;

/**
 * Created by hokyung on 2016. 11. 24..
 */

public interface MainTabClipbookFragmentEventListener {
    void setUpButtonVisibility(int visibility);
}
