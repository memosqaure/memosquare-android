package com.estsoft.memosquare.listeners;

import com.estsoft.memosquare.vo.CommentVo;

public interface AddCommentListener {
    void onSuccess(CommentVo comment);
    void onFailed();
}
