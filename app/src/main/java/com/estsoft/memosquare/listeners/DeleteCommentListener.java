package com.estsoft.memosquare.listeners;

public interface DeleteCommentListener {
    void onSuccess();
    void onFailed();
}
