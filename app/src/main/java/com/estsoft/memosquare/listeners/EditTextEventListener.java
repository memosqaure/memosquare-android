package com.estsoft.memosquare.listeners;

/**
 * Created by hokyung on 2016. 11. 15..
 */

public interface EditTextEventListener {
    void goUrl();
    void clearEditTextFocusAndKeyboard();
    void deleteCharacter();
}
