package com.estsoft.memosquare.listeners;

import com.estsoft.memosquare.vo.CommentVo;

public interface UpdateCommentListener {
    void onSuccess(CommentVo comment);
    void onFailed();
}
