package com.estsoft.memosquare.listeners;

import com.estsoft.memosquare.vo.MemoVo;

/**
 * Created by hokyung on 2016. 11. 15..
 */

public interface WebBrowserMemoWriteFragmentEventListener {
    boolean isHighlightAvailable();
    void toggleHighlight();
    String getUrl();
    void changeModeToWebBrowsing();
    void clearEditTextFocusAndKeyboard();
    void setMemoVo(MemoVo memoVo);
}
