package com.estsoft.memosquare.listeners;

import com.estsoft.memosquare.vo.CommentVo;

import java.util.ArrayList;

public interface GetCommentsListener {
    void onSuccess(ArrayList<CommentVo> commentList);
    void onFailed();
}
