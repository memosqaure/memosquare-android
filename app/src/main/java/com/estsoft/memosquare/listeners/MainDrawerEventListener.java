package com.estsoft.memosquare.listeners;

/**
 * Created by hokyung on 2016. 12. 19..
 */

public interface MainDrawerEventListener {
    void closeDrawer();
    void startSuggestion();
    void signOut();
}
