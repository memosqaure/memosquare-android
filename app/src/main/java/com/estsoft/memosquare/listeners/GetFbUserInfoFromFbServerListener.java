package com.estsoft.memosquare.listeners;

import org.json.JSONObject;

/**
 * Created by sun on 2016-11-28.
 */

public interface GetFbUserInfoFromFbServerListener {
    void onGetFbUserInfoFromFbServerSuccess(JSONObject object);
    void onGetFbUserInfoFromFbServerFail();
}
