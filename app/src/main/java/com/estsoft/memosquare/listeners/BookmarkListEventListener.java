package com.estsoft.memosquare.listeners;

import com.estsoft.memosquare.vo.BookmarkVo;

/**
 * Created by hokyung on 2016. 11. 30..
 */

public interface BookmarkListEventListener {
    void onClickBookmark(BookmarkVo bookmarkVo);
}
