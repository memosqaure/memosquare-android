package com.estsoft.memosquare.listeners;

import com.estsoft.memosquare.vo.MemoVo;

/**
 * Created by hokyung on 2016. 11. 15..
 */

public interface WebBrowserMemoListFragmentEventListener {
    String getUrl();
    void setUpButtonVisibility(int visibility);
    boolean isInModeChanging();
}
