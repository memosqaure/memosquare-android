package com.estsoft.memosquare.listeners;

/**
 * Created by hokyung on 2017. 1. 5..
 */

public interface CategoryConfigEventListener {
    String getCategoryName();

    void deleteCategory();

    void showNameIsEmptyToast();

    void modifyCategory(String name);
}
