package com.estsoft.memosquare.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.estsoft.memosquare.R;
import com.estsoft.memosquare.database.Repository.BookmarkRepository;
import com.estsoft.memosquare.listeners.WebBrowserOptionClickEventListener;
import com.estsoft.memosquare.vo.BookmarkVo;

import java.util.ArrayList;

/**
 * Created by hokyung on 2016. 11. 30..
 */

public class WebBrowserOptionMenuAdapter extends ArrayAdapter {

    // item 레이아웃
    private int mViewResource;

    // 위젯들
    private ImageButton mGoForward;
    private ImageButton mAddBookmark;
    private ImageButton mRefresh;
    private TextView mBookMark;
    private TextView mModifyHome;

    // 북마크가 있는지 알려주는 변수
    private boolean mIsBookMarked;

    // 콜백 리스너
    private WebBrowserOptionClickEventListener mWebBrowserOptionClickEventListener;

    public WebBrowserOptionMenuAdapter(Context context, int resource) {
        super(context, resource, new ArrayList());

        mViewResource = resource;
    }

    public void setWebBrowserOptionClickEventListener(WebBrowserOptionClickEventListener webBrowserOptionClickEventListener) {
        mWebBrowserOptionClickEventListener = webBrowserOptionClickEventListener;
    }

    @Override
    public int getCount() {
        return 1;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(mViewResource, null);
        }

        // 북마크 버튼 기본상태
        mIsBookMarked = false;

        // 위젯 할당 및 리스너 달기
        mGoForward = (ImageButton) convertView.findViewById(R.id.go_forward);
        mGoForward.setOnClickListener(mClickListener);

        mAddBookmark = (ImageButton) convertView.findViewById(R.id.add_bookmark);
        BookmarkVo bookmarkVo = BookmarkRepository.getInstance(getContext())
                .getBookmark(mWebBrowserOptionClickEventListener.getUrl());
        if (bookmarkVo != null) {
            mAddBookmark.setImageResource(R.drawable.ic_star_fill);
            mIsBookMarked = true;
        }
        mAddBookmark.setOnClickListener(mClickListener);

        mRefresh = (ImageButton) convertView.findViewById(R.id.webbrowser_refresh);
        mRefresh.setOnClickListener(mClickListener);
        
        mBookMark = (TextView) convertView.findViewById(R.id.bookmark_btn);
        mBookMark.setOnClickListener(mClickListener);

        mModifyHome = (TextView) convertView.findViewById(R.id.modify_home_btn);
        mModifyHome.setOnClickListener(mClickListener);

        return convertView;
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // 뷰에따라 동장
            switch (v.getId()) {
                case R.id.go_forward:
                    mWebBrowserOptionClickEventListener.goForward();
                    break;
                case R.id.add_bookmark:
                    // 북마크가 있던 상태
                    if (mIsBookMarked) {
                        mAddBookmark.setImageResource(R.drawable.ic_star_empty);
                        mWebBrowserOptionClickEventListener.removeBookmark();
                        mIsBookMarked = false;
                    }
                    // 북마크가 없던 상태
                    else {
                        mAddBookmark.setImageResource(R.drawable.ic_star_fill);
                        mWebBrowserOptionClickEventListener.addBookmark();
                        mIsBookMarked = true;
                    }
                    break;
                case R.id.webbrowser_refresh:
                    mWebBrowserOptionClickEventListener.refresh();
                    break;
                case R.id.bookmark_btn:
                    mWebBrowserOptionClickEventListener.showBookmarks();
                    break;
                case R.id.modify_home_btn:
                    mWebBrowserOptionClickEventListener.modifyHomepage();
                    break;
            }
        }
    };
}
