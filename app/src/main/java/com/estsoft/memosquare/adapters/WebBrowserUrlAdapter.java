package com.estsoft.memosquare.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;
import android.widget.Toast;

import com.estsoft.memosquare.R;
import com.estsoft.memosquare.vo.UrlVo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import timber.log.Timber;

/**
 * Created by hokyung on 2016. 11. 22..
 */

public class WebBrowserUrlAdapter extends ArrayAdapter {

    private int mViewResource;
    private ArrayList<UrlVo> mItems;
    private ArrayList<UrlVo> mItemsAll;
    private ArrayList<UrlVo> mSuggestions;
    private ArrayList<UrlVo> mPreviousList;
    private Handler mHandler;

    public WebBrowserUrlAdapter(Context context, int resource, ArrayList objects) {
        super(context, resource, objects);

        mViewResource = resource;
        mItems = objects;
        mItemsAll = (ArrayList<UrlVo>) mItems.clone();
        mSuggestions = new ArrayList<>();
        mPreviousList = new ArrayList<>();
        mHandler = new Handler();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(mViewResource, null);
        }

        UrlVo vo = mItems.get(position);
        if (vo != null) {
            TextView title = (TextView) convertView.findViewById(R.id.site_title);
            TextView url = (TextView) convertView.findViewById(R.id.site_url);
            if (title != null) {
                title.setText(vo.getTitle());
            }
            if (url != null) {
                url.setText(vo.getUrl());
            }
        }

        return convertView;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return mUrlFilter;
    }

    private Filter mUrlFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return super.convertResultToString(resultValue);
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                mSuggestions.clear();
                for (UrlVo item : mItemsAll) {
                    if (item.getTitle().toLowerCase().contains(constraint.toString().toLowerCase())
                            || item.getUrl().toLowerCase().contains(constraint.toString()
                            .toLowerCase())) {
                        mSuggestions.add(item);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mSuggestions;
                filterResults.count = mSuggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.values == null) {
                return;
            }

            final ArrayList<UrlVo> filteredList = (ArrayList) results.values;
            Timber.d(String.valueOf(filteredList));

            if (mPreviousList.equals((ArrayList) filteredList)) {
                return;
            }
            mPreviousList.clear();
            mPreviousList.addAll(filteredList);
            if (results.count > 0) {
                mHandler.removeCallbacksAndMessages(null);
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        clear();
                        for (UrlVo item : filteredList) {
                            add(item);
                        }
                        notifyDataSetChanged();
                    }
                }, 40);
            }
        }
    };
}
