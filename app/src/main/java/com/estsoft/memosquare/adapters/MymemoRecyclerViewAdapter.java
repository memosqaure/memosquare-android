package com.estsoft.memosquare.adapters;


import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.estsoft.memosquare.MemoSquare;
import com.estsoft.memosquare.R;
import com.estsoft.memosquare.activities.MainActivity;
import com.estsoft.memosquare.activities.MemoViewActivity;
import com.estsoft.memosquare.oldmodels.MainModel;
import com.estsoft.memosquare.utils.PicassoImageGetter;
import com.estsoft.memosquare.vo.MemoVo;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;


import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class MymemoRecyclerViewAdapter extends RecyclerView.Adapter<MymemoRecyclerViewAdapter.MemoViewHolder> {

    private Context mContext;
    private ArrayList<MemoVo> mMemoList;

    private MainModel mModel;

    //Google Analytics Tracker
    Tracker mTracker;

    public MymemoRecyclerViewAdapter(Context context, ArrayList<MemoVo> memoList) {
        this.mContext = context;
        this.mMemoList = memoList;
        this.mModel = MainModel.getInstance();

        MemoSquare application = (MemoSquare) MemoSquare.getContext();
        this.mTracker = application.getDefaultTracker();
    }

    public void setMemoList(ArrayList<MemoVo> memoList) {
        this.mMemoList = memoList;
    }

    //connect cardview and viewholder
    @Override
    public MymemoRecyclerViewAdapter.MemoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mCardView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.cardview_mymemo, parent, false);
        return new MymemoRecyclerViewAdapter.MemoViewHolder(mCardView);
    }

    //connect model(data) and viewholder
    @Override
    public void onBindViewHolder(final MymemoRecyclerViewAdapter.MemoViewHolder memoViewHolder, int i) {
        final MemoVo memoVo = mMemoList.get(i);
        memoViewHolder.mTitle.setText(memoVo.getTitle());

        // content 넣기
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) mContext.
                getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);
        PicassoImageGetter picassoImageGetter = new PicassoImageGetter(memoViewHolder.mContent,
                mContext.getResources(), Picasso.with(mContext), metrics.widthPixels);
        Spanned htmlSpan = Html.fromHtml(memoVo.getContent(), picassoImageGetter, null);

        memoViewHolder.mContent.setText(htmlSpan);

        memoViewHolder.mTimestamp.setText(memoVo.getTimestamp());

        // 좋아요 눌러져있나
        if (memoVo.is_like()) {
            memoViewHolder.mLikeIcon.setImageDrawable(memoViewHolder.mLike);
        } else {
            memoViewHolder.mLikeIcon.setImageDrawable(memoViewHolder.mLikeBorder);
        }

        // 좋아요 갯수
        memoViewHolder.mLikeCount.setText("" + memoVo.getNum_likes());

        // 댓글 갯수
        memoViewHolder.mCommentCount.setText("" + memoVo.getNum_comments());

        //is_cliped, is_private
        if(memoVo.is_clipped()) {
            memoViewHolder.mClipButton.setImageDrawable(memoViewHolder.mClippedImg);
        }else{
            memoViewHolder.mClipButton.setImageDrawable(memoViewHolder.mNotClippedImg);
        }

        if(memoVo.is_private()) {
            memoViewHolder.mLockView.setImageDrawable(memoViewHolder.mLockedImg);
        }else{
            memoViewHolder.mLockView.setImageDrawable(memoViewHolder.mUnlockedImg);
        }

        final int index = i;

        // card view onclick listener
        memoViewHolder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Timber.d("my memo mCardView " + memoVo);

                goToMemoView(memoVo, index);

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction("mymemo-click cardview")
                        .build());
            }
        });
        
        memoViewHolder.mLikeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (memoVo.is_like()) {
                    memoViewHolder.mLikeIcon.setImageDrawable(memoViewHolder.mLikeBorder);
                    memoVo.setNum_likes(memoVo.getNum_likes() - 1);
                    mModel.notLikeMemo(memoVo);
                } else {
                    memoViewHolder.mLikeIcon.setImageDrawable(memoViewHolder.mLike);
                    memoVo.setNum_likes(memoVo.getNum_likes() + 1);
                    mModel.likeMemo(memoVo);
                }
                memoVo.setIs_liked(!memoVo.is_like());
                memoViewHolder.mLikeCount.setText("" + memoVo.getNum_likes());
            }
        });

        memoViewHolder.mCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Timber.d("my memo mCommentButton " + memoVo);

                goToMemoView(memoVo, index);

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction("mymemo-click comment")
                        .build());
            }
        });

        //mClipButton-onclick listener
        memoViewHolder.mClipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 클립 해제하기: is_cliped=true: not cliped
                if (memoVo.is_clipped()) {
                    // 이미지 변경
                    memoViewHolder.mClipButton.setImageDrawable(memoViewHolder.mNotClippedImg);
                    memoVo.setIs_clipped(false);
                    mModel.notClipMemo(memoVo);
                    Timber.d("107"+memoVo.toString());
                    // 클립 카운트 감소
                    memoVo.setNum_clips(memoVo.getNum_clips()-1);

                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("mymemo-not clip")
                            .build());
                }
                // 클립하기: is_cliped=flase: clip
                else {
                    // 이미지 변경
                    memoViewHolder.mClipButton.setImageDrawable(memoViewHolder.mClippedImg);
                    memoVo.setIs_clipped(true);
                    mModel.clipMemo(memoVo);
                    Timber.d("118"+memoVo.toString());
                    memoVo.setNum_clips(memoVo.getNum_clips()+1);

                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("mymemo-clip")
                            .build());
                }
            }
        });
    }

    private void goToMemoView(MemoVo memoVo, int index) {
        Intent intent = MemoViewActivity.getIntent(mContext);
        intent.putExtra("memo", memoVo);
        intent.putExtra("index", index);
        ((AppCompatActivity) mContext).startActivityForResult(intent, MainActivity.REQUEST_MEMOVIEW);
    }

    @Override
    public int getItemCount() {
        return mMemoList.size();
    }

    public static class MemoViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.card_view) CardView mCardView;
        @BindView(R.id.tv_title) TextView mTitle;
        @BindView(R.id.tv_content) HtmlTextView mContent;
        @BindView(R.id.like_btn) LinearLayout mLikeButton;
        @BindView(R.id.like_icon) ImageView mLikeIcon;
        @BindView(R.id.like_count) TextView mLikeCount;
        @BindView(R.id.comment_btn) LinearLayout mCommentButton;
        @BindView(R.id.comment_count) TextView mCommentCount;
        @BindView(R.id.iv_lock) ImageView mLockView;
        @BindView(R.id.tv_timestamp) TextView mTimestamp;
        @BindView(R.id.ib_clip) ImageButton mClipButton;

        @BindDrawable(R.drawable.ic_like) Drawable mLike;
        @BindDrawable(R.drawable.ic_like_border) Drawable mLikeBorder;
        @BindDrawable(R.drawable.ic_lock_close_memo_list) Drawable mLockedImg;
        @BindDrawable(R.drawable.ic_lock_open_memo_list) Drawable mUnlockedImg;
        @BindDrawable(R.drawable.ic_bookmark_border) Drawable mNotClippedImg;
        @BindDrawable(R.drawable.ic_bookmark_clipped) Drawable mClippedImg;

        public MemoViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    //swipeRefreshLayout
    // Clean all elements of the recycler
    public void clear() {
        if(mMemoList.size() > 0){
            mMemoList.clear();
        }
        notifyDataSetChanged();
    }
}
