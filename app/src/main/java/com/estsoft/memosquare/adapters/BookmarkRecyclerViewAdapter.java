package com.estsoft.memosquare.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.estsoft.memosquare.R;
import com.estsoft.memosquare.listeners.BookmarkListEventListener;
import com.estsoft.memosquare.vo.BookmarkVo;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hokyung on 2016. 11. 30..
 */

public class BookmarkRecyclerViewAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private ArrayList<BookmarkVo> mList;

    //  콜백 리스너
    private BookmarkListEventListener mBookmarkListEventListener;

    public BookmarkRecyclerViewAdapter(Context context, ArrayList<BookmarkVo> bookmarkList) {
        mContext = context;
        mList = bookmarkList;
    }

    public void setBookmarkListEventListener(BookmarkListEventListener bookmarkListEventListener) {
        mBookmarkListEventListener = bookmarkListEventListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_bookmark, parent, false);

        return new BookmarkViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final BookmarkVo bookmarkVo = mList.get(position);
        BookmarkViewHolder viewHolder = (BookmarkViewHolder) holder;

        viewHolder.mName.setText(bookmarkVo.getName());
        viewHolder.mUrl.setText(bookmarkVo.getUrl());
        viewHolder.mLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBookmarkListEventListener.onClickBookmark(bookmarkVo);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class BookmarkViewHolder extends RecyclerView.ViewHolder {

        // 위젯 할당
        @BindView(R.id.linearlayout) LinearLayout mLinearLayout;
        @BindView(R.id.bookmark_name) TextView mName;
        @BindView(R.id.bookmark_url) TextView mUrl;

        public BookmarkViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
