package com.estsoft.memosquare.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.estsoft.memosquare.R;
import com.estsoft.memosquare.activities.CategoryActivity;
import com.estsoft.memosquare.activities.CommentEditActivity;
import com.estsoft.memosquare.listeners.MainDrawerEventListener;
import com.estsoft.memosquare.mvp.models.MainDrawerModel;
import com.estsoft.memosquare.vo.CategoryVo;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class CategoryRecyclerViewAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private ArrayList<CategoryVo> mCategoryList;

    private MainDrawerEventListener mMainDrawerEventListener;

    public CategoryRecyclerViewAdapter(Context context, ArrayList<CategoryVo> categoryList) {
        mContext = context;
        mCategoryList = categoryList;
    }

    public void setCategoryList(ArrayList<CategoryVo> categoryList) {
        mCategoryList = categoryList;
    }

    public void setMainDrawerEventListener(MainDrawerEventListener mainDrawerEventListener) {
        mMainDrawerEventListener = mainDrawerEventListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_category, parent, false);
        return new CategoryVH(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final CategoryVo category = mCategoryList.get(position);
        CategoryVH categoryVH = (CategoryVH) holder;
        Timber.d("bind view holder " + category);

        categoryVH.mCategory.setText(category.getName());
        categoryVH.mCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMainDrawerEventListener.closeDrawer();
                Intent intent = CategoryActivity.getIntent(mContext);
                intent.putExtra("category", category);

                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCategoryList.size();
    }

    public void addCategory(CategoryVo categoryVo) {
        mCategoryList.add(categoryVo);
        notifyDataSetChanged();
    }

    public class CategoryVH extends RecyclerView.ViewHolder {
        @BindView(R.id.category) TextView mCategory;

        public CategoryVH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
