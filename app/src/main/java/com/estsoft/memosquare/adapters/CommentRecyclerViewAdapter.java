package com.estsoft.memosquare.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


import com.estsoft.memosquare.MemoSquare;
import com.estsoft.memosquare.R;
import com.estsoft.memosquare.activities.CategoryActivity;
import com.estsoft.memosquare.activities.CommentEditActivity;
import com.estsoft.memosquare.listeners.DeleteCommentListener;
import com.estsoft.memosquare.oldmodels.CommentModel;
import com.estsoft.memosquare.utils.CircleTransform;
import com.estsoft.memosquare.vo.CommentVo;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class CommentRecyclerViewAdapter extends RecyclerView.Adapter {

    static final int REQUEST_EDIT_COMMENT = 1;

    private Context mContext;
    private ArrayList<CommentVo> mCommentList;
    private NotifyCommentDeletedListener mListener;

    private CommentModel mModel;

    //Google Analytics Tracker
    Tracker mTracker;

    public CommentRecyclerViewAdapter(Context context, ArrayList<CommentVo> commentList, NotifyCommentDeletedListener listener){
        this.mContext = context;
        this.mCommentList = commentList;
        this.mModel = CommentModel.getInstance();
        this.mListener = listener;

        MemoSquare application = (MemoSquare) MemoSquare.getContext();
        this.mTracker = application.getDefaultTracker();
    }

    public void setList(ArrayList<CommentVo> list) {
        this.mCommentList = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mItemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_comment, parent, false);
        return new CommentRecyclerViewAdapter.CommentViewHolder(mItemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, final int position) {
        CommentViewHolder holder = (CommentViewHolder)viewholder;

        final CommentVo comment = mCommentList.get(position);

        holder.mTimeStamp.setText(comment.getTimestamp());
        holder.mUserName.setText(comment.getOwner());
        holder.mContent.setText(comment.getContent());

        if (!comment.getOwner_pic_url().equals("")) {
            CircleTransform circleTransform = new CircleTransform();
            Picasso.with(mContext.getApplicationContext())
                    .load(comment.getOwner_pic_url())
                    .transform(circleTransform)
                    .into(holder.mProfileImg);
        }

        if(comment.is_owner()){
            holder.mEditBtn.setVisibility(View.VISIBLE);
            holder.mDeleteBtn.setVisibility(View.VISIBLE);

            holder.mEditBtn.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    Intent intent = CommentEditActivity.getIntent(mContext);
                    intent.putExtra("position", position);
                    intent.putExtra("comment", comment);

                    ((Activity) mContext).startActivityForResult(intent, REQUEST_EDIT_COMMENT);
                }
            });

            holder.mDeleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mModel.removeComment(comment, new DeleteCommentListener() {
                        @Override
                        public void onSuccess() {
                            mCommentList.remove(comment);
                            notifyDataSetChanged();
                            mListener.onDeleteComment();
                        }

                        @Override
                        public void onFailed() {
                            Timber.d("remove comment failed");
                        }
                    });

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mCommentList.size();
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_image) ImageView mProfileImg;
        @BindView(R.id.tv_timestamp) TextView mTimeStamp;
        @BindView(R.id.user_name) TextView mUserName;
        @BindView(R.id.tv_content) TextView mContent;

        @BindView(R.id.btn_edit) ImageButton mEditBtn;
        @BindView(R.id.btn_delete) ImageButton mDeleteBtn;

        public CommentViewHolder(View mItemView) {
            super(mItemView);
            ButterKnife.bind(this, mItemView);
        }
    }

    public interface NotifyCommentDeletedListener{
        void onDeleteComment();
    }


    //댓글 수정 되었을 때
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_EDIT_COMMENT){
            int position = (int)data.getSerializableExtra("position");
            CommentVo updated = (CommentVo)data.getSerializableExtra("updated");
            mCommentList.set(position, updated);
            notifyDataSetChanged();
        }
    }
}
