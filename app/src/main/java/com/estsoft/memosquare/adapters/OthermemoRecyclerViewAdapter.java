package com.estsoft.memosquare.adapters;


import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.estsoft.memosquare.MemoSquare;
import com.estsoft.memosquare.R;
import com.estsoft.memosquare.activities.MainActivity;
import com.estsoft.memosquare.activities.MemoViewActivity;
import com.estsoft.memosquare.activities.WebBrowserActivity;

import com.estsoft.memosquare.listeners.WebBrowserMemoListFragmentEventListener;
import com.estsoft.memosquare.oldmodels.MainModel;
import com.estsoft.memosquare.utils.CircleTransform;
import com.estsoft.memosquare.utils.PicassoImageGetter;
import com.estsoft.memosquare.vo.MemoVo;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;


public class OthermemoRecyclerViewAdapter extends RecyclerView.Adapter<OthermemoRecyclerViewAdapter.MemoViewHolder> {

    private Context mContext;
    private ArrayList<MemoVo> mMemoList;

    private MainModel mModel;

    // 웹에서의 카드뷰 클릭 이벤트 리스너
    private WebBrowserMemoListFragmentEventListener mWebBrowserMemoListFragmentEventListener;

    //Google Analytics Tracker
    Tracker mTracker;


    //data를 생성자 인수로 받는다
    public OthermemoRecyclerViewAdapter(Context context, ArrayList<MemoVo> memoList) {
        this.mContext = context;
        this.mMemoList = memoList;
        this.mModel = MainModel.getInstance();

        MemoSquare application = (MemoSquare) MemoSquare.getContext();
        mTracker = application.getDefaultTracker();
        Timber.d("테 아더 컨스트 " + mMemoList);
    }

    public void setMemoList(ArrayList<MemoVo> memoList) {
        this.mMemoList = memoList;
        Timber.d("테 아더 셋 메모 " + mMemoList);
    }

    public void setWebBrowserMemoListFragmentEventListener(WebBrowserMemoListFragmentEventListener webBrowserMemoListFragmentEventListener) {
        mWebBrowserMemoListFragmentEventListener = webBrowserMemoListFragmentEventListener;
    }

    //connect cardview and viewholder
    @Override
    public OthermemoRecyclerViewAdapter.MemoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mCardView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.cardview_othermemo, parent, false);

        return new OthermemoRecyclerViewAdapter.MemoViewHolder(mCardView);
    }

    //connect model(data) and viewholder
    @Override
    public void onBindViewHolder(final OthermemoRecyclerViewAdapter.MemoViewHolder memoViewHolder, int i) {
        final MemoVo memoVo = mMemoList.get(i);
        memoViewHolder.mTitle.setText(memoVo.getTitle());

        Timber.d("테 아더 바인드 " + memoVo);

        // content 넣기
        // 그림 파일 크기 조정
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) mContext.
                getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);
        PicassoImageGetter picassoImageGetter = new PicassoImageGetter(memoViewHolder.mContent,
                mContext.getResources(), Picasso.with(mContext), metrics.widthPixels);
        Spanned htmlSpan = Html.fromHtml(memoVo.getContent(), picassoImageGetter, null);

        memoViewHolder.mContent.setText(htmlSpan);

        // 좋아요를 눌렀는지 처리
        if (memoVo.is_like()) {
            memoViewHolder.mLikeIcon.setImageDrawable(memoViewHolder.mLike);
        } else {
            memoViewHolder.mLikeIcon.setImageDrawable(memoViewHolder.mLikeBorder);
        }

        // 좋아요 갯수
        memoViewHolder.mLikeCount.setText("" + memoVo.getNum_likes());

        // 댓글 갯수
        memoViewHolder.mCommentCount.setText("" + memoVo.getNum_comments());

        memoViewHolder.mTimestamp.setText(memoVo.getTimestamp());
        memoViewHolder.mUserName.setText(memoVo.getOwner());

        //is_cliped, is_private 처리
        //getDrawable method를 쓰는 api쓰려면 api level 21 이상이어야 한다. (구버전 안될 가능성이 있다 하지만 하겟다)
        Drawable drawable;
        if(memoVo.is_clipped()) {
            drawable= mContext.getResources().getDrawable(R.drawable.ic_bookmark_clipped);
            memoViewHolder.mClipButton.setImageDrawable(drawable);
        }else if(!memoVo.is_clipped()){
            drawable= mContext.getResources().getDrawable(R.drawable.ic_bookmark_border);
            memoViewHolder.mClipButton.setImageDrawable(drawable);
        }

        final int index = i;

        // card view onclick listener
        memoViewHolder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMemoView(memoVo, index);

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction("other memo-click cardview")
                        .build());
            }
        });

        memoViewHolder.mLikeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (memoVo.is_like()) {
                    memoViewHolder.mLikeIcon.setImageDrawable(memoViewHolder.mLikeBorder);
                    memoVo.setNum_likes(memoVo.getNum_likes() - 1);
                    mModel.notLikeMemo(memoVo);
                } else {
                    memoViewHolder.mLikeIcon.setImageDrawable(memoViewHolder.mLike);
                    memoVo.setNum_likes(memoVo.getNum_likes() + 1);
                    mModel.likeMemo(memoVo);
                }
                memoVo.setIs_liked(!memoVo.is_like());
                memoViewHolder.mLikeCount.setText("" + memoVo.getNum_likes());
            }
        });

        memoViewHolder.mCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMemoView(memoVo, index);

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction("other memo-click comment")
                        .build());
            }
        });

        //mClipButton-onclick listener
        memoViewHolder.mClipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // is_cliped=true: not cliped
                if (memoVo.is_clipped()) {
                    // 이미지 변경
                    memoViewHolder.mClipButton.setImageDrawable(memoViewHolder.mNotClippedImg);
                    memoVo.setIs_clipped(false);
                    mModel.notClipMemo(memoVo);
                    Timber.d("107"+memoVo.toString());
                    // 클립 카운트 감소
                    memoVo.setNum_clips(memoVo.getNum_clips()-1);

                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("other memo-not clip")
                            .build());
                }
                // is_cliped=flase: clip
                else {
                    // 이미지 변경
                    memoViewHolder.mClipButton.setImageDrawable(memoViewHolder.mClippedImg);
                    memoVo.setIs_clipped(true);
                    mModel.clipMemo(memoVo);
                    Timber.d("118"+memoVo.toString());
                    memoVo.setNum_clips(memoVo.getNum_clips()+1);

                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("other memo-clip")
                            .build());
                }
            }
        });

        //uri에서 프사 받아서 저장
        if (!memoVo.getOwner_pic_url().equals("")) {
            CircleTransform circleTransform = new CircleTransform();
            Picasso.with(mContext.getApplicationContext())
                    .load(memoVo.getOwner_pic_url())
                    .transform(circleTransform)
                    .into(memoViewHolder.mProfileImg);
        }

    }

    private void goToMemoView(MemoVo memoVo, int index) {
        Intent intent = MemoViewActivity.getIntent(mContext);
        intent.putExtra("memo", memoVo);
        intent.putExtra("index", index);
        if (mContext instanceof MainActivity)
            ((MainActivity) mContext).startActivityForResult(intent, ((MainActivity) mContext).REQUEST_MEMOVIEW);
        else if (mContext instanceof WebBrowserActivity)
            mContext.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return mMemoList.size();
    }

    public static class MemoViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.card_view) CardView mCardView;
        @BindView(R.id.tv_title) TextView mTitle;
        @BindView(R.id.tv_content) HtmlTextView mContent;
        @BindView(R.id.like_btn) LinearLayout mLikeButton;
        @BindView(R.id.like_icon) ImageView mLikeIcon;
        @BindView(R.id.like_count) TextView mLikeCount;
        @BindView(R.id.comment_btn) LinearLayout mCommentButton;
        @BindView(R.id.comment_count) TextView mCommentCount;
        @BindView(R.id.ib_clip) ImageButton mClipButton;
        @BindView(R.id.tv_timestamp) TextView mTimestamp;
        @BindView(R.id.user_name) TextView mUserName;
        @BindView(R.id.profile_image) ImageView mProfileImg;

        @BindDrawable(R.drawable.ic_like) Drawable mLike;
        @BindDrawable(R.drawable.ic_like_border) Drawable mLikeBorder;
        @BindDrawable(R.drawable.ic_bookmark_border) Drawable mNotClippedImg;
        @BindDrawable(R.drawable.ic_bookmark_clipped) Drawable mClippedImg;

        public MemoViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    //swipeRefreshLayout
    // Clean all elements of the recycler
    public void clear() {
        if(mMemoList.size() > 0){
            mMemoList.clear();
        }
        notifyDataSetChanged();
    }
}
