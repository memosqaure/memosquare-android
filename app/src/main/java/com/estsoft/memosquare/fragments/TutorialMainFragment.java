package com.estsoft.memosquare.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.estsoft.memosquare.R;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hokyung on 2016. 11. 25..
 */

public class TutorialMainFragment extends TutorialFragment {

    // 리소스 할당
    @BindDrawable(R.drawable.tutorial_main) Drawable mScreenshot;
    @BindString(R.string.tutorial_main) String mDescription;

    @Override
    void setWidgets() {
        mImageView.setImageDrawable(mScreenshot);
        mTextView.setText(mDescription);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mScreenshot = null;
    }
}
