package com.estsoft.memosquare.fragments;

import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;

import com.estsoft.memosquare.R;

import butterknife.BindDrawable;
import butterknife.BindString;

/**
 * Created by hokyung on 2016. 11. 25..
 */

public class TutorialWriteFragment extends TutorialFragment {

    // 리소스 할당
    @BindDrawable(R.drawable.tutorial_write) Drawable mScreenshot;
    @BindString(R.string.tutorial_write) String mDescription;

    @Override
    void setWidgets() {
        mImageView.setImageDrawable(mScreenshot);
        mTextView.setText(mDescription);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mScreenshot = null;
    }
}
