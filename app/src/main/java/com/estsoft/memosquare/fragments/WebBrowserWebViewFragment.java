package com.estsoft.memosquare.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.estsoft.memosquare.R;
import com.estsoft.memosquare.activities.WebBrowserActivity;
import com.estsoft.memosquare.database.Repository.UrlRepository;
import com.estsoft.memosquare.listeners.WebViewFragmentEventListener;
import com.estsoft.memosquare.utils.InjectScript;
import com.estsoft.memosquare.vo.UrlVo;

import java.util.LinkedList;
import java.util.Queue;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by hokyung on 2016. 11. 1..
 * 오직 웹뷰만 있는 fragment
 */

public class WebBrowserWebViewFragment extends Fragment {

    // handler queue에 넣고 뺄 데이터
    private static final int QUEUE = 111;

    // 위젯 할당
    @BindView(R.id.webview) WebView mWebView;
    @BindView(R.id.webview_progress_bar) ProgressBar mProgressBar;

    // 부모 액티비티
    private WebBrowserActivity mActivity;

    // 웹페이지 로딩을 위한 handler
    public Handler mHandler;

    // 웹페이지가 모두 로딩되었는지 확인을 위한 Queue
    private Queue<Integer> mHandlerQueue;

    // url 주소
    private String mUrl;

    // 자바스크립트 파일 삽입을 위한 Util Class
    private InjectScript mInjectScript;

    // 이벤트 리스너
    private WebViewFragmentEventListener mWebViewFragmentEventListener;

    public void setWebViewFragmentEventListener(
            WebViewFragmentEventListener webViewFragmentEventListener) {
        mWebViewFragmentEventListener = webViewFragmentEventListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container
            , @Nullable Bundle savedInstanceState) {

        // inflate view
        View view = inflater.inflate(R.layout.fragment_webview, container, false);
        ButterKnife.bind(this, view);

        // 부모 액티비티 연결
        mActivity = (WebBrowserActivity) getActivity();

        // 핸들러 할당
        mHandler = new Handler();

        // 큐 생성
        mHandlerQueue = new LinkedList<>();

        // 프로그래스바 설정
        mProgressBar.setMax(100);

        // 주소 설정
        mUrl = mWebViewFragmentEventListener.getUrl();

        // 자바스크립트 삽입 객체
        mInjectScript = InjectScript.get(getActivity());

        // 웹뷰 세팅
        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowUniversalAccessFromFileURLs(true);

        // 자바스크립트 객체 삽입
        mWebView.addJavascriptInterface(new JavaScriptInterface(), "INTERFACE");

        // 렌더링 이벤트에 응답하는 인터페이스 크롬 클라이언트. 자바 스크립트 대화 상자, favicon, 제목과 진행상황 처리
        mWebView.setWebChromeClient(new WebChromeClient() {

            // 웹 뷰 로딩 프로그래스바 설정
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
                    mProgressBar.setVisibility(View.GONE);
                }
                else {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mProgressBar.setProgress(newProgress);
                }
            }

            // 웹 콘솔 log 메세지 출력
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                return super.onConsoleMessage(consoleMessage);
            }
        });

        // 렌더링 이벤트에 응답하는 인터페이스 웹뷰 클라이언트. 각종 알림 및 요청 처리
        mWebView.setWebViewClient(new WebViewClient() {

            // 기본적으로는 암시적 인텐트를 만드는 메소드
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Timber.d("shouldOverrideUrlLoading");
                // false 를 반환하면 url로드를 웹뷰에게 맡기는 것
                // 전화번호나 이메일주소등을 눌렀을때는 true로 내가 직접 처리해줄수있음\
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                Timber.d("onPageStarted");
                mHandlerQueue.clear();
                mWebViewFragmentEventListener.changeUrl(url);
                mWebViewFragmentEventListener.setHighlightAvailable(false);
                mWebViewFragmentEventListener.changeModeToWebBrowsing();
                mHandler.removeCallbacksAndMessages(null);
                // 페이지 새로 로딩시에 메모 리스트 초기화
                mWebViewFragmentEventListener.clearMemoList();
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(final WebView view, final String url) {
                Timber.d("webview client onPageFinished");

                // 큐에 데이터를 넣는다
                mHandlerQueue.offer(QUEUE);

                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // 큐에서 하나 제거 후 확인
                        mHandlerQueue.poll();
                        if (isQueueEmpty()) {
                            Timber.d("last injection");
//                            mWebViewFragmentEventListener.changeModeToWebBrowsing();
                            mWebViewFragmentEventListener.setHasMemo(false);
                            mWebViewFragmentEventListener.setHighlightAvailable(true);
                            mInjectScript.injectJavaScriptFiles(mWebView);
                            // 메모 리스트 새로고침
                            mWebViewFragmentEventListener.initializeOffset();
                            mWebViewFragmentEventListener.loadMemoList();
                            // 방문한 웹브라우저 주소 디비에 저장
                            UrlRepository urlRepository = UrlRepository.getInstance(getContext());
                            UrlVo urlVo = urlRepository.getUrl(url);
                            // 처음 방문의 경우
                            if (urlVo == null) {
                                Timber.d("first meet" + url);
                                urlVo = new UrlVo();
                                urlVo.setTitle(view.getTitle());
                                urlVo.setUrl(url);
                                urlVo.setCount(1);
                                urlRepository.addUrl(urlVo);
                            }
                            // 방문했던 페이지
                            else {
                                Timber.d("already met before " + urlVo);
                                urlVo.setCount(urlVo.getCount() + 1);
                                urlRepository.updateUrl(urlVo);
                            }
                        }
                    }
                }, 3000);
            }
        });

        // 웹뷰에 주소창의 내용을 띄운다
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mWebView.loadUrl(mUrl);
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        // 스래드 초기화
        mHandler.removeCallbacksAndMessages(null);
        mHandlerQueue.clear();
        super.onDestroyView();
    }

    // 주소 직접 입력 시 동작 할 메소드
    public void goUrl(String url) {
        final String URL = url;
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mWebView.loadUrl(URL);
            }
        });
    }

    // 사이트 이름 반환하는 메소드
    public String getName() {
        return mWebView.getTitle();
    }

    // 뒤로가기 동작 메소드
    public boolean onBackPressed() {
        // 이전 기록이 있다면 거기로 이동
        if (mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        } else {
            return false;
        }
    }

    // 앞으로 가기 동작 메소드
    public boolean goForward() {
        if (mWebView.canGoForward()) {
            mWebView.goForward();
            return true;
        }
        return false;
    }

    // 하이라이트 동작시키기
    public void jsExecute() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mWebView.loadUrl("javascript:memosquareCliping();");
            }
        });
    }

    class JavaScriptInterface {

        public JavaScriptInterface() {
            Timber.d("constructor");
        }

        // 웹에서 텍스트를 받아오기 위한 메소드
        @SuppressWarnings("unused")
        @JavascriptInterface
        public void getHighlightedContent(String content) {
            final String CONTENT = content;
            Timber.d("getHighlightedContent: " + CONTENT);
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mWebViewFragmentEventListener.addHighlightedContents(CONTENT);
                }
            });
        };

        // 웹에서 이미지 태그를 받아오기 위한 메소드
        @SuppressWarnings("unused")
        @JavascriptInterface
        public void getImgTag(final String content) {
            Timber.d("getImgTag: " + content);
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mWebViewFragmentEventListener.addImage(content);
                }
            });
        }

        // 웹페이지 전체 내용을 받아오기 위한 메소드
        // 디버깅용
        @SuppressWarnings("unused")
        @JavascriptInterface
        public void getPageHtml(String page) {
            String temp = page;

            // 로그 하나의 최대 길이는 4000
            while (temp.length() > 4000) {
                Timber.d("page: " + temp.substring(0, 4000));
                temp = temp.substring(4000);
            }

            Timber.d("page: " + temp);
        }
    }

    // 큐가 비었는지 확인하는 메소드
    private boolean isQueueEmpty() {
        return mHandlerQueue.peek() == null;
    }
}
