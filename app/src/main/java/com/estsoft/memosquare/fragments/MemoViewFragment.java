package com.estsoft.memosquare.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.estsoft.memosquare.MemoSquare;
import com.estsoft.memosquare.R;
import com.estsoft.memosquare.activities.MemoViewActivity;
import com.estsoft.memosquare.activities.WebBrowserActivity;

import com.estsoft.memosquare.listeners.MemoRemoveEventListener;

import com.estsoft.memosquare.oldmodels.MemoViewModel;
import com.estsoft.memosquare.utils.CircleTransform;
import com.estsoft.memosquare.utils.MemoRemoveDialogUtil;
import com.estsoft.memosquare.utils.PicassoImageGetter;
import com.estsoft.memosquare.vo.MemoVo;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MemoViewFragment extends Fragment {

    // 위젯 할당
    @BindView(R.id.memoview_profile_img) ImageView mProfileImg;
    @BindView(R.id.memoview_timestamp) TextView mTimeStamp;
    @BindView(R.id.memoview_owner) TextView mOwnerName;
    @BindView(R.id.memoview_mymemo_btns) LinearLayout mMyMemoButtons;
    @BindView(R.id.memoview_title) TextView mTitle;
//    @BindView(R.id.memoview_content) TextView mContent;
    @BindView(R.id.memoview_content) HtmlTextView mContent;
    @BindView(R.id.memoview_secret) ImageButton mLockButton;

    // 리소스 할당
    @BindDrawable(R.drawable.ic_lock_close) Drawable mLockedImg;
    @BindDrawable(R.drawable.ic_lock_open) Drawable mUnlockedImg;

    // 메모데이터가 담길 변수
    private MemoVo mMemoVo;
    private int memoIndex;

    private MemoViewModel mModel;

    //Google Analytics Tracker
    Tracker mTracker;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_memo_view, container, false);
        ButterKnife.bind(this, view);

        MemoSquare application = (MemoSquare) MemoSquare.getContext();
        mTracker = application.getDefaultTracker();

        mModel = MemoViewModel.getInstance();

        // 메모 데이터 받아오기
        Intent intent = getActivity().getIntent();
        mMemoVo = (MemoVo) intent.getSerializableExtra("memo");
        memoIndex = intent.getIntExtra("index", -1);

        // 데이터 할당
        setWidgets();

        // 메모가 남의 것이면 버튼들을 숨긴다
        if (!mMemoVo.is_mymemo()) {
            mMyMemoButtons.setVisibility(View.GONE);

            mTracker.setScreenName("memo detail-other memo");
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        }else{
            mTracker.setScreenName("memo detail-my memo");
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        }



        return view;
    }

    // 각 데이터들 위젯에 할당
    private void setWidgets() {
        if (!mMemoVo.getOwner_pic_url().equals("")) {
            // profile image
            CircleTransform circleTransform = new CircleTransform();
            Picasso.with(getActivity().getApplicationContext())
                    .load(mMemoVo.getOwner_pic_url())
                    .transform(circleTransform)
                    .into(mProfileImg);
        }

        // time stamp
        mTimeStamp.setText(mMemoVo.getTimestamp());

        // owner name
        mOwnerName.setText(mMemoVo.getOwner());

        // title
        mTitle.setText(mMemoVo.getTitle());

        // content
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getContext().
                getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);
        PicassoImageGetter picassoImageGetter = new PicassoImageGetter(mContent, getResources(),
                Picasso.with(getContext()), metrics.widthPixels);
        Spanned htmlSpan = Html.fromHtml(mMemoVo.getContent(), picassoImageGetter, null);
        mContent.setText(htmlSpan);

        // setSecretButtonImage button
        if(!mMemoVo.is_private()) mLockButton.setImageDrawable(mUnlockedImg);
    }

    // setSecretButtonImage 버튼 클릭시 메소드
    @OnClick(R.id.memoview_secret)
    void secretButton() {
        if(mMemoVo.is_private()){
            mMemoVo.setIs_private(false);
            mLockButton.setImageDrawable(mUnlockedImg);

            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction("memo detail-change to public")
                    .build());

        }else{
            mMemoVo.setIs_private(true);
            mLockButton.setImageDrawable(mLockedImg);

            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction("memo detail-change to private")
                    .build());
        }
        mModel.lockToggle(mMemoVo);
    }

    // modify 버튼 클릭시 메소드
    @OnClick(R.id.memoview_modify)
    void modifyButton() {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("memo detail-click edit button")
                .build());

        Intent intent = WebBrowserActivity.getIntent(getActivity());
        intent.putExtra(Intent.EXTRA_TEXT, mMemoVo.getPage());
        intent.putExtra("memo", mMemoVo);
        intent.putExtra("mode", WebBrowserActivity.MEMOWRITE);
        startActivity(intent);
        getActivity().finish();
    }

    // remove 버튼 클릭시 메소드
    @OnClick(R.id.memoview_remove)
    void removeButton() {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("memo detail-click remove button")
                .build());
        // 다이얼로그에 콜백을 넣어서 구현
        MemoRemoveDialogUtil dialogUtil = new MemoRemoveDialogUtil(getFragmentManager(), new MemoRemoveEventListener() {
            @Override
            public void remove() {
                // 메모 지우는 메소드
                mModel.deleteMemo(mMemoVo);
                Intent intent = new Intent();
                intent.putExtra("index", memoIndex);
                getActivity().setResult(((MemoViewActivity)getActivity()).RESULT_REMOVE, intent);
                getActivity().finish();
            }
        });
        dialogUtil.show();
    }


    // 메모 정보를 액티비티로 전송하기 위한 메소드
    public MemoVo getMemoVo() {
        return mMemoVo;
    }
}
