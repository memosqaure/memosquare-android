package com.estsoft.memosquare.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.estsoft.memosquare.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hokyung on 2016. 11. 25..
 * 튜토리얼 프래그먼트를 쉽게 작성하기 위한 추상 클래스
 */

public abstract class TutorialFragment extends Fragment {

    // 위젯 할당
    @BindView(R.id.tutorial_screenshot) ImageView mImageView;
    @BindView(R.id.tutorial_description) TextView mTextView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tutorial, container, false);
        ButterKnife.bind(this, view);

        setWidgets();

        return view;
    }

    abstract void setWidgets();
}
