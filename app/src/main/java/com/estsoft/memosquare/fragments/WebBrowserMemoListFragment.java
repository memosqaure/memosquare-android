package com.estsoft.memosquare.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.estsoft.memosquare.MemoSquare;
import com.estsoft.memosquare.R;
import com.estsoft.memosquare.adapters.OthermemoRecyclerViewAdapter;

import com.estsoft.memosquare.listeners.GetMemoListListener;
import com.estsoft.memosquare.listeners.WebBrowserMemoListFragmentEventListener;
import com.estsoft.memosquare.mvp.presenters.WebBrowserMemoListPresenter;
import com.estsoft.memosquare.mvp.presenters.WebBrowserMemoListPresenterImpl;
import com.estsoft.memosquare.mvp.views.WebBrowserMemoListView;
import com.estsoft.memosquare.oldmodels.WebBrowserModel;
import com.estsoft.memosquare.utils.EndlessRecyclerOnScrollListener;
import com.estsoft.memosquare.vo.MemoVo;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


import java.util.ArrayList;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class WebBrowserMemoListFragment extends Fragment implements WebBrowserMemoListView {

//    @BindView(R.id.recyclerview)
    RecyclerView mRecView;
    @BindView(R.id.swipeContainer) SwipeRefreshLayout mSwipeContainer;
    @BindView(R.id.emptyView) TextView mEmptyView;

    @BindString(R.string.loading) String mLoadingText;
    @BindString(R.string.no_memo) String mNoMemoText;
    @BindString(R.string.networkError) String mNetworkErrorText;

    private OthermemoRecyclerViewAdapter mAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private EndlessRecyclerOnScrollListener mEndlessRecyclerOnScrollListener;
    private boolean mIsTop;  // 지금 스크롤이 리스트의 맨 위에 있는지 상태를 저장하는 변수

    private WebBrowserMemoListPresenter mPresenter;

    // 이벤트 리스너
    private WebBrowserMemoListFragmentEventListener mWebBrowserMemoListFragmentEventListener;

    //Google Analytics Tracker
    Tracker mTracker;

    public void setWebBrowserMemoListFragmentEventListener(WebBrowserMemoListFragmentEventListener webBrowserMemoListFragmentEventListener) {
        mWebBrowserMemoListFragmentEventListener = webBrowserMemoListFragmentEventListener;
    }

    public boolean isTop() {
        return mIsTop;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //1. inflate the layout for this fragment.
        View rootView = inflater.inflate(R.layout.fragment_showsmemolist, container, false);
        ButterKnife.bind(this, rootView);

        MemoSquare application = (MemoSquare) MemoSquare.getContext();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Web-memo list");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        mRecView = (RecyclerView)rootView.findViewById(R.id.recyclerview);

        initializeMembers();

        mPresenter = new WebBrowserMemoListPresenterImpl(this);
        mPresenter.onCreateView();

        return rootView;
    }

    private void initializeMembers() {
        mIsTop = true;
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mEndlessRecyclerOnScrollListener = generateEndlessRecyclerOnScrollListener();
        mSwipeContainer.setOnRefreshListener(generateOnRefreshListener());

        mRecView.setHasFixedSize(true);
        mRecView.setLayoutManager(mLinearLayoutManager);
        mRecView.addOnScrollListener(mEndlessRecyclerOnScrollListener);
    }

    private EndlessRecyclerOnScrollListener generateEndlessRecyclerOnScrollListener() {
        return new EndlessRecyclerOnScrollListener(mLinearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                loadMemoList();
            }

            @Override
            public void setIsTop(boolean isTop) {
                // 모드가 변경중이면 동작하지 않는다
                if (mWebBrowserMemoListFragmentEventListener.isInModeChanging()) {
                    return;
                }

                mIsTop = isTop;
                mPresenter.setUpButtonVisibility(isTop);
            }
        };
    }

    private SwipeRefreshLayout.OnRefreshListener generateOnRefreshListener() {
        return new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // mEndlessRecyclerOnScrollListener.loading = false로 되어 있으면 onLoader가 실행된다.
                mEndlessRecyclerOnScrollListener.setLoading(true);
                mEndlessRecyclerOnScrollListener.reset();
                mAdapter.clear();
                initializeOffset();
                loadMemoList();
                mSwipeContainer.setRefreshing(false);
            }
        };
    }

    @Override
    public void generateAdapterAndSetToRecView(ArrayList<MemoVo> items) {
        Timber.d("테 제너레이트 " + items);
        mAdapter = new OthermemoRecyclerViewAdapter(getActivity(), items);
        mAdapter.setWebBrowserMemoListFragmentEventListener(mWebBrowserMemoListFragmentEventListener);
        mRecView.setAdapter(mAdapter);
    }

    @Override
    public void setMemoListToAdapter(ArrayList<MemoVo> items) {
        Timber.d("테 셋 메모 리스트 " + items);
        mAdapter.setMemoList(items);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void setIsTopAsTrue() {
        mIsTop = true;
    }

    @Override
    public boolean IsTop() {
        return mIsTop;
    }

    @Override
    public String getUrl() {
        return mWebBrowserMemoListFragmentEventListener.getUrl();
    }

    @Override
    public void showRecyclerView(){
        mEmptyView.setVisibility(View.GONE);
        mRecView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoadingMessage(){
        mRecView.setVisibility(View.GONE);
        mEmptyView.setVisibility(View.VISIBLE);
        mEmptyView.setText(mLoadingText);
    }

    @Override
    public void showNetworkErrorMessage(){
        mRecView.setVisibility(View.GONE);
        mEmptyView.setVisibility(View.VISIBLE);
        mEmptyView.setText(mNetworkErrorText);
    }

    @Override
    public void showNoMemoMessage(){
        mRecView.setVisibility(View.GONE);
        mEmptyView.setVisibility(View.VISIBLE);
        mEmptyView.setText(mNoMemoText);
    }

    @Override
    public void goUp() {
        mRecView.scrollToPosition(3);
        mRecView.smoothScrollToPosition(0);
    }

    @Override
    public void notifyDataSetChanged() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void setButtonVisibility(int visibility) {
        mWebBrowserMemoListFragmentEventListener.setUpButtonVisibility(visibility);
    }

    public void clearMemoList() {
        mPresenter.clearMemoList();
    }

    // 페이지가 다 뜰 경우 메모를 받아온다
    // 프레젠터에 위임
    public void loadMemoList() {
        mPresenter.loadPageMemos();
    }

    public void initializeOffset() {
        mPresenter.initializeOffset();
    }
}
