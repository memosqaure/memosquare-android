package com.estsoft.memosquare.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.estsoft.memosquare.R;
import com.estsoft.memosquare.listeners.AddCategoryEventListener;

/**
 * Created by hokyung on 2016. 12. 20..
 */

public class AddCategoryDialogFragment extends DialogFragment {

    // 콜백 메소드
    private AddCategoryEventListener mAddCategoryEventListener;

    public void setAddCategoryEventListener(AddCategoryEventListener addCategoryEventListener) {
        mAddCategoryEventListener = addCategoryEventListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // 카테고리 입력받을 폼
        final EditText form = new EditText(getActivity());
        form.setSingleLine();
        InputFilter[] inputFilters = new InputFilter[1];
        inputFilters[0] = new InputFilter.LengthFilter(44);
        form.setFilters(inputFilters);

        float dpi = getContext().getResources().getDisplayMetrics().density;
        int viewSpacingLeft = (int) dpi * 20;
        int viewSpacingTop = (int) dpi * 10;
        int viewSpacingRight = (int) dpi * 20;
        int viewSpacingBottom = (int) dpi * 0;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.add_category_title)
                .setView(form, viewSpacingLeft, viewSpacingTop, viewSpacingRight, viewSpacingBottom)
                .setPositiveButton(R.string.add_category_positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String name = form.getText().toString();
                        if (name.equals(""))
                            Toast.makeText(getContext(), R.string.category_name_empty,
                                    Toast.LENGTH_SHORT).show();
                        else
                            mAddCategoryEventListener.add(name);
                    }
                });

        return builder.create();
    }
}
