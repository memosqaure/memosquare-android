package com.estsoft.memosquare.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.estsoft.memosquare.MemoSquare;
import com.estsoft.memosquare.R;
import com.estsoft.memosquare.database.Repository.LocalCategoryRepository;
import com.estsoft.memosquare.listeners.SaveMemoButtonEventListener;
import com.estsoft.memosquare.vo.CategoryVo;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

public class SaveMemoDialogFragment extends DialogFragment {

    private SaveMemoButtonEventListener mSaveMemoButtonEventListener;

    //Google Analytics Tracker
    Tracker mTracker;

    public void setSaveMemoButtonEventListener(SaveMemoButtonEventListener saveMemoButtonEventListener) {
        mSaveMemoButtonEventListener = saveMemoButtonEventListener;

        MemoSquare application = (MemoSquare) MemoSquare.getContext();
        this.mTracker = application.getDefaultTracker();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ArrayList<CategoryVo> categoryList = (ArrayList) LocalCategoryRepository.
                getInstance(getContext()).getCategoryList();

        final String[] names = new String[categoryList.size() + 1];
        names[0] = getResources().getString(R.string.default_category);
        for (int i = 0; i < categoryList.size(); i++) {
            names[i + 1] = categoryList.get(i).getName();
        }

        final EditText categoryName = new EditText(getActivity());
        categoryName.setHint(R.string.add_category_hint);
        categoryName.setSingleLine();
        InputFilter[] inputFilters = new InputFilter[1];
        inputFilters[0] = new InputFilter.LengthFilter(44);
        categoryName.setFilters(inputFilters);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.category_select_dialog)
                .setItems(names, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            // 기본 카테고리로 저장
                            mSaveMemoButtonEventListener.save("");
                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("Action")
                                    .setAction("web-write memo-category dialog-choose 'uncategorized'")
                                    .build());
                        } else {
                            // 해당 카테고리로 저장
                            mSaveMemoButtonEventListener.save(names[which]);
                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("Action")
                                    .setAction("web-write memo-category dialog-choose not 'uncategorized'")
                                    .build());
                        }
                    }
                })
                .setView(categoryName)
                .setPositiveButton(R.string.category_save_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String category = categoryName.getText().toString();
                        category = category.trim();
                        if (category.equals("")) {
                                mSaveMemoButtonEventListener.save("");
                        } else {
                                mSaveMemoButtonEventListener.save(category);
                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("Action")
                                    .setAction("web-write memo-category dialog-create new category")
                                    .build());
                        }
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("Action")
                                .setAction("web-write memo-category dialog-click save button")
                                .build());
                    }
                })
                .setNegativeButton(R.string.category_save_cancel, null);

        return builder.create();
    }
}
