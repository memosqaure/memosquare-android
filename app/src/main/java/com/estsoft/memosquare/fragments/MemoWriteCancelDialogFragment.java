package com.estsoft.memosquare.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.estsoft.memosquare.R;
import com.estsoft.memosquare.listeners.MemoWriteCancelEventListener;

/**
 * Created by hokyung on 2016. 11. 14..
 * 메모 작성을 벗어날 것인지 묻는 다이얼로그 클래스
 */

public class MemoWriteCancelDialogFragment extends DialogFragment {

    private MemoWriteCancelEventListener mMemoWriteCancelEventListener;

    public void setMemoWriteCancelEventListener(MemoWriteCancelEventListener memoWriteCancelEventListener) {
        mMemoWriteCancelEventListener = memoWriteCancelEventListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.memo_write_cancel_title)
                .setMessage(R.string.memo_write_cancel_message)
                .setPositiveButton(R.string.dialog_positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mMemoWriteCancelEventListener.cancel();
                    }
                })
                .setNegativeButton(R.string.dialog_negative, null);

        return builder.create();
    }
}
