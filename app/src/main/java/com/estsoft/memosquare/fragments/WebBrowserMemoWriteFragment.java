package com.estsoft.memosquare.fragments;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Selection;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.estsoft.memosquare.MemoSquare;
import com.estsoft.memosquare.R;
import com.estsoft.memosquare.listeners.GetImageSpanCallbackListener;
import com.estsoft.memosquare.listeners.SaveMemoButtonEventListener;
import com.estsoft.memosquare.listeners.WebBrowserMemoWriteFragmentEventListener;
import com.estsoft.memosquare.mvp.presenters.WebBrowserMemoWritePresenter;
import com.estsoft.memosquare.mvp.presenters.WebBrowserMemoWritePresenterImpl;
import com.estsoft.memosquare.mvp.views.WebBrowserMemoWriteView;
import com.estsoft.memosquare.utils.GetOneImageSpan;
import com.estsoft.memosquare.utils.PicassoImageGetter;
import com.estsoft.memosquare.utils.ProgressBarDialogUtil;
import com.estsoft.memosquare.utils.SaveMemoDialogUtil;
import com.estsoft.memosquare.vo.MemoVo;
import com.squareup.picasso.Picasso;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

/**
 * Created by hokyung on 2016. 11. 1..
 * 메모 쓰기 버튼 누를시 활성화되는 Fragment
 * 깔끔한 코드를 위해 OnClickListener를 implements하여 구현하였음
 */

public class WebBrowserMemoWriteFragment extends Fragment implements WebBrowserMemoWriteView {

    // 위젯 할당
    @BindView(R.id.highlight_btn) ImageButton mHighlightButton;
    @BindView(R.id.memo_title_input) EditText mTitleInput;
    @BindView(R.id.memo_write_container) LinearLayout mWriteContainer;
    @BindView(R.id.memo_content_input) EditText mContentInput;
    @BindView(R.id.secret_btn) ImageButton mSecretButton;

    // 리소스 할당
    @BindString(R.string.title_hint) String mTitleHint;
    @BindString(R.string.title_basic) String mTitleBasic;
    @BindString(R.string.wait) String mWait;
    @BindString(R.string.title_is_empty) String mTitleEmpty;
    @BindString(R.string.content_is_empty) String mContentEmpty;
    @BindString(R.string.saved) String mSaved;
    @BindString(R.string.save_fail) String mSaveFail;
    @BindDrawable(R.drawable.ic_more_horiz) Drawable mDisabledState;
    @BindDrawable(R.drawable.ic_marker) Drawable mNormalState;
    @BindDrawable(R.drawable.ic_marker_orange) Drawable mHighlightState;
    @BindDrawable(R.drawable.ic_lock_open) Drawable mLockOpen;
    @BindDrawable(R.drawable.ic_lock_close) Drawable mLockClose;

    // 메모 수정의 경우 갖고있을 메모 객체
    private MemoVo mMemoVo;

    // 프레젠터
    private WebBrowserMemoWritePresenter mWebBrowserMemoWritePresenter;

    // 프로그래스바
    private ProgressBarDialogUtil mProgressBarDialogUtil;

    // 이미지 뷰의 파라미터
    private int mImageWidth;

    // 이벤트 리스너
    private WebBrowserMemoWriteFragmentEventListener mWebBrowserMemoWriteFragmentEventListener;

    //Google Analytics Tracker
    Tracker mTracker;

    public void setMemoVo(MemoVo memoVo) {
        Timber.d("setMemoVo " + memoVo);
        mMemoVo = memoVo;
    }

    public void setWebBrowserMemoWriteFragmentEventListener(WebBrowserMemoWriteFragmentEventListener webBrowserMemoWriteFragmentEventListener) {
        mWebBrowserMemoWriteFragmentEventListener = webBrowserMemoWriteFragmentEventListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_web_memo_write, container, false);
        ButterKnife.bind(this, view);

        MemoSquare application = (MemoSquare) MemoSquare.getContext();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Web-write memo");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());


        Timber.d("onCreateView");

        mWebBrowserMemoWritePresenter = new WebBrowserMemoWritePresenterImpl(this);
        mWebBrowserMemoWritePresenter.onCreateView();

        initializeMembers();

        return view;
    }

    private void initializeMembers() {
        // 위젯 할당
        setWidgets(mMemoVo);

        // 프로그래스바 다이얼로그 준비
        mProgressBarDialogUtil = new ProgressBarDialogUtil(getActivity());

        mTitleInput.setHint(mTitleHint + " " + mWebBrowserMemoWritePresenter.getDate());

        // 하이라이트 버튼 상태 지정
        setHighlightButtonEnable(mWebBrowserMemoWriteFragmentEventListener.isHighlightAvailable());

        // 메모에 삽입될 사진 크기 정보
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getContext().
                getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);
        mImageWidth = metrics.widthPixels / 2;
    }

    // 메모 수정 같이 메모 객체가 이미 있는 경우 위젯에 값을 할당한다
    public void setWidgets(final MemoVo memoVo) {
        if (memoVo != null && !memoVo.getTitle().equals("")) {
            final ProgressBarDialogUtil progressBarDialogUtil = new ProgressBarDialogUtil(getActivity());
            progressBarDialogUtil.show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Timber.d("setWidgets " + memoVo);
                    mWebBrowserMemoWritePresenter.setModify(true);
                    mTitleInput.setText(memoVo.getTitle());

                    // content 넣기
                    PicassoImageGetter picassoImageGetter = new PicassoImageGetter(mContentInput,
                            getResources(), Picasso.with(getContext()), mImageWidth);
                    Spanned htmlSpan = mWebBrowserMemoWritePresenter.
                            getOriginalMemoHtmlSpan(memoVo.getContent(), picassoImageGetter);

                    mContentInput.setText(htmlSpan);
                    mWebBrowserMemoWritePresenter.setSecret(memoVo.is_private());
                    progressBarDialogUtil.cancel();
                }
            }, 100);
        }
    }

    @OnClick(R.id.highlight_btn)
    public void onHighlightClick() {
        Timber.d("highlight_btn clicked");
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("web-write memo-click marker button")
                .build());
        mWebBrowserMemoWritePresenter.switchHighlight();
    }
    
    @Override
    public boolean toggleHighlight() {
        if (mWebBrowserMemoWriteFragmentEventListener.isHighlightAvailable()) {
            mWebBrowserMemoWriteFragmentEventListener.toggleHighlight();
            return true;
        } else {
            Toast.makeText(getActivity(), mWait, Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    @Override
    public void setHighlightImageDisabled() {
        mHighlightButton.setImageDrawable(mDisabledState);
    }

    @Override
    public void setHighlightImageNormal() {
        mHighlightButton.setImageDrawable(mNormalState);
    }

    @Override
    public void setHighlightImageActivated() {
        mHighlightButton.setImageDrawable(mHighlightState);
    }

    @OnClick(R.id.secret_btn)
    public void onSecretClick() {
        mWebBrowserMemoWritePresenter.setSecretButtonImage();
    }

    @Override
    public void setSecretImageOpen() {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("web-write memo-change to public")
                .build());

        mSecretButton.setImageDrawable(mLockOpen);
    }

    @Override
    public void setSecretImageClose() {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("web-write memo-change to private")
                .build());

        mSecretButton.setImageDrawable(mLockClose);
    }

    @OnClick(R.id.memo_save_btn)
    public void onSaveClick() {
        Timber.d("WebBrowserMemoWriteFragment 197: savebutton clicked");

        if (mContentInput.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), mContentEmpty, Toast.LENGTH_SHORT).show();
            return;
        }

        SaveMemoDialogUtil dialogUtil = new SaveMemoDialogUtil(getActivity().getSupportFragmentManager(),
                new SaveMemoButtonEventListener() {
                    @Override
                    public void save(String category) {
                        if (mMemoVo == null)
                            mMemoVo = new MemoVo();

                        mMemoVo.setCategory_name(category);

                        mWebBrowserMemoWritePresenter.saveMemo();
                    }
                });
        dialogUtil.show();

        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("web-write memo-click save button")
                .build());
    }

    // MemoVo 객체에 담는다
    @Override
    public MemoVo getMemoVo() {
        if (mMemoVo == null) {
            mMemoVo = new MemoVo();
        }

        // 제목이 비었을 경우 날짜를 제목으로 붙인다
        if (mTitleInput.getText().toString().equals("")) {
            mMemoVo.setTitle(mTitleBasic + " " + mWebBrowserMemoWritePresenter.getDate());
        } else {
            mMemoVo.setTitle(mTitleInput.getText().toString());
        }

        String content = mWebBrowserMemoWritePresenter.getMemoForSave(mContentInput.getText());
        Timber.d("save content\n" + content);
        mMemoVo.setContent(content);

        return mMemoVo;
    }

    @Override
    public String getPageUrl() {
        return mWebBrowserMemoWriteFragmentEventListener.getUrl();
    }

    @Override
    public void showProgressBar() {
        mProgressBarDialogUtil.show();
    }

    @Override
    public void hideProgressBar() {
        mProgressBarDialogUtil.cancel();
    }

    @OnClick(R.id.memowrite_close_btn)
    public void closeFragment() {
        getActivity().onBackPressed();

        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("web-write memo-click close button")
                .build());
    }

    // fragment가 기존에 있을 시에 toggleAvailable 추가 반영
    @Override
    public void setHighlightButtonEnable(boolean enable) {
        Timber.d("setHighlightButtonEnable " + enable);

        // 뷰가 그려져있지 않다면
        if (mHighlightButton == null) {
            Timber.d("setHighlightButtonEnable view is null");
            return;
        }
        else {
            mHighlightButton.setEnabled(enable);
        }

        mWebBrowserMemoWritePresenter.setHighlightState(enable);
    }

    // 하이라이트 된 메모를 edittext로 가져오는 메소드(WebBrowserActivity를 통해 WebViewFragment에서 사용)
    public void addHighlightedContents(String content) {
        Timber.d("addHighlightedContents " + content);
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getContext().
                getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);
        PicassoImageGetter picassoImageGetter = new PicassoImageGetter(mContentInput,
                getResources(), Picasso.with(getContext()), metrics.widthPixels / 2);
        Spanned htmlSpan = mWebBrowserMemoWritePresenter.
                getHtmlSpan(content, picassoImageGetter);
        int position = Selection.getSelectionStart(mContentInput.getText());
        mContentInput.getText().insert(position, htmlSpan);
    }

    // 이미지를 추가하는 메소드
    public void addImage(final String url) {
        GetOneImageSpan getOneImageSpan = new GetOneImageSpan();
        getOneImageSpan.setImageWidth(mImageWidth);
        getOneImageSpan.setListener(new GetImageSpanCallbackListener() {
            @Override
            public void onGetSpanSuccess(Spanned spanned) {
                int position = Selection.getSelectionStart(mContentInput.getText());
                mContentInput.getText().insert(position, spanned);
            }
        });
        getOneImageSpan.execute(url);
    }

    // 메모 저장 성공시 동작할 메소드
    @Override
    public void onSaveSuccess(MemoVo memoVo) {
        Toast.makeText(getActivity(), mSaved, Toast.LENGTH_SHORT).show();

        // 1. 메모쓰기 뷰를 초기화한다
        clearAllWidgets();

        // 2. 웹브라우징 모드로 전환
        mWebBrowserMemoWriteFragmentEventListener.changeModeToWebBrowsing();

        // 3. 키보드를 없앤다
        mWebBrowserMemoWriteFragmentEventListener.clearEditTextFocusAndKeyboard();
    }

    // 주소 이동시 메모 내용 지우기
    public void clearAllWidgets() {
        mTitleInput.setText("");
        mContentInput.setText("");
        mWebBrowserMemoWritePresenter.setSecret(false);
        mSecretButton.setImageDrawable(mLockOpen);
        mWebBrowserMemoWritePresenter.setModify(false);
    }

    // 메모 저장 실패시 동작할 메소드
    @Override
    public void onSaveFail() {
        Toast.makeText(getActivity(), mSaveFail, Toast.LENGTH_SHORT).show();
    }

    // 웹 뷰 모드로 변환 시 하이라이트 종료
    public void offHighlight() {
        mWebBrowserMemoWritePresenter.offHighlight(mWebBrowserMemoWriteFragmentEventListener.isHighlightAvailable());
    }
}
