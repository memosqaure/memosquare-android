package com.estsoft.memosquare.fragments;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.estsoft.memosquare.R;
import com.estsoft.memosquare.adapters.OthermemoRecyclerViewAdapter;
import com.estsoft.memosquare.listeners.GetMemoListListener;
import com.estsoft.memosquare.listeners.MainTabClipbookFragmentEventListener;
import com.estsoft.memosquare.oldmodels.MainModel;
import com.estsoft.memosquare.utils.EndlessRecyclerOnScrollListener;
import com.estsoft.memosquare.vo.MemoVo;


import java.util.ArrayList;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainTabClipbookFragment extends android.support.v4.app.Fragment {

    @BindView(R.id.recyclerview) RecyclerView mRecView;
    @BindView(R.id.swipeContainer) SwipeRefreshLayout mSwipeContainer;
    @BindView(R.id.emptyView) TextView mEmptyView;

    @BindString(R.string.loading) String mLoadingText;
    @BindString(R.string.no_memo) String mNoMemoText;
    @BindString(R.string.networkError) String mNetworkErrorText;

    private ArrayList<MemoVo> mList;
    private int mOffset;
    private OthermemoRecyclerViewAdapter mAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private EndlessRecyclerOnScrollListener mEndlessRecyclerOnScrollListener;
    private boolean mIsTop;  // 지금 스크롤이 리스트의 맨 위에 있는지 상태를 저장하는 변수

    private MainTabClipbookFragmentEventListener mMainTabClipbookFragmentEventListener;

    private MainModel mModel;

    public void setMainTabClipbookFragmentEventListener(MainTabClipbookFragmentEventListener mainTabClipbookFragmentEventListener) {
        mMainTabClipbookFragmentEventListener = mainTabClipbookFragmentEventListener;
    }

    public boolean isTop() {
        return mIsTop;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //1. inflate the layout for this fragment.
        View rootView = inflater.inflate(R.layout.fragment_showsmemolist, container, false);
        ButterKnife.bind(this, rootView);
        mModel = MainModel.getInstance();

        //2. Adapter
        mList = new ArrayList<MemoVo>();
        mAdapter = new OthermemoRecyclerViewAdapter(getActivity(), mList);

        //3. create LinearLayoutManager - it determines the time to recycle the views.
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        //4. Recyclerview setting
        mRecView.setHasFixedSize(true);
        mRecView.setAdapter(mAdapter);
        mRecView.setLayoutManager(mLinearLayoutManager);

        mEndlessRecyclerOnScrollListener = new EndlessRecyclerOnScrollListener(mLinearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                loadClippedMemos();
            }

            @Override
            public void setIsTop(boolean isTop) {
                mIsTop = isTop;
                if (mIsTop) {
                    mMainTabClipbookFragmentEventListener.setUpButtonVisibility(View.GONE);
                } else {
                    mMainTabClipbookFragmentEventListener.setUpButtonVisibility(View.VISIBLE);
                }
            }
        };

        mRecView.addOnScrollListener(mEndlessRecyclerOnScrollListener);

        //5. DB connection(Async) - get Memo List
        initializeOffset();
        loadClippedMemos();

        //6. SwipeRefreshLayout
        mSwipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // mEndlessRecyclerOnScrollListener.loading = false로 되어 있으면 onLoader가 실행된다.
                mEndlessRecyclerOnScrollListener.setLoading(true);
                mEndlessRecyclerOnScrollListener.reset();
                initializeOffset();
                mAdapter.clear();
                loadClippedMemos();
                mSwipeContainer.setRefreshing(false);
            }
        });

        return rootView;
    }

    public void loadClippedMemos(){
        if(mOffset >= 0) {
            mModel.getClippedMemos(mOffset, new GetMemoListListener() {
                @Override
                public void onGetMemoListSuccess(ArrayList<MemoVo> mMemoList, int offset) {
                    mList.addAll(mMemoList);
                    mOffset = offset;

                    if (mList.size() == 0) {
                        showNoMemoMessage();
                    } else {
                        showRecyclerViewAdapter();
                        mAdapter.setMemoList(mList);
                        mAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onGetMemoListFail() {
                    showNetworkErrorMessage();
                }
            });
        }
    }

    public void initializeOffset(){
        mOffset = 0;
    }

    public void showRecyclerViewAdapter(){
        mEmptyView.setVisibility(View.GONE);
        mRecView.setVisibility(View.VISIBLE);
    }

    public void showNetworkErrorMessage(){
        mRecView.setVisibility(View.GONE);
        mEmptyView.setVisibility(View.VISIBLE);
        mEmptyView.setText(mNetworkErrorText);
    }

    public void showNoMemoMessage(){
        mRecView.setVisibility(View.GONE);
        mEmptyView.setVisibility(View.VISIBLE);
        mEmptyView.setText(mNoMemoText);
    }

    public void goUp() {
        mRecView.scrollToPosition(3);
        mRecView.smoothScrollToPosition(0);
    }
}
