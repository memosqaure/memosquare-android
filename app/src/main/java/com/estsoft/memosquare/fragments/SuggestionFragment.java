package com.estsoft.memosquare.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.estsoft.memosquare.MemoSquare;
import com.estsoft.memosquare.R;
import com.estsoft.memosquare.listeners.SendSuggestionEventListener;
import com.estsoft.memosquare.oldmodels.MainModel;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SuggestionFragment extends Fragment {

    // 위젯 할당
    @BindView(R.id.suggestion_content) EditText mContent;

    // 리소스 할당
    @BindString(R.string.suggestion_content_empty) String mContentEmpty;
    @BindString(R.string.suggestion_send_success) String mSendSuccess;
    @BindString(R.string.suggestion_send_failed) String mSendFailed;

    private MainModel mModel;

    //Google Analytics Tracker
    Tracker mTracker;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_suggestion, container, false);
        ButterKnife.bind(this, view);

        MemoSquare application = (MemoSquare) MemoSquare.getContext();
        mTracker = application.getDefaultTracker();

        return view;
    }

    @OnClick(R.id.suggestion_send_btn)
    void sendButtonClicked(View view) {
        // 내용이 없으면 토스트 메세지만
        if (isContentEmpty()) {
            Toast.makeText(getActivity(), mContentEmpty, Toast.LENGTH_SHORT).show();
            return;
        }

        mModel.sendSuggestion(mContent.getText().toString(), new SendSuggestionEventListener() {
            @Override
            public void onSendSuggestionSuccess() {
                onSuccess();
            }
            @Override
            public void onSendSuggestionFail() {
                onFailed();
            }
        });

        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("suggestion-click send button")
                .build());

            }

    // 건의 내용이 비어있는가 확인
    private boolean isContentEmpty() {
        String content = mContent.getText().toString();
        if (content.trim().equals("")) {
            return true;
        } else {
            return false;
        }
    }

    // 전송에 성공하면 토스트 메세지 후 종료
    private void onSuccess() {
        Toast.makeText(getActivity(), mSendSuccess, Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }

    // 전송 실패하면 토스트 메세지만
    private void onFailed() {
        Toast.makeText(getActivity(), mSendFailed, Toast.LENGTH_SHORT).show();
    }
}
