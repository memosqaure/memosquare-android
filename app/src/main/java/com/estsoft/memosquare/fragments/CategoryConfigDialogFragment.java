package com.estsoft.memosquare.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.estsoft.memosquare.R;
import com.estsoft.memosquare.listeners.CategoryConfigEventListener;

/**
 * Created by hokyung on 2017. 1. 5..
 */

public class CategoryConfigDialogFragment extends DialogFragment {

    private EditText mCategoryName;
    private ImageButton mDeleteButton;

    // 콜백
    private CategoryConfigEventListener mCategoryConfigEventListener;

    public void setCategoryConfigEventListener(CategoryConfigEventListener categoryConfigEventListener) {
        mCategoryConfigEventListener = categoryConfigEventListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_category_config, null);
        mCategoryName = (EditText) view.findViewById(R.id.category_name);
        mCategoryName.setText(mCategoryConfigEventListener.getCategoryName());

        mDeleteButton = (ImageButton) view.findViewById(R.id.category_delete);
        mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCategoryConfigEventListener.deleteCategory();
            }
        });

        builder.setTitle(R.string.category_config_title)
                .setView(view)
                .setPositiveButton(R.string.category_save_btn,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // 카테고리명이 비어있을 때
                                if (mCategoryName.getText().toString().trim().equals("")) {
                                    mCategoryConfigEventListener.showNameIsEmptyToast();
                                    return;
                                }
                                mCategoryConfigEventListener.
                                        modifyCategory(mCategoryName.getText().toString());
                            }
                        })
                .setNegativeButton(R.string.category_save_cancel, null);

        return builder.create();
    }
}
