package com.estsoft.memosquare.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.estsoft.memosquare.MemoSquare;
import com.estsoft.memosquare.R;
import com.estsoft.memosquare.adapters.CommentRecyclerViewAdapter;
import com.estsoft.memosquare.listeners.AddCommentListener;
import com.estsoft.memosquare.listeners.GetCommentsListener;
import com.estsoft.memosquare.oldmodels.CommentModel;
import com.estsoft.memosquare.vo.CommentVo;
import com.estsoft.memosquare.vo.MemoVo;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;


public class CommentFragment extends android.support.v4.app.Fragment implements CommentRecyclerViewAdapter.NotifyCommentDeletedListener{

    @BindView(R.id.recyclerview) RecyclerView mRecView;
    @BindView(R.id.tv_num_comments) TextView mCommentCount;
    @BindView(R.id.et_write_comment) EditText mEditTextComment;
    @BindView(R.id.btn_save) ImageButton mBtnSave;

    private MemoVo mMemoVo;

    //comment 리스트
    private ArrayList<CommentVo> mList;
    private CommentRecyclerViewAdapter mAdapter;
    private LinearLayoutManager mLinearLayoutManager;

    private CommentModel mModel;

    //Google Analytics Tracker
    Tracker mTracker;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_comment, container, false);
        ButterKnife.bind(this, rootView);
        MemoSquare application = (MemoSquare) MemoSquare.getContext();
        mTracker = application.getDefaultTracker();

        mList = new ArrayList<CommentVo>();
        mAdapter = new CommentRecyclerViewAdapter(getActivity(), mList, this);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mRecView.setHasFixedSize(true);
        mRecView.setAdapter(mAdapter);
        mRecView.setLayoutManager(mLinearLayoutManager);

        // 메모 데이터 받아오기
        Intent intent = getActivity().getIntent();
        mMemoVo = (MemoVo) intent.getSerializableExtra("memo");

        loadComments();

        return rootView;
    }

    @OnClick(R.id.btn_save)
    void savecomment(){
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("main-click 'create memo' button")
                .build());

        createComment(mEditTextComment.getText().toString());
    }

    public void loadComments(){
        mModel.getCommentList(mMemoVo, new GetCommentsListener() {
            @Override
            public void onSuccess(ArrayList<CommentVo> commentList) {
                mList.addAll(commentList);
                mCommentCount.setText(mList.size()+"");

                if(mList.size() != 0) {
                    mAdapter.setList(mList);
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailed() {
                Timber.d("failed to load comments");
            }
        });
    }

    public void createComment(String content){
        mModel.createComment(mMemoVo, content, new AddCommentListener() {
            @Override
            public void onSuccess(CommentVo comment) {
                Timber.d(comment.toString());
                mList.add(comment);
                mAdapter.setList(mList);
                mAdapter.notifyDataSetChanged();

                mEditTextComment.getText().clear();
                mCommentCount.setText(mList.size()+"");
            }

            @Override
            public void onFailed() {
                Toast.makeText(getContext(), "Try again", Toast.LENGTH_SHORT).show();
                Timber.d("failed to create comment");
            }
        });

    }

    @Override
    public void onDeleteComment() {
        mCommentCount.setText(mList.size()+"");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mAdapter.onActivityResult(requestCode, resultCode, data);
    }
}
