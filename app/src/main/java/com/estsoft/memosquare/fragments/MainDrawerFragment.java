package com.estsoft.memosquare.fragments;

import android.app.ActivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.estsoft.memosquare.MemoSquare;
import com.estsoft.memosquare.R;
import com.estsoft.memosquare.adapters.CategoryRecyclerViewAdapter;
import com.estsoft.memosquare.database.Repository.LocalCategoryRepository;
import com.estsoft.memosquare.listeners.AddCategoryEventListener;
import com.estsoft.memosquare.listeners.MainDrawerEventListener;
import com.estsoft.memosquare.mvp.presenters.MainDrawerPresenter;
import com.estsoft.memosquare.mvp.presenters.MainDrawerPresenterImpl;
import com.estsoft.memosquare.mvp.views.MainDrawerView;
import com.estsoft.memosquare.preferences.UserPreferences;
import com.estsoft.memosquare.utils.AddCategoryDialogUtil;
import com.estsoft.memosquare.utils.CircleTransform;
import com.estsoft.memosquare.vo.CategoryVo;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.ACTIVITY_SERVICE;

public class MainDrawerFragment extends Fragment implements MainDrawerView {

    @BindView(R.id.nav_profile_image) ImageView mNavProfileImage;
    @BindView(R.id.nav_profile_name) TextView mNavProfileName;
    @BindView(R.id.category_recycler) RecyclerView mCategoryRecycler;

    @BindString(R.string.networkError) String mNetworkError;
    @BindString(R.string.default_category) String mDefaultCategoryName;

    private MainDrawerPresenter mPresenter;
    private CategoryRecyclerViewAdapter mAdapter;

    private MainDrawerEventListener mMainDrawerEventListener;

    public void setMainDrawerEventListener(MainDrawerEventListener mainDrawerEventListener) {
        mMainDrawerEventListener = mainDrawerEventListener;
    }

    //Google Analytics Tracker
    Tracker mTracker;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_drawer, container, false);
        ButterKnife.bind(this, view);

        MemoSquare application = (MemoSquare) MemoSquare.getContext();
        mTracker = application.getDefaultTracker();

        initializeMembers();

        mPresenter = new MainDrawerPresenterImpl(this);
        mPresenter.setDefaultCategoryName(mDefaultCategoryName);
        mPresenter.onCreateView();

        return view;
    }

    private void initializeMembers() {
        Picasso.with(getContext())
                .load(UserPreferences.getCurrentUser(getActivity()).getPicture_url())
                .transform(new CircleTransform())
                .into(mNavProfileImage);
        mNavProfileName.setText(UserPreferences.getCurrentUser(getActivity()).getName());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mCategoryRecycler.setHasFixedSize(true);
        mCategoryRecycler.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.onResume();
    }

    @OnClick(R.id.nav_home_btn)
    public void home(View view) {
        mMainDrawerEventListener.closeDrawer();

        ActivityManager manager = (ActivityManager) getActivity().getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfoList = manager.getRunningTasks(10);

        // Main Activity가 아니면 액티비티가 2개일 것이므로
        if (taskInfoList.get(0).numActivities == 2) {
            getActivity().finish();
        }

    }

    @OnClick(R.id.add_category_btn)
    public void addCategory(View view) {


        new AddCategoryDialogUtil(getFragmentManager(), new AddCategoryEventListener() {
            @Override
            public void add(String name) {
                addCategory(name);
            }
        }).show();
    }

    private void addCategory(String name) {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("drawer-click-add category")
                .build());

        mPresenter.addCategoryToServer(name);
    }

    @Override
    public void addCategoryToRecycler(CategoryVo categoryVo) {
        mAdapter.addCategory(categoryVo);
    }

    @OnClick(R.id.suggestion_btn)
    public void suggestion(View view) {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("drawer-click suggestion")
                .build());
        mMainDrawerEventListener.startSuggestion();
    }

    @OnClick(R.id.signout_btn)
    public void signOut(View view) {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("drawer-click sign out")
                .build());
        mMainDrawerEventListener.signOut();
    }

    @Override
    public void generateAdapterAndSetToRecView(ArrayList<CategoryVo> items) {
        mAdapter = new CategoryRecyclerViewAdapter(getActivity(), items);
        mAdapter.setMainDrawerEventListener(mMainDrawerEventListener);
        mCategoryRecycler.setAdapter(mAdapter);
    }

    @Override
    public void setCategoryListToAdapter(ArrayList<CategoryVo> items) {
        mAdapter.setCategoryList(items);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void resetLocalCategoryDb(ArrayList<CategoryVo> list) {
        LocalCategoryRepository localCategoryRepository =
                LocalCategoryRepository.getInstance(getContext());

        localCategoryRepository.clearCategories();
        for (CategoryVo categoryVo : list) {
            localCategoryRepository.addCategory(categoryVo);
        }
    }

    @Override
    public void showNetworkErrorMessage() {
        Toast.makeText(getContext(), "1" + mNetworkError, Toast.LENGTH_SHORT).show();
    }
}
