package com.estsoft.memosquare.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.estsoft.memosquare.R;
import com.estsoft.memosquare.listeners.ModifyHomepageDialogEventListener;

/**
 * Created by hokyung on 2016. 11. 30..
 */

public class ModifyHomepageDialogFragment extends DialogFragment {

    // string 리소스
    private String mBasicHomepage;
    private String mCurrentHomepage;

    // 콜백 메소드
    private ModifyHomepageDialogEventListener mModifyHomepageDialogEventListener;

    public void setModifyHomepageDialogEventListener(ModifyHomepageDialogEventListener modifyHomepageDialogEventListener) {
        mModifyHomepageDialogEventListener = modifyHomepageDialogEventListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        mBasicHomepage = getActivity().getResources().getString(R.string.modify_home_basic);
        mCurrentHomepage = getActivity().getResources().getString(R.string.modify_home_current);
        String[] items = {mBasicHomepage, mCurrentHomepage};

        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        // 기본 홈페이지 사용
                        mModifyHomepageDialogEventListener.useBasicHomepage();
                        dialog.cancel();
                        break;
                    case 1:
                        // 현재 홈페이지 사용
                        mModifyHomepageDialogEventListener.useCurrentHomepage();
                        dialog.cancel();
                        break;
                }
            }
        });

        return builder.create();
    }
}
