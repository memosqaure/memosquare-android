package com.estsoft.memosquare.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.estsoft.memosquare.R;
import com.estsoft.memosquare.adapters.BookmarkRecyclerViewAdapter;
import com.estsoft.memosquare.database.Repository.BookmarkRepository;
import com.estsoft.memosquare.listeners.BookmarkListEventListener;
import com.estsoft.memosquare.vo.BookmarkVo;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hokyung on 2016. 11. 30..
 * 즐겨찾기
 */

public class BookmarkFragment extends Fragment {

    // 위젯 할당
    @BindView(R.id.bookmark_recycler) RecyclerView mRecyclerView;

    // 콜백 리스너
    private BookmarkListEventListener mBookmarkListEventListener;

    // 그 외 변수
    private ArrayList<BookmarkVo> mList;
    private BookmarkRecyclerViewAdapter mAdapter;

    public void setBookmarkListEventListener(BookmarkListEventListener bookmarkListEventListener) {
        mBookmarkListEventListener = bookmarkListEventListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookmark_list, container, false);
        ButterKnife.bind(this, view);

        mList = (ArrayList) BookmarkRepository.getInstance(getContext()).getBookmarkList();
        mAdapter = new BookmarkRecyclerViewAdapter(getActivity(), mList);
        mAdapter.setBookmarkListEventListener(mBookmarkListEventListener);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        return view;
    }
}
