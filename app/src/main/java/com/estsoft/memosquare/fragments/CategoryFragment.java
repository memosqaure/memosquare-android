package com.estsoft.memosquare.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.estsoft.memosquare.R;
import com.estsoft.memosquare.adapters.MymemoRecyclerViewAdapter;
import com.estsoft.memosquare.listeners.MainTabMymemoFragmentEventListener;
import com.estsoft.memosquare.mvp.presenters.CategoryPresenter;
import com.estsoft.memosquare.mvp.presenters.CategoryPresenterImpl;
import com.estsoft.memosquare.mvp.views.CategorisedMemoView;
import com.estsoft.memosquare.mvp.views.MainTabMyMemoView;
import com.estsoft.memosquare.utils.EndlessRecyclerOnScrollListener;
import com.estsoft.memosquare.utils.ProgressBarDialogUtil;
import com.estsoft.memosquare.vo.CategoryVo;
import com.estsoft.memosquare.vo.MemoVo;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;


public class CategoryFragment extends Fragment implements CategorisedMemoView{

    private CategoryPresenter mPresenter;

    @BindView(R.id.recyclerview) RecyclerView mRecView;
    @BindView(R.id.swipeContainer) SwipeRefreshLayout mSwipeContainer;
    @BindView(R.id.emptyView) TextView mEmptyView;
    @BindString(R.string.no_memo) String mNoMemoText;
    @BindString(R.string.networkError) String mNetworkErrorText;
    private ProgressBarDialogUtil mProgressBarDialogUtil;

    private CategoryVo mCategoryVo;
    private LinearLayoutManager mLinearLayoutManager;
    private MymemoRecyclerViewAdapter mAdapter;
    private boolean mIsTop;

    private EndlessRecyclerOnScrollListener mEndlessRecyclerOnScrollListener;
    private MainTabMymemoFragmentEventListener mMainTabMymemoFragmentEventListener;

    public void setCategoryVo(CategoryVo categoryVo) {
        mCategoryVo = categoryVo;
    }

    public void setMainTabMymemoFragmentEventListener(MainTabMymemoFragmentEventListener mainTabMymemoFragmentEventListener) {
        mMainTabMymemoFragmentEventListener = mainTabMymemoFragmentEventListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_showsmemolist, container, false);
        ButterKnife.bind(this, view);

        initializeMembers();

        mPresenter = new CategoryPresenterImpl(this);
        mPresenter.onCreateView();

        return view;
    }

    private void initializeMembers(){
        mProgressBarDialogUtil = new ProgressBarDialogUtil(getActivity());

        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mIsTop = true;

        mEndlessRecyclerOnScrollListener = generateOnScrollListener();
        mSwipeContainer.setOnRefreshListener(generateOnSwipeRefreshListener());

        mRecView.setHasFixedSize(true);
        mRecView.setLayoutManager(mLinearLayoutManager);
        mRecView.addOnScrollListener(mEndlessRecyclerOnScrollListener);
    }

    private EndlessRecyclerOnScrollListener generateOnScrollListener(){
        return new EndlessRecyclerOnScrollListener(mLinearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                mPresenter.loadCategorisedMemos();
            }

            @Override
            public void setIsTop(boolean isTop) {
                mIsTop = isTop;
                mPresenter.setUpButtonVisibility(mIsTop);
            }
        };
    }

    private SwipeRefreshLayout.OnRefreshListener generateOnSwipeRefreshListener(){
        return new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mEndlessRecyclerOnScrollListener.setLoading(true);
                mEndlessRecyclerOnScrollListener.reset();

                mAdapter.clear();
                mPresenter.initializeOffset();
                mPresenter.loadCategorisedMemos();
                mSwipeContainer.setRefreshing(false);
            }
        };
    }

    @Override
    public int getCategoryPk() {
        return mCategoryVo.getPk();
    }

    @Override
    public void generateAdapterAndSetToRecView(ArrayList<MemoVo> items) {
        mAdapter = new MymemoRecyclerViewAdapter(getActivity(), items);
        mRecView.setAdapter(mAdapter);
    }

    @Override
    public void setIsTopAsTrue() {
        mIsTop = true;
    }

    @Override
    public boolean IsTop() {
        return mIsTop;
    }

    @Override
    public void showProgress() {
        mProgressBarDialogUtil.show();
    }

    @Override
    public void hideProgress() {
        mProgressBarDialogUtil.cancel();
    }

    @Override
    public void setMemoListToAdapter(ArrayList<MemoVo> items) {
        mAdapter.setMemoList(items);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void showRecyclerView() {
        mEmptyView.setVisibility(View.GONE);
        mRecView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showNetworkErrorMessage() {
        mRecView.setVisibility(View.GONE);
        mEmptyView.setVisibility(View.VISIBLE);
        mEmptyView.setText(mNetworkErrorText);
    }

    @Override
    public void showNoMemoMessage() {
        mRecView.setVisibility(View.GONE);
        mEmptyView.setVisibility(View.VISIBLE);
        mEmptyView.setText(mNoMemoText);
    }

    @Override
    public void goUp() {
        mRecView.scrollToPosition(3);
        mRecView.smoothScrollToPosition(0);
    }

    @Override
    public void notifyDataChangedToAdapter() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void setButtonVisibility(int visibility) {
        mMainTabMymemoFragmentEventListener.setUpButtonVisibility(visibility);
    }

    @Override
    public void removeMemo(int index) {
        mPresenter.removeCategorisedMemo(index);
    }
}
