package com.estsoft.memosquare.database.Repository;

import android.util.Log;

import com.estsoft.memosquare.database.RetrofitService.MemoService;
import com.estsoft.memosquare.database.RetrofitService.ServiceGenerator;
import com.estsoft.memosquare.enums.MemoAction;
import com.estsoft.memosquare.enums.MemoListType;
import com.estsoft.memosquare.listeners.GetMemoListListener;
import com.estsoft.memosquare.listeners.MemoSaveEventListener;
import com.estsoft.memosquare.vo.MemoListWrapper;
import com.estsoft.memosquare.vo.MemoVo;
import com.estsoft.memosquare.vo.MemoVoWrapper;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class MemoRepository {

    public static final int LIMIT = 12;

    private static MemoService mService = ServiceGenerator.createService(MemoService.class);;

    private static MemoRepository sMemoModel;
    private MemoRepository(){
        mService = ServiceGenerator.createService(MemoService.class);
    }
    public static MemoRepository getInstance() {
        if (sMemoModel == null) {
            sMemoModel = new MemoRepository();
        }
        return sMemoModel;
    }

    //MemoList얻어올 때
    public static void getMemoList(MemoListType type, int offset, GetMemoListListener mCallback) {
        //Initialize
        Call<MemoListWrapper<MemoVo>> call = null;
        switch (type) {
            case MY:
                call = mService.getMyMemos(null,LIMIT, offset);
                break;
            case CLIPPED:
                call = mService.getClippedMemos(LIMIT, offset);
                break;
        }
        enqueueListCall(mCallback, call);
    }

    //getPagememos 따로 처리
    public static void getMemoList(MemoListType type, int offset, String url, GetMemoListListener mCallback){
        if (type.equals(MemoListType.PAGE)){
        Call<MemoListWrapper<MemoVo>> call = mService.getPageMemos(LIMIT, offset, url);
        enqueueListCall(mCallback, call);
        }
    }

    //category에 해당하는 메모들
    public static void getMemoList(MemoListType type, int offset, int categoryPk, GetMemoListListener mCallback){
        if (type.equals(MemoListType.MY)){
            Call<MemoListWrapper<MemoVo>> call = mService.getCategorisedMemos(LIMIT, offset, categoryPk);
            enqueueListCall(mCallback, call);
        }
    }

    public static void enqueueListCall(final GetMemoListListener mCallback, Call<MemoListWrapper<MemoVo>> call){
        call.enqueue(new Callback<MemoListWrapper<MemoVo>>(){
            @Override
            public void onResponse(Call<MemoListWrapper<MemoVo>> call, Response<MemoListWrapper<MemoVo>> response) {
                if (response.isSuccessful()) {
                    MemoListWrapper<MemoVo> mMemoVoWrapper = response.body();

                    int offset;

                    //메모 리스트의 끝이면 offset = -1
                    if(mMemoVoWrapper.getNext()==null){
                        offset = -1;
                    }else{
                        offset=getOffsetFromText(mMemoVoWrapper.getNext());
                    }

                    ArrayList<MemoVo> mList = mMemoVoWrapper.getModellist();
                    mCallback.onGetMemoListSuccess(mList, offset);
                } else {
                    mCallback.onGetMemoListFail();
                }
            }
            @Override
            public void onFailure(Call<MemoListWrapper<MemoVo>> call, Throwable t) {
                mCallback.onGetMemoListFail();
            }
        });
    }

    public static int getOffsetFromText(String next){
        String[] array = next.split("offset=");
        String temp = array[1];
        array = temp.split("&url");
        int offset = Integer.parseInt(array[0]);
        return offset;
    }

    public static void createMemo(MemoVo memo, final MemoSaveEventListener mCallback){
        Timber.d("109:"+memo.toString());

        Call<MemoVoWrapper<MemoVo>> call = mService.createMemo(memo);
        call.enqueue(new Callback<MemoVoWrapper<MemoVo>>(){
            @Override
            public void onResponse(Call<MemoVoWrapper<MemoVo>> call, Response<MemoVoWrapper<MemoVo>> response) {
                if (response.isSuccessful()) {
                    MemoVoWrapper<MemoVo> wrapper = response.body();
                    MemoVo mMemoVo = wrapper.getMemoVo();
                    Timber.d("109 memovo: "+mMemoVo.toString());
                    mCallback.onMemoSaveSuccess(mMemoVo);
                } else {
                    Timber.d("78: ERROR:"+response.message());
                    mCallback.onMemoSaveFail();
                }
            }
            @Override
            public void onFailure(Call<MemoVoWrapper<MemoVo>> call, Throwable t) {
                Timber.d("85: Error / "+ t.getMessage());
                mCallback.onMemoSaveFail();
            }
        });
    }

    // This method  converts String to RequestBody
    public static RequestBody toRequestBody (String value) {
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), value);
        return body ;
    }

    public static void updateMemo(MemoVo memo, final MemoSaveEventListener mCallback){
        Call<MemoVoWrapper<MemoVo>> call = mService.updateMemo(memo.getPk(), memo);
        call.enqueue(new Callback<MemoVoWrapper<MemoVo>>(){
            @Override
            public void onResponse(Call<MemoVoWrapper<MemoVo>> call, Response<MemoVoWrapper<MemoVo>> response) {
                if (response.isSuccessful()) {
                    MemoVoWrapper<MemoVo> wrapper = response.body();
                    MemoVo mMemoVo = wrapper.getMemoVo();
                    Timber.d("132 memovo: "+mMemoVo.toString());
                    mCallback.onMemoSaveSuccess(mMemoVo);
                } else {
                    Timber.d("78: ERROR:"+response.message());
                    mCallback.onMemoSaveFail();
                }
            }
            @Override
            public void onFailure(Call<MemoVoWrapper<MemoVo>> call, Throwable t) {
                Timber.d("85: Error / "+ t.getMessage());
                mCallback.onMemoSaveFail();
            }
        });

    }

    //Async의 onResponse에서 아무것도 하지 않는 request들
    public static void requestOnResponseEmpty(MemoVo memo, MemoAction action){
        Call call;
        switch (action){
            case DELETE:
                call = mService.deleteMemo(memo.getPk());
                MemoRepository.<ResponseBody>enqueueCall(call);
                break;
            case CLIP:
                call = mService.increaseclip(memo.getPk());
                MemoRepository.<MemoVo>enqueueCall(call);
                break;
            case NOTCLIP:
                call = mService.decreaseclip(memo.getPk());
                MemoRepository.<MemoVo>enqueueCall(call);
                break;
            case LOCKTOGGLE:
                call = mService.lockToggle(memo.getPk());
                MemoRepository.<String>enqueueCall(call);
                break;
            case LIKE:
                call = mService.likeMemo(memo.getPk());
                MemoRepository.<String>enqueueCall(call);
                break;
            case NOTLIKE:
                call = mService.notLikeMemo(memo.getPk());
                MemoRepository.<String>enqueueCall(call);
                break;
        }
    }

    public static <T> void enqueueCall(Call call){
        call.enqueue(new Callback<T>(){
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                if (response.isSuccessful()) {
                    Timber.d("RESPONSE: "+response.code()+response.message());
                    if(response.body()!= null){
                        Timber.d("RESPONSEBODY: "+response.body().toString());
                    }
                } else {
                    Timber.d("ERROR"+response.message());
                }
            }
            @Override
            public void onFailure(Call<T> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }
}
