package com.estsoft.memosquare.database.RetrofitService;

import com.estsoft.memosquare.vo.CategoryListWrapper;
import com.estsoft.memosquare.vo.CategoryVo;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface CategoryService {

    @POST("category/")
    Call<CategoryVo> addCategory(@Body CategoryVo categoryVo);

    @GET("category/")
    Call<CategoryListWrapper<CategoryVo>> getCategoryList();

    @POST("category/{pk}/")
    Call<CategoryVo> updateCategory(@Path("pk") int pk, @Body CategoryVo categoryVo);

    @DELETE("category/{pk}/")
    Call<ResponseBody> removeCategory(@Path("pk") int pk);
}
