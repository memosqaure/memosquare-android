package com.estsoft.memosquare.database.RetrofitService;

import com.estsoft.memosquare.vo.MemoListWrapper;
import com.estsoft.memosquare.vo.MemoVo;
import com.estsoft.memosquare.vo.MemoVoWrapper;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MemoService {

    //MEMO CRUD
    @POST("memo/")
    Call<MemoVoWrapper<MemoVo>> createMemo(@Body MemoVo Memo);

    @POST("memo/{pk}/")
    Call<MemoVoWrapper<MemoVo>> updateMemo(@Path("pk") int pk, @Body MemoVo memo);

    @DELETE("memo/{pk}/")
    Call<ResponseBody> deleteMemo(@Path("pk") int pk);

    //MEMOLISTS
    @GET("memo/")
    Call<MemoListWrapper<MemoVo>> getMyMemos(@Query("category_pk") Integer categoryPK, @Query("limit") int limit, @Query("offset") int offset);

    @GET("memo/")
    Call<MemoListWrapper<MemoVo>> getCategorisedMemos(@Query("limit") int limit, @Query("offset") int offset, @Query("category_pk") int categoryId);
    
    @GET("memo/clipbook")
    Call<MemoListWrapper<MemoVo>> getClippedMemos(@Query("limit") int limit, @Query("offset") int offset);

    @GET("memo/page/")
    Call<MemoListWrapper<MemoVo>> getPageMemos(@Query("limit") int limit, @Query("offset") int offset, @Query("url") String url);

    //CLIP/UNCLIP & PRIVATE/PUBLIC
    @POST("memo/{pk}/clip/")
    Call<MemoVo> increaseclip(@Path("pk") int pk);

    @DELETE("memo/{pk}/clip/")
    Call<MemoVo> decreaseclip(@Path("pk") int pk);

    @POST("memo/{pk}/lock/")
    Call<String> lockToggle(@Path("pk") int pk);

    @POST("memo/{pk}/like/")
    Call<ResponseBody> likeMemo(@Path("pk") int pk);

    @DELETE("memo/{pk}/like/")
    Call<ResponseBody> notLikeMemo(@Path("pk") int pk);

}
