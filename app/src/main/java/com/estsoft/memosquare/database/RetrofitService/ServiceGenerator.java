package com.estsoft.memosquare.database.RetrofitService;

import android.util.Log;

import com.estsoft.memosquare.R;
import com.estsoft.memosquare.preferences.AddCookiesInterceptor;
import com.estsoft.memosquare.preferences.ReceivedCookiesInterceptor;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    public static final String API_BASE_URL = "http://estfutures.cafe24.com/";
//    public static final String API_BASE_URL = "http://memo-square.com/";

    //httpclient에 cookie를 위한 interceptor 달아줌.
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder().
            addInterceptor(new AddCookiesInterceptor()).
            addInterceptor(new ReceivedCookiesInterceptor()).
            addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY
            ));

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    //httpClient를 REST API로 생성해준다. (하나의 service이자 하나의 client가 된다)
    public static <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }
}


