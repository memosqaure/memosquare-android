package com.estsoft.memosquare.database.Repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.estsoft.memosquare.vo.BookmarkVo;

import java.util.ArrayList;
import java.util.List;

import static com.estsoft.memosquare.database.Repository.BookmarkRepository.BookmarkDbSchema.*;

/**
 * Created by hokyung on 2016. 11. 30..
 */

public class BookmarkRepository {

    private static BookmarkRepository sBookmarkRepository;
    private Context mContext;
    private SQLiteDatabase mDatabase;

    private BookmarkRepository(Context context) {
        mContext = context.getApplicationContext();
        mDatabase = new BookmarkBaseHelper(mContext).getWritableDatabase();
    }

    public static BookmarkRepository getInstance(Context context) {
        if (sBookmarkRepository == null) {
            sBookmarkRepository = new BookmarkRepository(context);
        }
        return sBookmarkRepository;
    }

    public void addBookmark(BookmarkVo bookmarkVo) {
        ContentValues contentValues = getContentValues(bookmarkVo);
        mDatabase.insert(BookmarkTable.NAME, null, contentValues);
    }

    public BookmarkVo getBookmark(String url) {
        BookmarkCursorWrapper cursorWrapper = queryBookmarks(BookmarkTable.Cols.URL + " = ? ",
                new String[] {url}, null);

        try {
            if (cursorWrapper.getCount() == 0) {
                return null;
            }
            cursorWrapper.moveToFirst();
            return cursorWrapper.getBookmark();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            cursorWrapper.close();
        }
    }

    public void deleteBookmark(BookmarkVo bookmarkVo) {
        mDatabase.delete(BookmarkTable.NAME, "_id = ? ", new String[]{bookmarkVo.getId()});
    }

    public List<BookmarkVo> getBookmarkList() {
        List<BookmarkVo> bookmarkVos = new ArrayList<>();

        BookmarkCursorWrapper cursorWrapper = queryBookmarks(null, null, null);

        try {
            cursorWrapper.moveToFirst();
            while (!cursorWrapper.isAfterLast()) {
                bookmarkVos.add(cursorWrapper.getBookmark());
                cursorWrapper.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursorWrapper.close();
        }

        return bookmarkVos;
    }

    private static ContentValues getContentValues(BookmarkVo bookmarkVo) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(BookmarkTable.Cols.NAME, bookmarkVo.getName());
        contentValues.put(BookmarkTable.Cols.URL, bookmarkVo.getUrl());

        return contentValues;
    }

    private BookmarkCursorWrapper queryBookmarks(String whereClause, String[] whereArgs,
                                                 String orderBy) {
        Cursor cursor = mDatabase.query(
                BookmarkTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                orderBy,
                null
        );
        return new BookmarkCursorWrapper(cursor);
    }

    private class BookmarkBaseHelper extends SQLiteOpenHelper {
        private static final int VERSION = 1;
        private static final String DATABASE_NAME = "bookmarkBase.db";

        public BookmarkBaseHelper(Context context) {
            super(context, DATABASE_NAME, null, VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table " + BookmarkTable.NAME + "("
                    + " _id integer primary key autoincrement, "
                    + BookmarkTable.Cols.NAME + ", "
                    + BookmarkTable.Cols.URL
                    + ")"
            );
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }

    private class BookmarkCursorWrapper extends CursorWrapper {
        public BookmarkCursorWrapper(Cursor cursor) {
            super(cursor);
        }

        public BookmarkVo getBookmark() {
            String id = getString(getColumnIndex("_id"));
            String name = getString(getColumnIndex(BookmarkTable.Cols.NAME));
            String url = getString(getColumnIndex(BookmarkTable.Cols.URL));

            BookmarkVo bookmarkVo = new BookmarkVo();
            bookmarkVo.setId(id);
            bookmarkVo.setName(name);
            bookmarkVo.setUrl(url);

            return bookmarkVo;
        }
    }

    public static class BookmarkDbSchema {
        public static final class BookmarkTable {
            public static final String NAME = "bookmarks";

            public static final class Cols {
                public static final String NAME = "name";
                public static final String URL = "url";
            }
        }
    }
}
