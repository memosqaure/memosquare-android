package com.estsoft.memosquare.database.RetrofitService;

import com.estsoft.memosquare.vo.CommentListWrapper;
import com.estsoft.memosquare.vo.CommentVo;
import com.estsoft.memosquare.vo.MemoVo;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CommentService {

    //create
    @FormUrlEncoded
    @POST("comment/")
    Call<CommentVo> addComment(@Field("memo_pk") int memo_pk, @Field("content") String content);

    //read-get comments list of a memo
    @GET("comment/")
    Call<CommentListWrapper<CommentVo>> getCommentList(@Query("memo_pk") int memoPK);

    //update
    @FormUrlEncoded
    @POST("comment/{pk}/")
    Call<CommentVo> updateComment(@Path("pk") int commentPK, @Field("content") String content);

    //delete
    @DELETE("comment/{pk}/")
    Call<ResponseBody> removeComment(@Path("pk") int commentPK);
}
