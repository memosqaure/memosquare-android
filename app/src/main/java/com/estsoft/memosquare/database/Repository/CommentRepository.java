package com.estsoft.memosquare.database.Repository;

import com.estsoft.memosquare.database.RetrofitService.CommentService;
import com.estsoft.memosquare.database.RetrofitService.ServiceGenerator;
import com.estsoft.memosquare.listeners.AddCommentListener;
import com.estsoft.memosquare.listeners.DeleteCommentListener;
import com.estsoft.memosquare.listeners.GetCommentsListener;
import com.estsoft.memosquare.listeners.UpdateCommentListener;
import com.estsoft.memosquare.vo.CommentListWrapper;
import com.estsoft.memosquare.vo.CommentVo;
import com.estsoft.memosquare.vo.MemoVo;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class CommentRepository {

    private static CommentRepository sCommentRepository;
    private CommentService mCommentService;

    private CommentRepository() {
        mCommentService = ServiceGenerator.createService(CommentService.class);
    }

    public static CommentRepository getInstance() {
        if (sCommentRepository == null) {
            sCommentRepository = new CommentRepository();
        }
        return sCommentRepository;
    }


    public void addComment(MemoVo memoVo, String content, final AddCommentListener listener) {
        Call<CommentVo> call = mCommentService.addComment(memoVo.getPk(), content);
        call.enqueue(new Callback<CommentVo>() {
            @Override
            public void onResponse(Call<CommentVo> call, Response<CommentVo> response) {
                CommentVo comment = response.body();
                if (comment != null)
                    listener.onSuccess(comment);
            }

            @Override
            public void onFailure(Call<CommentVo> call, Throwable t) {
                listener.onFailed();
            }
        });
    }

    public void getCommentList(MemoVo memo, final GetCommentsListener listener) {
        Call<CommentListWrapper<CommentVo>> call = mCommentService.getCommentList(memo.getPk());
        call.enqueue(new Callback<CommentListWrapper<CommentVo>>() {
            @Override
            public void onResponse(Call<CommentListWrapper<CommentVo>> call, Response<CommentListWrapper<CommentVo>> response) {
                ArrayList<CommentVo> list = response.body().getCommentList();
                if (list != null)
                    listener.onSuccess(list);
            }

            @Override
            public void onFailure(Call<CommentListWrapper<CommentVo>> call, Throwable t) {
                listener.onFailed();
            }
        });

    }

    public void updateComment(CommentVo comment, String content, final UpdateCommentListener listener) {
        Call<CommentVo> call = mCommentService.updateComment(comment.getPk(), content);
        call.enqueue(new Callback<CommentVo>() {
            @Override
            public void onResponse(Call<CommentVo> call, Response<CommentVo> response) {
                listener.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<CommentVo> call, Throwable t) {
                listener.onFailed();
            }
        });
    }

    public void removeComment(CommentVo comment, final DeleteCommentListener listener) {
        Call<ResponseBody> call = mCommentService.removeComment(comment.getPk());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                listener.onSuccess();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Timber.d("remove failed");
            }
        });
    }
}


