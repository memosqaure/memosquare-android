package com.estsoft.memosquare.database.RetrofitService;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface UtilService {

    @FormUrlEncoded
    @POST("report/")
    Call<ResponseBody> sendSuggestion(@Field("content") String content);

}
