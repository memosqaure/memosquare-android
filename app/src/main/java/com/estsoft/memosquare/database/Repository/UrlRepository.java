package com.estsoft.memosquare.database.Repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.estsoft.memosquare.vo.UrlVo;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

import static com.estsoft.memosquare.database.Repository.UrlRepository.UrlDbSchema.UrlTable;

/**
 * Created by hokyung on 2016. 11. 22..
 * 사이트 주소를 저장하기 위한 sqlite 클래스
 */

public class UrlRepository {

    private static UrlRepository sUrlRepository;
    private Context mContext;
    private SQLiteDatabase mDatabase;

    private UrlRepository(Context context) {
        mContext = context.getApplicationContext();
        mDatabase = new UrlBaseHelper(mContext).getWritableDatabase();
    }

    public static UrlRepository getInstance(Context context) {
        if (sUrlRepository == null) {
            sUrlRepository = new UrlRepository(context);
        }
        return sUrlRepository;
    }

    public void addUrl(UrlVo urlVo) {
        Timber.d("addUrl " + urlVo);
        ContentValues contentValues = getContentValues(urlVo);
        mDatabase.insert(UrlTable.NAME, null, contentValues);
    }

    public void updateUrl(UrlVo urlVo) {
        Timber.d("updateUrl " + urlVo);
        String id = urlVo.getId();
        ContentValues contentValues = getContentValues(urlVo);

        mDatabase.update(UrlTable.NAME, contentValues, "_id = ?", new String[] {id});
    }

    // 해당 주소 값을 가진 객체를 반환
    public UrlVo getUrl(String url) {
        Timber.d("getUrl " + url);

        UrlCursorWrapper cursorWrapper = queryUrls(UrlTable.Cols.URL + " = ? ", new String[] {url},
                null);

        try {
            if (cursorWrapper.getCount() == 0){
                return null;
            }
            cursorWrapper.moveToFirst();
            return cursorWrapper.getUrl();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            cursorWrapper.close();
        }
    }

    public List<UrlVo> getUrlList() {
        Timber.d("getUrlList");
        List<UrlVo> urlVos = new ArrayList<>();

        UrlCursorWrapper cursorWrapper = queryUrls(null, null, UrlTable.Cols.COUNT + " desc");

        try {
            cursorWrapper.moveToFirst();
            while (!cursorWrapper.isAfterLast()) {
                urlVos.add(cursorWrapper.getUrl());
                cursorWrapper.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursorWrapper.close();
        }

        return urlVos;
    }

    private static ContentValues getContentValues(UrlVo urlVo) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(UrlTable.Cols.TITLE, urlVo.getTitle().toString());
        contentValues.put(UrlTable.Cols.URL, urlVo.getUrl().toString());
        contentValues.put(UrlTable.Cols.COUNT, urlVo.getCount());

        return contentValues;
    }

    private UrlCursorWrapper queryUrls(String whereClause, String[] whereArgs, String orderBy) {
        Cursor cursor = mDatabase.query(
                UrlTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                orderBy,
                null
        );
        return new UrlCursorWrapper(cursor);
    }

    private class UrlBaseHelper extends SQLiteOpenHelper {
        private static final int VERSION = 1;
        private static final String DATABASE_NAME = "urlBase.db";

        public UrlBaseHelper(Context context) {
            super(context, DATABASE_NAME, null, VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table " + UrlTable.NAME + "("
                    + " _id integer primary key autoincrement, "
                    + UrlTable.Cols.TITLE + ", "
                    + UrlTable.Cols.URL + ", "
                    + UrlTable.Cols.COUNT
                    + ")"
            );
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }

    private class UrlCursorWrapper extends CursorWrapper {
        public UrlCursorWrapper(Cursor cursor) {
            super(cursor);
        }

        public UrlVo getUrl() {
            String id = getString(getColumnIndex("_id"));
            String title = getString(getColumnIndex(UrlTable.Cols.TITLE));
            String url = getString(getColumnIndex(UrlTable.Cols.URL));
            int count = getInt(getColumnIndex(UrlTable.Cols.COUNT));

            UrlVo urlVo = new UrlVo();
            urlVo.setId(id);
            urlVo.setTitle(title);
            urlVo.setUrl(url);
            urlVo.setCount(count);

            return urlVo;
        }
    }

    public static class UrlDbSchema {
        public static final class UrlTable {
            public static final String NAME = "urls";

            public static final class Cols {
                public static final String TITLE = "title";
                public static final String URL = "url";
                public static final String COUNT = "count";
            }
        }
    }
}
