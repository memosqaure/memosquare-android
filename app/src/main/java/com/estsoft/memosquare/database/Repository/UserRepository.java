package com.estsoft.memosquare.database.Repository;

import com.estsoft.memosquare.database.RetrofitService.ServiceGenerator;
import com.estsoft.memosquare.database.RetrofitService.UserService;
import com.estsoft.memosquare.listeners.SignInServerListener;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by sun on 2016-11-11.
 */

public class UserRepository {

    private static UserService mService = ServiceGenerator.createService(UserService.class);

    private static UserRepository sUserRepository;

    private UserRepository(){ }

    public static UserRepository getInstance() {
        if (sUserRepository == null) {
            sUserRepository = new UserRepository();
        }
        return sUserRepository;
    }

    public static void signInServer(String token, final SignInServerListener callback){
        //send token to backend server & get cookie
        Call<ResponseBody> call = mService.signIn(token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    Timber.d("server conn success: "+response.message());
                    callback.onSignInServerSuccess();
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Timber.d("server conn fail: "+t.getMessage());
                callback.onSignInServerFail();
            }
        });
    }
}
