package com.estsoft.memosquare.database.RetrofitService;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by sun on 2016-10-31.
 */

public interface UserService {

    @FormUrlEncoded
    @POST("sign_in/")
    Call<ResponseBody> signIn(@Field("token") String token);


}
