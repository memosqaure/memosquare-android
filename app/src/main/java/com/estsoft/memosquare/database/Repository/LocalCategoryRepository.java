package com.estsoft.memosquare.database.Repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.estsoft.memosquare.vo.CategoryVo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hokyung on 2017. 1. 6..
 */

public class LocalCategoryRepository {

    private static LocalCategoryRepository sLocalCategoryRepository;
    private Context mContext;
    private SQLiteDatabase mDatabase;

    private LocalCategoryRepository(Context context) {
        mContext = context;
        mDatabase = new CategoryBaseHelper(mContext).getWritableDatabase();
    }

    public static LocalCategoryRepository getInstance(Context context) {
        if (sLocalCategoryRepository == null) {
            sLocalCategoryRepository = new LocalCategoryRepository(context);
        }
        return sLocalCategoryRepository;
    }

    public void addCategory(CategoryVo categoryVo) {
        ContentValues contentValues = getContentValues(categoryVo);
        mDatabase.insert(CategoryDbSchema.CategoryTable.NAME, null, contentValues);
    }

    public List<CategoryVo> getCategoryList() {
        List<CategoryVo> list = new ArrayList<>();

        CategoryCursorWrapper cursorWrapper = queryCategories(null, null, null);

        try {
            cursorWrapper.moveToFirst();
            while (!cursorWrapper.isAfterLast()) {
                list.add(cursorWrapper.getCategory());
                cursorWrapper.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursorWrapper.close();
        }

        return list;
    }

    public void clearCategories() {
        mDatabase.delete(CategoryDbSchema.CategoryTable.NAME, null, null);
    }

    private static ContentValues getContentValues(CategoryVo categoryVo) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CategoryDbSchema.CategoryTable.Cols.PK, categoryVo.getPk());
        contentValues.put(CategoryDbSchema.CategoryTable.Cols.NAME, categoryVo.getName());

        return contentValues;
    }

    private CategoryCursorWrapper queryCategories(String whereClause, String[] whereArgs,
                                                  String orderBy) {
        Cursor cursor = mDatabase.query(
                CategoryDbSchema.CategoryTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                orderBy,
                null
        );
        return new CategoryCursorWrapper(cursor);
    }

    private class CategoryBaseHelper extends SQLiteOpenHelper {
        private static final int VERSION = 1;
        private static final String DATABASE_NAME = "categoryBase.db";

        public CategoryBaseHelper(Context context) {
            super(context, DATABASE_NAME, null, VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table " + CategoryDbSchema.CategoryTable.NAME + "("
                    + CategoryDbSchema.CategoryTable.Cols.PK + ", "
                    + CategoryDbSchema.CategoryTable.Cols.NAME
                    + ")"
            );
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }

    private class CategoryCursorWrapper extends CursorWrapper {
        public CategoryCursorWrapper(Cursor cursor) {
            super(cursor);
        }

        public CategoryVo getCategory() {
            int pk = getInt(getColumnIndex(CategoryDbSchema.CategoryTable.Cols.PK));
            String name = getString(getColumnIndex(CategoryDbSchema.CategoryTable.Cols.NAME));

            CategoryVo categoryVo = new CategoryVo();
            categoryVo.setPk(pk);
            categoryVo.setName(name);

            return categoryVo;
        }
    }

    public static class CategoryDbSchema {
        public static final class CategoryTable {
            public static final String NAME = "categories";

            public static final class Cols {
                public static final String PK = "pk";
                public static final String NAME = "name";
            }
        }
    }
}
