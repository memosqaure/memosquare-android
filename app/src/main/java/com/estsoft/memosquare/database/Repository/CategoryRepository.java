package com.estsoft.memosquare.database.Repository;

import com.estsoft.memosquare.database.RetrofitService.CategoryService;
import com.estsoft.memosquare.database.RetrofitService.ServiceGenerator;
import com.estsoft.memosquare.mvp.models.MainDrawerModel;
import com.estsoft.memosquare.vo.CategoryListWrapper;
import com.estsoft.memosquare.vo.CategoryVo;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class CategoryRepository {
    private static CategoryRepository sCategoryRepository;
    private CategoryService mCategoryService;

    private CategoryRepository() {
        mCategoryService = ServiceGenerator.createService(CategoryService.class);
    }

    public static CategoryRepository getInstance() {
        if (sCategoryRepository == null) {
            sCategoryRepository = new CategoryRepository();
        }
        return sCategoryRepository;
    }

    public void addCategory(final CategoryVo categoryVo, final MainDrawerModel.MainDrawerModelListener listener) {
        Call<CategoryVo> call = mCategoryService.addCategory(categoryVo);
        call.enqueue(new Callback<CategoryVo>() {
            @Override
            public void onResponse(Call<CategoryVo> call, Response<CategoryVo> response) {
                CategoryVo category = response.body();
                if (category != null)
                    listener.onSaveSuccess(category);
            }

            @Override
            public void onFailure(Call<CategoryVo> call, Throwable t) {
                listener.onFailed();
            }
        });
    }

    public void getList(final MainDrawerModel.MainDrawerModelListener listener) {
        Call<CategoryListWrapper<CategoryVo>> call = mCategoryService.getCategoryList();
        call.enqueue(new Callback<CategoryListWrapper<CategoryVo>>() {
            @Override
            public void onResponse(Call<CategoryListWrapper<CategoryVo>> call, Response<CategoryListWrapper<CategoryVo>> response) {
                ArrayList<CategoryVo> list = response.body().getCategoryList();
                if (list != null)
                    listener.onGetCategoryListSuccess(list);
            }

            @Override
            public void onFailure(Call<CategoryListWrapper<CategoryVo>> call, Throwable t) {
                listener.onFailed();
            }
        });
    }

    public void updateCategory(CategoryVo categoryVo,
                               final MainDrawerModel.MainDrawerModelListener listener) {
        Call<CategoryVo> call = mCategoryService.updateCategory(categoryVo.getPk(), categoryVo);
        call.enqueue(new Callback<CategoryVo>() {
            @Override
            public void onResponse(Call<CategoryVo> call, Response<CategoryVo> response) {
                listener.onUpdateSuccess(response.body());
            }

            @Override
            public void onFailure(Call<CategoryVo> call, Throwable t) {
                listener.onFailed();
            }
        });
    }

    public void removeCategory(CategoryVo categoryVo,
                               final MainDrawerModel.MainDrawerModelListener listener) {
        Call<ResponseBody> call = mCategoryService.removeCategory(categoryVo.getPk());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                listener.onRemoveSuccess();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Timber.d("remove failed");
            }
        });
    }
}
