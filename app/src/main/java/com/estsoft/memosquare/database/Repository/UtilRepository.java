package com.estsoft.memosquare.database.Repository;

import com.estsoft.memosquare.database.RetrofitService.ServiceGenerator;
import com.estsoft.memosquare.database.RetrofitService.UtilService;
import com.estsoft.memosquare.listeners.SendSuggestionEventListener;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sun on 2016-11-24.
 */

public class UtilRepository {
    private static UtilService mService = ServiceGenerator.createService(UtilService.class);

    private static UtilRepository mUtilRepository;

    private UtilRepository(){ }

    public static UtilRepository getInstance() {
        if (mUtilRepository == null) {
            mUtilRepository = new UtilRepository();
        }
        return mUtilRepository;
    }

    public static void sendSuggestion(String content, final SendSuggestionEventListener callback){
        Call<ResponseBody> call = mService.sendSuggestion(content);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                callback.onSendSuggestionSuccess();
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callback.onSendSuggestionFail();
            }
        });
    }
}
