package com.estsoft.memosquare.utils;

import android.support.v4.app.FragmentManager;

import com.estsoft.memosquare.fragments.ModifyHomepageDialogFragment;
import com.estsoft.memosquare.listeners.ModifyHomepageDialogEventListener;

/**
 * Created by hokyung on 2016. 11. 30..
 */

public class ModifyHomepageDialogUtil {

    // memo close dialog 상수
    private static final String DIALOG_MODIFY_HOMEPAGE = "ModifyHomepage";

    private FragmentManager mFragmentManager;
    private ModifyHomepageDialogFragment mDialog;
    private ModifyHomepageDialogEventListener mModifyHomepageDialogEventListener;

    public ModifyHomepageDialogUtil(FragmentManager fragmentManager, ModifyHomepageDialogEventListener modifyRemoveEventListener) {
        mFragmentManager = fragmentManager;
        mModifyHomepageDialogEventListener = modifyRemoveEventListener;
    }

    public void show() {
        mDialog = new ModifyHomepageDialogFragment();
        mDialog.setModifyHomepageDialogEventListener(mModifyHomepageDialogEventListener);
        mDialog.show(mFragmentManager, DIALOG_MODIFY_HOMEPAGE);
    }
}
