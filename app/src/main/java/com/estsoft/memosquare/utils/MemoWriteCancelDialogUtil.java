package com.estsoft.memosquare.utils;

import android.support.v4.app.FragmentManager;

import com.estsoft.memosquare.fragments.MemoWriteCancelDialogFragment;
import com.estsoft.memosquare.listeners.MemoWriteCancelEventListener;

/**
 * Created by hokyung on 2016. 11. 14..
 */

public class MemoWriteCancelDialogUtil {

    // memo close dialog 상수
    private static final String DIALOG_CANCEL_MEMO_WRITE = "CancelMemoWrite";

    private FragmentManager mFragmentManager;
    private MemoWriteCancelDialogFragment mDialog;
    private MemoWriteCancelEventListener mMemoWriteCancelEventListener;

    public MemoWriteCancelDialogUtil(FragmentManager fragmentManager
            , MemoWriteCancelEventListener memoWriteCancelEventListener) {
        mFragmentManager = fragmentManager;
        mMemoWriteCancelEventListener = memoWriteCancelEventListener;
    }

    public void show() {
        mDialog = new MemoWriteCancelDialogFragment();
        mDialog.setMemoWriteCancelEventListener(mMemoWriteCancelEventListener);
        mDialog.show(mFragmentManager, DIALOG_CANCEL_MEMO_WRITE);
    }
}
