package com.estsoft.memosquare.utils;

/**
 * Created by hokyung on 2016. 12. 19..
 * 태그들을 다루기 위한 유틸 클래스
 */

public class HtmlTagUtil {

    private static final String mLeftOfImgTag = "<img src=\"";
    private static final String mRightOfImgTag = "\">";
    private static final String mSealedLeftOfImgTag = "aaABCDEFGHIJKLMNOPQRSbbTUVWXYZYXWVUTSRQPONMLKJIHGFEDCBA";
    private static final String mSealedRightOfImgTag = "ddABCDEFGHIJKLMNOPQRSTUVWXYZYXWVUTSRQPONMLKJIHGFEDCBAabcab";

    public static String convertBrToNewLine(String content) {
        return content.replaceAll("</?br>", "\n");
    }

    public static String convertNewLineToBr(String content) {
        return content.replaceAll("\n", "<br>");
    }

    public static String sealImgTags(String content) {
        content = content.replaceAll(mLeftOfImgTag, mSealedLeftOfImgTag);
        content = content.replaceAll(mRightOfImgTag, mSealedRightOfImgTag);
        return content;
    }

    public static String unsealImgTags(String content) {
        content = content.replaceAll(mSealedLeftOfImgTag, mLeftOfImgTag);
        content = content.replaceAll(mSealedRightOfImgTag, mRightOfImgTag);
        return content;
    }

    public static String convertPToBr(String content) {
        return content.replaceAll("<p.*?>", "").replaceAll("</p>", "<br>");
    }

    public static String trimSerialBrP(String content) {
        return content.replaceAll("</?br></p>", "</p>");
    }

    public static String removeAllTags(String content) {
        return content.replaceAll("<.*?>", "");
    }

    public static String removeEncodedAngleBrackets(String content) {
        return content.replaceAll("&lt;.*?&gt;", "");
    }

    public static String convertToPlainText(String content) {
        content = convertPToBr(content);
        content = convertBrToNewLine(content);
        content = removeAllTags(content);
        content = removeEncodedAngleBrackets(content);
        return content;
    }

    public static String removeUnderLine(String content) {
        return content.replaceAll("</?u>", "");
    }
}
