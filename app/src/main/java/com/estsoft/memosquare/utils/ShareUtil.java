package com.estsoft.memosquare.utils;

import android.net.Uri;

import com.estsoft.memosquare.vo.MemoVo;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import timber.log.Timber;

public class ShareUtil {

    public static void shareToFacebook(ShareDialog shareDialog, MemoVo mMemoVo){
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle(HtmlTagUtil.convertToPlainText(mMemoVo.getTitle()))
                    .setContentDescription(HtmlTagUtil.convertToPlainText(mMemoVo.getContent()))
//                            .setImageUrl(Uri.parse(mFbShareImgUri))
                    .setContentUrl(Uri.parse("http://memo-square.com/memo/"+mMemoVo.getPk()+"/"))
                    .build();

            shareDialog.show(linkContent);
        }


    }
}
