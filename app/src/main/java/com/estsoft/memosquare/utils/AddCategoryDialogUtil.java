package com.estsoft.memosquare.utils;

import android.support.v4.app.FragmentManager;

import com.estsoft.memosquare.fragments.AddCategoryDialogFragment;
import com.estsoft.memosquare.listeners.AddCategoryEventListener;

/**
 * Created by hokyung on 2016. 12. 20..
 */

public class AddCategoryDialogUtil {

    // memo close dialog 상수
    private static final String DIALOG_ADD_CATEGORY = "AddCategory";

    // 콜백 메서드
    private AddCategoryEventListener mAddCategoryEventListener;

    private FragmentManager mFragmentManager;
    private AddCategoryDialogFragment mDialog;

    public AddCategoryDialogUtil(FragmentManager fragmentManager, AddCategoryEventListener addCategoryEventListener) {
        mFragmentManager = fragmentManager;
        mAddCategoryEventListener = addCategoryEventListener;
    }

    public void show() {
        mDialog = new AddCategoryDialogFragment();
        mDialog.setAddCategoryEventListener(mAddCategoryEventListener);
        mDialog.show(mFragmentManager, DIALOG_ADD_CATEGORY);
    }
}
