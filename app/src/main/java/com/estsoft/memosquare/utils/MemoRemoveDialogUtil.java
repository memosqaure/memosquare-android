package com.estsoft.memosquare.utils;

import android.support.v4.app.FragmentManager;

import com.estsoft.memosquare.listeners.MemoRemoveEventListener;
import com.estsoft.memosquare.fragments.MemoRemoveDialogFragment;

/**
 * Created by hokyung on 2016. 11. 14..
 */

public class MemoRemoveDialogUtil {

    // memo close dialog 상수
    private static final String DIALOG_MEMO_REMOVE = "RemoveMemo";

    // 콜백 메서드
    private MemoRemoveEventListener mMemoRemoveEventListener;

    private FragmentManager mFragmentManager;
    private MemoRemoveDialogFragment mDialog;

    public MemoRemoveDialogUtil(FragmentManager fragmentManager, MemoRemoveEventListener memoRemoveEventListener) {
        mFragmentManager = fragmentManager;
        mMemoRemoveEventListener = memoRemoveEventListener;
    }

    public void show() {
        mDialog = new MemoRemoveDialogFragment();
        mDialog.setMemoRemoveEventListener(mMemoRemoveEventListener);
        mDialog.show(mFragmentManager, DIALOG_MEMO_REMOVE);
    }
}
