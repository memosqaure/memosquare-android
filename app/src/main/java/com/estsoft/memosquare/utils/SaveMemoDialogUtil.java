package com.estsoft.memosquare.utils;

import android.support.v4.app.FragmentManager;

import com.estsoft.memosquare.fragments.SaveMemoDialogFragment;
import com.estsoft.memosquare.listeners.SaveMemoButtonEventListener;


public class SaveMemoDialogUtil {

    // memo close dialog 상수
    private static final String DIALOG_SAVE_MEMO = "SaveMemo";

    // 콜백 메서드
    private SaveMemoButtonEventListener mSaveMemoButtonEventListener;

    private FragmentManager mFragmentManager;
    private SaveMemoDialogFragment mDialog;

    public SaveMemoDialogUtil(FragmentManager fragmentManager, SaveMemoButtonEventListener addCategoryEventListener) {
        mFragmentManager = fragmentManager;
        mSaveMemoButtonEventListener = addCategoryEventListener;
    }

    public void show() {
        mDialog = new SaveMemoDialogFragment();
        mDialog.setSaveMemoButtonEventListener(mSaveMemoButtonEventListener);
        mDialog.show(mFragmentManager, DIALOG_SAVE_MEMO);
    }
}
