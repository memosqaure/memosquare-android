package com.estsoft.memosquare.utils;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import timber.log.Timber;

/**
 * Created by hokyung on 2017. 1. 12..
 */

public class PicassoImageGetter implements Html.ImageGetter {
    final Resources mResources;
    final Picasso mPicasso;
    final TextView mTextView;
    private int mImageWidth;

    public PicassoImageGetter(TextView textView, Resources resources, Picasso picasso,
                              int imageWidth) {
        this.mTextView = textView;
        this.mResources = resources;
        this.mPicasso = picasso;
        mImageWidth = imageWidth;
    }

    @Override
    public Drawable getDrawable(final String source) {
        final BitmapDrawablePlaceHolder result = new BitmapDrawablePlaceHolder();

        new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void... params) {
                try {
                    return mPicasso.load(source).get();
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                try {
                    final BitmapDrawable drawable = new BitmapDrawable(mResources, bitmap);

                    int ratio = mImageWidth / drawable.getIntrinsicWidth();

                    // 사진의 크기
                    drawable.setBounds(0, 0, drawable.getIntrinsicWidth() * ratio,
                            drawable.getIntrinsicHeight() * ratio);

                    result.setDrawable(drawable);

                    // 공간의 크기
                    result.setBounds(0, 0, drawable.getIntrinsicWidth() * ratio,
                            drawable.getIntrinsicHeight() * ratio);

                    mTextView.setText(mTextView.getText());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.execute((Void) null);

        return result;
    }

    static class BitmapDrawablePlaceHolder extends BitmapDrawable {
        protected Drawable drawable;

        public void setDrawable(Drawable drawable) {
            this.drawable = drawable;
        }

        @Override
        public void draw(Canvas canvas) {
            if (drawable != null) {
                drawable.draw(canvas);
            }
        }
    }
}
