package com.estsoft.memosquare.utils;

import android.support.v4.app.FragmentManager;

import com.estsoft.memosquare.fragments.CategoryConfigDialogFragment;
import com.estsoft.memosquare.listeners.CategoryConfigEventListener;

/**
 * Created by hokyung on 2017. 1. 5..
 */

public class CategoryConfigDialogUtil {

    private static final String DIALOG_CATEGORY_CONFIG = "CategoryConfig";

    // 콜백
    private CategoryConfigEventListener mCategoryConfigEventListener;

    private FragmentManager mFragmentManager;
    private CategoryConfigDialogFragment mDialog;

    public CategoryConfigDialogUtil(FragmentManager fragmentManager, CategoryConfigEventListener categoryConfigEventListener) {
        mFragmentManager = fragmentManager;
        mCategoryConfigEventListener = categoryConfigEventListener;
    }

    public void show() {
        mDialog = new CategoryConfigDialogFragment();
        mDialog.setCategoryConfigEventListener(mCategoryConfigEventListener);
        mDialog.show(mFragmentManager, DIALOG_CATEGORY_CONFIG);
    }
}
