package com.estsoft.memosquare.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.text.Html;
import android.text.Spanned;

import com.estsoft.memosquare.listeners.GetImageSpanCallbackListener;
import com.estsoft.memosquare.vo.CategoryVo;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by hokyung on 2017. 1. 9..
 * 웹의 이미지 주소를 통해 이미지를 받아와서 Spanned 객체로 넘긴다
 */

public class GetOneImageSpan extends AsyncTask<String, Void, Spanned> {

    private int mImageWidth;
    private GetImageSpanCallbackListener mListener;

    public void setImageWidth(int imageWidth) {
        mImageWidth = imageWidth;
    }

    public void setListener(GetImageSpanCallbackListener listener) {
        mListener = listener;
    }

    @Override
    protected Spanned doInBackground(String... params) {
        Spanned spanned = Html.fromHtml("<img src=\"" + params[0] + "\"><br>",
                new Html.ImageGetter() {
                    @Override
                    public Drawable getDrawable(String source) {
                        Drawable drawable = getDrawableFromUrl(source);
                        int ratio = mImageWidth / drawable.getIntrinsicWidth();
                        drawable.setBounds(0, 0, drawable.getIntrinsicWidth() * ratio,
                                drawable.getIntrinsicHeight() * ratio);

                        return drawable;
                    }
                }, null);

        return spanned;
    }

    private Drawable getDrawableFromUrl(String url) {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.connect();
            InputStream inputStream = connection.getInputStream();

            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

            inputStream.close();

            return new BitmapDrawable(bitmap);
        }catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Spanned spanned) {
        super.onPostExecute(spanned);
        mListener.onGetSpanSuccess(spanned);
    }
}
