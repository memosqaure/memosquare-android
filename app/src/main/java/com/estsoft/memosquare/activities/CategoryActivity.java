package com.estsoft.memosquare.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.estsoft.memosquare.MemoSquare;
import com.estsoft.memosquare.R;
import com.estsoft.memosquare.fragments.CategoryFragment;
import com.estsoft.memosquare.fragments.MainDrawerFragment;
import com.estsoft.memosquare.listeners.CategoryConfigEventListener;
import com.estsoft.memosquare.listeners.MainDrawerEventListener;
import com.estsoft.memosquare.listeners.MainTabMymemoFragmentEventListener;
import com.estsoft.memosquare.mvp.models.MainDrawerModel;
import com.estsoft.memosquare.mvp.models.MainDrawerModelImpl;
import com.estsoft.memosquare.preferences.HomepagePreferences;
import com.estsoft.memosquare.preferences.UserPreferences;
import com.estsoft.memosquare.utils.CategoryConfigDialogUtil;
import com.estsoft.memosquare.utils.ProgressBarDialogUtil;
import com.estsoft.memosquare.vo.CategoryVo;
import com.facebook.login.LoginManager;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class CategoryActivity extends AppCompatActivity {

    // activity for result 를 위한 코드
    public final static int REQUEST_MEMOVIEW = 23;

    // 위젯 할당
    @BindView(R.id.category_toolbar) Toolbar mToolbar;
    @BindView(R.id.drawer_layout) DrawerLayout mDrawerLayout;
    @BindView(R.id.up_fab) FloatingActionButton mUpFab;

    // 리소스 할당
    @BindDrawable(R.drawable.ic_menu) Drawable mMenuIcon;

    private CategoryVo mCategoryVo;
    private CategoryConfigDialogUtil mCategoryConfigDialogUtil;
    private ProgressBarDialogUtil mProgressBarDialogUtil;

    private MainDrawerFragment mMainDrawerFragment;
    private CategoryFragment mCategoryFragment;

    private MainDrawerEventListener mMainDrawerEventListener;
    private MainTabMymemoFragmentEventListener mMainTabMymemoFragmentEventListener;
    private MainDrawerModel.MainDrawerModelListener mMainDrawerModelListener;
    private CategoryConfigEventListener mCategoryConfigEventListener;

    private MainDrawerModel mMainDrawerModel;

    //Google Analytics Tracker
    Tracker mTracker;

    public static Intent getIntent(Context context) {
        return new Intent(context, CategoryActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        ButterKnife.bind(this);

        MemoSquare application = (MemoSquare) getApplication();
        mTracker = application.getDefaultTracker();

        Intent data = getIntent();
        mCategoryVo = (CategoryVo) data.getSerializableExtra("category");

        mProgressBarDialogUtil = new ProgressBarDialogUtil(this);
        mMainDrawerModel = new MainDrawerModelImpl();

        setUpActionBar();
        setUpEventListener();
        setUpFragments();
    }

    private void setUpActionBar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(mCategoryVo.getName());
        getSupportActionBar().setHomeAsUpIndicator(mMenuIcon);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setUpEventListener() {
        mMainDrawerEventListener = new MainDrawerEventListener() {
            @Override
            public void closeDrawer() {
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            }

            @Override
            public void startSuggestion() {
                CategoryActivity.this.startSuggestion();
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction("drawer-click suggestion")
                        .build());
            }

            @Override
            public void signOut() {
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction("drawer-click sign out")
                        .build());
                CategoryActivity.this.signOut();
            }
        };
        mMainTabMymemoFragmentEventListener = new MainTabMymemoFragmentEventListener() {
            @Override
            public void setUpButtonVisibility(int visibility) {
                mUpFab.setVisibility(visibility);
            }
        };
        mCategoryConfigEventListener = new CategoryConfigEventListener() {
            @Override
            public String getCategoryName() {
                return mCategoryVo.getName();
            }

            @Override
            public void deleteCategory() {
                mProgressBarDialogUtil.show();
                mMainDrawerModel.removeCategory(mCategoryVo, mMainDrawerModelListener);

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction("category-delete category")
                        .build());
            }

            @Override
            public void showNameIsEmptyToast() {
                Toast.makeText(CategoryActivity.this, R.string.category_name_empty, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void modifyCategory(String name) {
                mProgressBarDialogUtil.show();
                mCategoryVo.setName(name);
                mMainDrawerModel.updateCategory(mCategoryVo, mMainDrawerModelListener);

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction("category-update category")
                        .build());
            }
        };
        mMainDrawerModelListener = new MainDrawerModel.MainDrawerModelListener() {
            @Override
            public void onGetCategoryListSuccess(ArrayList<CategoryVo> categoryList) {
                // 드로어에서 쓰는 메소드
            }

            @Override
            public void onSaveSuccess(CategoryVo categoryVo) {
                // 드로어에서 쓰는 메소드
            }

            @Override
            public void onUpdateSuccess(CategoryVo categoryVo) {
                mProgressBarDialogUtil.cancel();
                mCategoryVo = categoryVo;
                getSupportActionBar().setTitle(mCategoryVo.getName());
            }

            @Override
            public void onRemoveSuccess() {
                mProgressBarDialogUtil.cancel();
                Timber.d("category remove success");
                finish();
            }

            @Override
            public void onFailed() {
                mProgressBarDialogUtil.cancel();
                Toast.makeText(CategoryActivity.this, R.string.networkError, Toast.LENGTH_SHORT).
                        show();
            }
        };
    }

    private void setUpFragments() {
        mMainDrawerFragment = new MainDrawerFragment();
        mCategoryFragment = new CategoryFragment();
        mCategoryFragment.setCategoryVo(mCategoryVo);

        mMainDrawerFragment.setMainDrawerEventListener(mMainDrawerEventListener);
        mCategoryFragment.setMainTabMymemoFragmentEventListener(mMainTabMymemoFragmentEventListener);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.drawer_container, mMainDrawerFragment)
                .add(R.id.category_list_container, mCategoryFragment)
                .commit();

        mCategoryConfigDialogUtil = new CategoryConfigDialogUtil(getSupportFragmentManager(),
                mCategoryConfigEventListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_MEMOVIEW) {
            if (resultCode == MemoViewActivity.RESULT_REMOVE) {
                mCategoryFragment.removeMemo(data.getIntExtra("index", -1));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_category, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.category_setting:
                mCategoryConfigDialogUtil.show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            super.onBackPressed();
        }
    }

    @OnClick(R.id.fab)
    public void fab() {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("category-click 'create memo' button")
                .build());

        startActivity(WebBrowserActivity.getIntent(this));
    }

    @OnClick(R.id.up_fab)
    public void upFab() {
        mCategoryFragment.goUp();
    }

    //일반 methods
    private void signOut(){
        UserPreferences.clearCurrentUser(this);
        LoginManager.getInstance().logOut();
        // 홈페이지 삭제
        HomepagePreferences.clearCurrentHomepage(this);
        startActivity(WelcomeActivity.getIntent(this));
        finish();
    }

    // 건의사항 액티비티 실행
    private void startSuggestion() {
        startActivity(SuggestionActivity.getIntent(this));
    }
}
