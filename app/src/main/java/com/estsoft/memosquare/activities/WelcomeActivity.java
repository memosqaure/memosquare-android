package com.estsoft.memosquare.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;


import com.estsoft.memosquare.MemoSquare;
import com.estsoft.memosquare.R;
import com.estsoft.memosquare.listeners.GetFbUserInfoFromFbServerListener;
import com.estsoft.memosquare.listeners.SignInServerListener;
import com.estsoft.memosquare.oldmodels.WelcomeModel;
import com.estsoft.memosquare.preferences.UserPreferences;
import com.estsoft.memosquare.utils.ProgressBarDialogUtil;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class WelcomeActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener{

    //Widgets
    @BindView(R.id.btn_fb_api) LoginButton mFbApiBtn;
    @BindView(R.id.btn_gl_api) SignInButton mGlApiBtn;
    @BindString(R.string.networkError) String mNetworkErrorTxt;

    //login API
    private CallbackManager mFbCallbackManager;
    private FacebookCallback<LoginResult> mFbCallback;
    private static final int RC_GOOGLE_SIGN_IN = 9001;
    private GoogleApiClient mGoogleApiClient;

    // progressbar dialog
    private ProgressBarDialogUtil mProgressBarDialogUtil;

    //Google Analytics Tracker
    Tracker mTracker;

    public static Intent getIntent(Context context) {
        return new Intent(context, WelcomeActivity.class);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);

        MemoSquare application = (MemoSquare) getApplication();
        mTracker = application.getDefaultTracker();

        //0.1 Current User Exists
        if(UserPreferences.getCurrentUser(WelcomeActivity.this) != null ){
            Timber.d("67: "+ UserPreferences.getCurrentUser(WelcomeActivity.this).toString());
            startActivity(MainActivity.getIntent(WelcomeActivity.this));
            finish();
        }


        // 0.2 No Current User Exists
        mProgressBarDialogUtil = new ProgressBarDialogUtil(this);

        /** 1. Facebook sdk & Google api initialization */
        mFbCallbackManager = CallbackManager.Factory.create();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                //enableAutoManage(FragmentActivity, OnConnectionFailedListener)
                .enableAutoManage(this, this )
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    /** Facebook callback 생성 */
    @Override
    protected void onResume() {
        super.onResume();
        // Facebook login callback 생성
        mFbCallback = generateFbCallback();

        // Facebook login button에 읽어올 것 사용자에게 권한 확인 부여
        mFbApiBtn.setReadPermissions("public_profile", "email");
    }

    /** 2. When Buttons clicked */
    @OnClick(R.id.btn_fb_custom)
    void onFbClick(){

        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Login-Facebook")
                .build());

        mProgressBarDialogUtil.show();

        mFbApiBtn.performClick();
        mFbApiBtn.setPressed(true);
        mFbApiBtn.invalidate();
        mFbApiBtn.registerCallback(mFbCallbackManager, mFbCallback);
        mFbApiBtn.setPressed(false);
        mFbApiBtn.invalidate();
    }

    @OnClick(R.id.btn_gl_custom)
    void onGlClick(){

        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Login-Google")
                .build());

        mProgressBarDialogUtil.show();

        mGlApiBtn.performClick();
        mGlApiBtn.setPressed(true);
        mGlApiBtn.invalidate();
        signIn();
        mGlApiBtn.setPressed(false);
        mGlApiBtn.invalidate();
    }


    /** 3. onActivityResult -> facebook, google dialog 꺼졌을 때 */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (FacebookSdk.isFacebookRequestCode(requestCode)) {
            mFbCallbackManager.onActivityResult(requestCode, resultCode, data);
        }else if (requestCode == RC_GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    /** Google 관련 method */
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_GOOGLE_SIGN_IN);
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Timber.d("google onConnectionFailed:" + connectionResult);
        Toast.makeText(WelcomeActivity.this, mNetworkErrorTxt, Toast.LENGTH_SHORT).show();
        mProgressBarDialogUtil.cancel();
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            Timber.d("google Signed in & ID Token: "+ acct.getIdToken());
            try{
                WelcomeModel.setGlUserInPreference(acct, WelcomeActivity.this);
                signInServer(acct.getIdToken());
            }catch(JSONException e) {
                Toast.makeText(WelcomeActivity.this, mNetworkErrorTxt, Toast.LENGTH_SHORT).show();
                mProgressBarDialogUtil.cancel();
            }
        } else {
            Toast.makeText(WelcomeActivity.this, mNetworkErrorTxt, Toast.LENGTH_SHORT).show();
            mProgressBarDialogUtil.cancel();

        }
    }

    /** Facebook 관련 method */
    private FacebookCallback<LoginResult> generateFbCallback(){

        return new FacebookCallback<LoginResult>(){
            //facebook login success: get token & get information from facebook server & send token to backend server
            @Override
            public void onSuccess(final LoginResult result) {
                WelcomeModel.getFbUserInfoFromFbServer(result, new GetFbUserInfoFromFbServerListener() {
                    @Override
                    public void onGetFbUserInfoFromFbServerSuccess(JSONObject object) {
                        try{
                            WelcomeModel.setFbUserInPreference(object, WelcomeActivity.this);
                            signInServer(result.getAccessToken().getToken());
                        }catch(JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onGetFbUserInfoFromFbServerFail() {
                        Toast.makeText(WelcomeActivity.this, mNetworkErrorTxt, Toast.LENGTH_SHORT).show();
                        mProgressBarDialogUtil.cancel();
                    }
                });

            }
            @Override
            public void onError(FacebookException error) {
                Log.e("test", "Error: " + error);
                Toast.makeText(WelcomeActivity.this, mNetworkErrorTxt , Toast.LENGTH_SHORT).show();
                mProgressBarDialogUtil.cancel();
            }
            @Override
            public void onCancel() {
                mProgressBarDialogUtil.cancel();
            }
        };
    }



/** 공통 */
    private void signInServer(String token){
        WelcomeModel.signInServer(token, new SignInServerListener() {
            @Override
            public void onSignInServerSuccess() {
                Timber.d("93: onSignInServerSuccess");
                mProgressBarDialogUtil.cancel();
                startMainActivity();
            }
            @Override
            public void onSignInServerFail() {
                Timber.d("99: onSignInServerFail");
                mProgressBarDialogUtil.cancel();
            }
        });
    }

    private void startMainActivity(){
        startActivity(MainActivity.getIntent(WelcomeActivity.this));
        startActivity(TutorialActivity.getIntent(WelcomeActivity.this));
        finish();
    }


}
