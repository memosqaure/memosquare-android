package com.estsoft.memosquare.activities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ListPopupWindow;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.estsoft.memosquare.MemoSquare;
import com.estsoft.memosquare.R;
import com.estsoft.memosquare.adapters.WebBrowserOptionMenuAdapter;
import com.estsoft.memosquare.adapters.WebBrowserPagerAdapter;
import com.estsoft.memosquare.adapters.WebBrowserUrlAdapter;
import com.estsoft.memosquare.database.Repository.BookmarkRepository;
import com.estsoft.memosquare.database.Repository.UrlRepository;
import com.estsoft.memosquare.fragments.WebBrowserMemoListFragment;
import com.estsoft.memosquare.fragments.WebBrowserMemoWriteFragment;
import com.estsoft.memosquare.fragments.WebBrowserWebViewFragment;
import com.estsoft.memosquare.listeners.EditTextEventListener;
import com.estsoft.memosquare.listeners.ModifyHomepageDialogEventListener;
import com.estsoft.memosquare.listeners.WebBrowserMemoListFragmentEventListener;
import com.estsoft.memosquare.listeners.WebBrowserMemoWriteFragmentEventListener;
import com.estsoft.memosquare.listeners.WebBrowserOptionClickEventListener;
import com.estsoft.memosquare.listeners.WebViewFragmentEventListener;
import com.estsoft.memosquare.oldmodels.WebBrowserModel;
import com.estsoft.memosquare.preferences.HomepagePreferences;
import com.estsoft.memosquare.utils.EditTextOnKeyListener;
import com.estsoft.memosquare.utils.ModifyHomepageDialogUtil;
import com.estsoft.memosquare.vo.BookmarkVo;
import com.estsoft.memosquare.vo.MemoVo;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindDimen;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

// Webbrowser 상태에서 항상 떠있을 액티비티
// 한 액티비티 안에서 changeMode를 통해 메모보기, 쓰기 등 여러 형태로 바꾼다
public class WebBrowserActivity extends AppCompatActivity {

    // 모드의 종류
    public static final int WEBBROWSING = 1000;
    public static final int MEMOWRITE = 1001;

    // 뷰페이저 아이템 번호
    private static final int WEBBROWSERFRAGMENT = 0;
    private static final int WEBMEMOLISTFRAGMENT = 1;

    // Request Code
    private static final int REQUEST_BOOKMARK = 78;

    // 위젯 할당
    @BindView(R.id.web_appbar) AppBarLayout mAppBarLayout;
    @BindView(R.id.web_url_input) AutoCompleteTextView mUrlInput;
    @BindView(R.id.go_url_btn) ImageButton mGoUrl;
    @BindView(R.id.square_btn) ImageButton mSquareButton;
    @BindView(R.id.webbrowser_option) ImageButton mOptionButton;
    @BindView(R.id.web_tabLayout) TabLayout mTabLayout;
    @BindView(R.id.web_viewpager) ViewPager mViewPager;
    @BindView(R.id.web_memo_container) FrameLayout mMemoContainer;
    @BindView(R.id.web_fab) FloatingActionButton mFab;
    @BindView(R.id.web_up_fab) FloatingActionButton mUpFab;

    // resource 불러오기
    @BindString(R.string.basic_url) String mBasicUrl;
    @BindString(R.string.tab_web) String mTabWeb;
    @BindString(R.string.tab_memo) String mTabMemo;
    @BindColor(R.color.colorGray) int mGray;
    @BindColor(R.color.colorLightGray) int mLightGray;
    @BindColor(R.color.colorDarkGray) int mDarkGray;
    @BindColor(R.color.colorDarkBlue) int mDarkBlue;
    @BindDimen(R.dimen.webbrowser_option_height) int mOptionHeight;

    // Fragment 관련 변수
    private WebBrowserWebViewFragment mWebBrowserWebViewFragment;
    private WebBrowserMemoListFragment mWebBrowserMemoListFragment;
    private WebBrowserMemoWriteFragment mWebBrowserMemoWriteFragment;
    private FragmentManager mFragmentManager;
    private LinearLayout.LayoutParams mContainerParams;

    // 현재 상태를 저장하기 위한 변수
    private int mCurrentMode;    // 현재 모드
    private boolean mHighlightAvailable;    // 토글 가능여부 저장
    private MemoVo mMemoVo;   // 메모 정보 저장
    private boolean mHasMemo;    // 웹에서보기 등을 통해왔는지 저장
    private String mUrl;
    private boolean mIsInModeChanging;   // 모드를 바꾸는 중인지 저장. 모드를 바꿀때는 리스트뷰의 스크롤 이벤트 방지

    // 모델 객체
    private WebBrowserModel mModel;

    // 이벤트 리스너 인터페이스
    private WebViewFragmentEventListener mWebViewFragmentEventListener;
    private WebBrowserMemoWriteFragmentEventListener mWebBrowserMemoWriteFragmentEventListener;
    private WebBrowserMemoListFragmentEventListener mWebBrowserMemoListFragmentEventListener;
    private WebBrowserOptionClickEventListener mWebBrowserOptionClickEventListener;
    private ModifyHomepageDialogEventListener mModifyHomepageDialogEventListener;

    // 옵션 메뉴 변수
    private ListPopupWindow mPopupWindow;

    //Google Analytics Tracker
    Tracker mTracker;


    // 웹브라우저 액티비티로 오기위한 인텐트
    public static Intent getIntent(Context context) {
        return new Intent(context, WebBrowserActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webbrowser);
        ButterKnife.bind(this);

        MemoSquare application = (MemoSquare) getApplication();
        mTracker = application.getDefaultTracker();

        // 모드 변화중인지는 기본이 ƒalse
        mIsInModeChanging = false;

        // 모델 할당
        mModel = WebBrowserModel.getInstance();

        // 리스너 할당
        setFragmentEventListeners();

        // 첫 모드는 웹 뷰 모드
        mCurrentMode = WEBBROWSING;

        // hasMemo의 기본은 false
        mHasMemo = false;

        // memo container 용 레이아웃 파라미터 할당
        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        mContainerParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                , (int) (size.y * 0.35));

        // 자신이 지정한 홈페이지가 있을 경우
        if(HomepagePreferences.getCurrentHomepage(WebBrowserActivity.this) != null) {
            mBasicUrl = HomepagePreferences.getCurrentHomepage(WebBrowserActivity.this);
        }

        // 1. Setup Fragment Manager
        mFragmentManager = getSupportFragmentManager();

        // 데이터가 넘어온 경우
        Intent intent = getIntent();
        if (intent.getStringExtra(Intent.EXTRA_TEXT) != null) {
            // 공유를 타고왔을 경우
            mBasicUrl = intent.getStringExtra(Intent.EXTRA_TEXT);
            Timber.d("intent.string " + mBasicUrl);

            // 메모에서  수정을 눌렀을 경우
            mMemoVo = (MemoVo) intent.getSerializableExtra("memo");

            if (mMemoVo == null)
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction("web-came from other browser app")
                        .build());
            else {
                // -1 은 잘못된 모드. 모드가 없는 경우
                int mode = intent.getIntExtra("mode", -1);
                if (mode != -1) {
                    mHasMemo = true;
                    changeMode(mode);
                }
            }
        }

        // 2. Setup Url AutoCompleteTextView, go Url Button
        mUrlInput.setText(mBasicUrl);
        mUrl = mBasicUrl;
        EditTextOnKeyListener editTextOnKeyListener = new EditTextOnKeyListener(
                WebBrowserActivity.this, EditTextOnKeyListener.WEBBROWSERACTIVITY);
        editTextOnKeyListener.setEditTextEventListener(new EditTextEventListener() {
            @Override
            public void goUrl() {
                WebBrowserActivity.this.goUrl();
            }

            @Override
            public void clearEditTextFocusAndKeyboard() {
                WebBrowserActivity.this.clearEditTextFocusAndKeyboard();
            }

            @Override
            public void deleteCharacter() {
                int length = mUrlInput.getText().toString().length();
                if (length > 0) {
                    int start = mUrlInput.getSelectionStart();
                    int end = mUrlInput.getSelectionEnd();
                    if (start != 0) {
                        start--;
                    }
                    mUrlInput.getText().delete(start, end);
                }
            }
        });
        mUrlInput.setOnKeyListener(editTextOnKeyListener);
        mUrlInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // 포커스를 받을 경우 버튼 visible
                if (hasFocus) {
                    mGoUrl.setVisibility(View.VISIBLE);
                }
                // 포커스에서 벗어날 경우 버튼 gone
                else {
                    mGoUrl.setVisibility(View.GONE);
                }
            }
        });
        mGoUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goUrl();
            }
        });

        // 2.1 setup auto complete data
        ArrayList list = (ArrayList) UrlRepository.getInstance(this).getUrlList();
        mUrlInput.setAdapter(new WebBrowserUrlAdapter(this, R.layout.item_url_history, list));
        mUrlInput.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // 보기에 있는 아이템 눌렸을 때
                String url = ((TextView)view.findViewById(R.id.site_url)).getText().toString();
                changeUrl(url);
                goUrl();
                clearEditTextFocusAndKeyboard();
            }
        });

        // 3. Option Button
        // 3.1 Square Button Listener
        mSquareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goMain();
            }
        });

        // 3.2 Option Button Listener
        mOptionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOptionMenu(v);
            }
        });

        // 3.3 Option View 구성
        mPopupWindow = new ListPopupWindow(this);

        // 4. Setup View Pager
        // 4.1 Creating PagerAdapter
        WebBrowserPagerAdapter adapter = new WebBrowserPagerAdapter(getSupportFragmentManager());
        mWebBrowserWebViewFragment = new WebBrowserWebViewFragment();
        mWebBrowserWebViewFragment.setWebViewFragmentEventListener(mWebViewFragmentEventListener);
        adapter.addFragment(mWebBrowserWebViewFragment, mTabWeb);
        mWebBrowserMemoListFragment = new WebBrowserMemoListFragment();
        mWebBrowserMemoListFragment.setWebBrowserMemoListFragmentEventListener(
                mWebBrowserMemoListFragmentEventListener);
        adapter.addFragment(mWebBrowserMemoListFragment, mTabMemo);
        mViewPager.setAdapter(adapter);

        // 4.2 Connecting tabLayout, ViewPager
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                // 메모 리스트 페이지가 보일때
                if (position == 1) {
                    if (mWebBrowserMemoListFragment.isTop()) {
                        mUpFab.setVisibility(View.GONE);
                    } else {
                        Timber.d("onPageSelected to MemoList");
                        mUpFab.setVisibility(View.VISIBLE);
                    }
                }
                // 웹뷰가 보일때
                else {
                    mUpFab.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // 5. Setup Write Floating Action Button
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // memo write 모드로 변경
                changeMode(MEMOWRITE);

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction("web-click 'create memo'button")
                        .build());

            }
        });

        // 6. Setup Up Floating Action Button
        mUpFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWebBrowserMemoListFragment.goUp();
            }
        });
    }

    // 취소키 눌렸을 때
    @Override
    public void onBackPressed() {
        // 옵션 메뉴가 보여지고 있을 경우
        if (mPopupWindow.isShowing()) {
            mPopupWindow.dismiss();
            return;
        }
        // 웹브라우징 모드일 경우
        if (mCurrentMode == WEBBROWSING) {
            switch (mViewPager.getCurrentItem()) {
                case WEBBROWSERFRAGMENT:
                    // 브라우저에 방문했던 페이지가 있다면 거기로 간다
                    if (!mWebBrowserWebViewFragment.onBackPressed()) {
                        goMain();
                    }
                    break;
                case WEBMEMOLISTFRAGMENT:
                    // 메모 리스트에서 취소키를 누를 시 웹뷰로 간다
                    mViewPager.setCurrentItem(WEBBROWSERFRAGMENT, true);
                    break;
                default:
                    Timber.d("wrong page number: " + mViewPager.getCurrentItem());
            }
        }
        // 메모 쓰기 모드일 경우
        else if (mCurrentMode == MEMOWRITE) {
            // 메모 종료를 묻는 다이얼로그를 넣어야한다
            // 소프트 키보드 활성화 상태 일 시, 키보드를 없앤다
            clearEditTextFocusAndKeyboard();

            // 웹브라우징 모드로 변환한다
            changeMode(WEBBROWSING);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_BOOKMARK) {
            if (resultCode == RESULT_OK) {
                // 북마크에서 아이템을 클릭한 상황
                String url = data.getStringExtra("url");
                changeUrl(url);
                mWebBrowserWebViewFragment.goUrl(url);
                clearEditTextFocusAndKeyboard();
            }
        }
    }

    // 이미 MainActivity가 있으면 그냥 finish()
    // 없으면 startActivity 후 finish()
    private void goMain() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

        List<ActivityManager.RunningTaskInfo> taskInfoList = manager.getRunningTasks(10);

        if (taskInfoList.get(0).numActivities == 1 && taskInfoList.get(0).topActivity
                .getClassName().equals(this.getClass().getName())) {
            Intent intent = MainActivity.getIntent(this);
            startActivity(intent);
        }

        finish();
    }

    // 옵션의 overflow 아이콘의 메뉴 보이기
    private void showOptionMenu(View anchor) {

        // 보여지던 중이면 끄기
        if (mPopupWindow.isShowing()) {
            mPopupWindow.dismiss();
            return;
        }

        WebBrowserOptionMenuAdapter adapter = new WebBrowserOptionMenuAdapter(
                this,
                R.layout.item_webbrowser_option);
        adapter.setWebBrowserOptionClickEventListener(mWebBrowserOptionClickEventListener);

        // 기기의 화면에 비례하는 픽셀 값 얻기
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager)getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);

        mPopupWindow.setAnchorView(anchor);
        mPopupWindow.setAdapter(adapter);
        mPopupWindow.setWidth((int) (metrics.widthPixels * 0.5));
        mPopupWindow.setHeight(mOptionHeight);
        mPopupWindow.setModal(false);
        mPopupWindow.show();
    }

    // 모드에 따라 액티비티의 형태를 바꾼다
    private void changeMode(int mode) {
        // 모드 변화 시작
        mIsInModeChanging = true;

        switch (mode) {
            case WEBBROWSING:
                Timber.d("mode change to webbrowsing");

                // 1. web_memo_container를 gone으로
                mMemoContainer.setVisibility(View.GONE);

                // 2. mWebBrowserMemoWriteFragment의 하이라이팅을 끈다
                if (mWebBrowserMemoWriteFragment != null) {
                    mWebBrowserMemoWriteFragment.offHighlight();
                }

                // 3. web_fab를 visible로
                mFab.setVisibility(View.VISIBLE);

                // 4. current mode를 바꾼다
                mCurrentMode = WEBBROWSING;

                break;
            case MEMOWRITE:
                Timber.d("mode change to memo writing");
                // 1. 메모 형태로 레이아웃을 바꾼다
                changeLayoutToMemo();

                // 2. web_memo_container에 WebMomoWriteFragment를 담는다
                if (mWebBrowserMemoWriteFragment == null) {
                    mWebBrowserMemoWriteFragment = new WebBrowserMemoWriteFragment();
                    mWebBrowserMemoWriteFragment.setWebBrowserMemoWriteFragmentEventListener(mWebBrowserMemoWriteFragmentEventListener);
                }

                // 웹 브라우징 모드의 경우 fragment를 새로 할당해야한다
                // 근데 일단 당장은 브라우징 모드에서밖에 못들어오는데
                mFragmentManager.beginTransaction()
                        .replace(R.id.web_memo_container, mWebBrowserMemoWriteFragment)
                        .commit();

                // 메모 정보가 있을 경우 메모 정보 입력
                if (mHasMemo) {
                    mWebBrowserMemoWriteFragment.setMemoVo(mMemoVo);
                }
                // 없을경우 빈 메모 입력
                else {
                    mWebBrowserMemoWriteFragment.setMemoVo(new MemoVo());
                }

                // 3. 하이라이팅 가능 여부를 전달한다
                mWebBrowserMemoWriteFragment.setHighlightButtonEnable(mHighlightAvailable);

                // 4. current mode를 변경한다
                mCurrentMode = MEMOWRITE;

                break;
            default:
                Timber.d("wrong mode number " + mode);
        }

        // 모드 변화 끝
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mIsInModeChanging = false;
            }
        }, 100);
    }

    // 메모 쓰기와 보기 뷰 형태로 액티비티 모습 변경
    private void changeLayoutToMemo() {
        // 1. memo_container를 준비한다
        // 보이게하고
        mMemoContainer.setVisibility(View.VISIBLE);
        // height 적용하고
        mMemoContainer.setLayoutParams(mContainerParams);

        // 2. web_fab를 gone으로
        mFab.setVisibility(View.GONE);
    }

    // memo write, view fragment를 비운다
    private void clearMemoFragments() {
        Timber.d("clearMemoFragments");
        if (mWebBrowserMemoWriteFragment != null) {
            mWebBrowserMemoWriteFragment.setMemoVo(new MemoVo());
            mWebBrowserMemoWriteFragment.clearAllWidgets();
        }
    }

    // 리스너 할당
    private void setFragmentEventListeners() {
        // 홈페이지 변경 이벤트 리스너
        mModifyHomepageDialogEventListener = new ModifyHomepageDialogEventListener() {
            @Override
            public void useBasicHomepage() {
                HomepagePreferences.clearCurrentHomepage(WebBrowserActivity.this);
            }

            @Override
            public void useCurrentHomepage() {
                HomepagePreferences.setHomepage(getUrl(), WebBrowserActivity.this);
            }
        };

        // 옵션메뉴 이벤트 리스너
        mWebBrowserOptionClickEventListener = new WebBrowserOptionClickEventListener() {
            @Override
            public void goForward() {
                if (mWebBrowserWebViewFragment.goForward()) {
                    mPopupWindow.dismiss();
                }
            }

            @Override
            public void addBookmark() {
                BookmarkVo bookmarkVo = new BookmarkVo();
                bookmarkVo.setName(mWebBrowserWebViewFragment.getName());
                bookmarkVo.setUrl(WebBrowserActivity.this.getUrl());

                BookmarkRepository.getInstance(WebBrowserActivity.this)
                        .addBookmark(bookmarkVo);
            }

            @Override
            public void removeBookmark() {
                BookmarkRepository repository = BookmarkRepository
                        .getInstance(WebBrowserActivity.this);
                BookmarkVo bookmarkVo = repository.getBookmark(WebBrowserActivity.this.getUrl());
                if (bookmarkVo == null) {
                    Timber.d("bookmark is null");
                    return;
                }

                repository.deleteBookmark(bookmarkVo);
            }

            @Override
            public void refresh() {
                WebBrowserActivity.this.goUrl();
                mPopupWindow.dismiss();
            }

            @Override
            public void showBookmarks() {
                mPopupWindow.dismiss();
                Intent intent = BookmarkActivity.getInstance(WebBrowserActivity.this);
                startActivityForResult(intent, REQUEST_BOOKMARK);
            }

            @Override
            public void modifyHomepage() {
                mPopupWindow.dismiss();
                ModifyHomepageDialogUtil modifyHomepageDialogUtil = new ModifyHomepageDialogUtil(
                        getSupportFragmentManager(), mModifyHomepageDialogEventListener);
                modifyHomepageDialogUtil.show();
            }

            @Override
            public String getUrl() {
                return WebBrowserActivity.this.getUrl();
            }
        };

        // 웹뷰 이벤트 리스너
        mWebViewFragmentEventListener = new WebViewFragmentEventListener() {
            @Override
            public String getUrl() {
                return mUrl;
            }

            @Override
            public void setHasMemo(boolean hasMemo) {
                Timber.d("setHasMemo " + hasMemo);
                WebBrowserActivity.this.mHasMemo = hasMemo;
            }

            @Override
            public boolean getHasMemo() {
                return mHasMemo;
            }

            @Override
            public void changeUrl(String url) {
                Timber.d("changeUrl " + url);
                WebBrowserActivity.this.changeUrl(url);
            }

            @Override
            public void setHighlightAvailable(boolean available) {
                Timber.d("setHighlightAvailable: " + available);
                mHighlightAvailable = available;
                // available이 true일 경우가 웹과의 통신 맨 마지막인 경우이므로
                // 이것때문에 메모 수정이나 보기의 경우 맨 처음 hasMemo가 true 이고, available이 false 이므로 changeMode가 실행안됨
                if (available) {
                    mHasMemo = false;
                }

                // 하이라이트 가능여부 부여하기
                if (mWebBrowserMemoWriteFragment != null) {
                    mWebBrowserMemoWriteFragment.setHighlightButtonEnable(true);
                }
            }

            @Override
            public void clearMemoList() {
               mWebBrowserMemoListFragment.clearMemoList();
            }

            @Override
            public void loadMemoList() {
                mWebBrowserMemoListFragment.loadMemoList();
            }

            @Override
            public void addHighlightedContents(String content) {
                Timber.d("getHtmlSpan " + content);
                if (mWebBrowserMemoWriteFragment != null) {
                    mWebBrowserMemoWriteFragment.addHighlightedContents(content);
                }
            }

            @Override
            public void addImage(String url) {
                if (mWebBrowserMemoWriteFragment != null) {
                    mWebBrowserMemoWriteFragment.addImage(url);
                }
            }

            @Override
            public void changeModeToWebBrowsing() {
                if (!mHasMemo) {
                    clearMemoFragments();
                    changeMode(WEBBROWSING);
                }
            }

            @Override
            public void initializeOffset() {
                mWebBrowserMemoListFragment.initializeOffset();
            }
        };

        // 메모 쓰기 뷰 리스너
        mWebBrowserMemoWriteFragmentEventListener = new WebBrowserMemoWriteFragmentEventListener() {
            @Override
            public boolean isHighlightAvailable() {
                return WebBrowserActivity.this.mHighlightAvailable;
            }

            @Override
            public void toggleHighlight() {
                Timber.d("toggleHighlight");
                mWebBrowserWebViewFragment.jsExecute();
            }

            @Override
            public String getUrl() {
                return WebBrowserActivity.this.getUrl();
            }

            @Override
            public void changeModeToWebBrowsing() {
                changeMode(WEBBROWSING);
            }

            @Override
            public void clearEditTextFocusAndKeyboard() {
                WebBrowserActivity.this.clearEditTextFocusAndKeyboard();
            }

            @Override
            public void setMemoVo(MemoVo memoVo) {
                mMemoVo = memoVo;
            }
        };

        // 메모 리스트 프래그먼트 리스너
        mWebBrowserMemoListFragmentEventListener = new WebBrowserMemoListFragmentEventListener() {
            @Override
            public String getUrl() {
                return mUrl;
            }

            @Override
            public void setUpButtonVisibility(int visibility) {
                mUpFab.setVisibility(visibility);
            }

            @Override
            public boolean isInModeChanging() {
                return mIsInModeChanging;
            }
        };
    }

    // 주소창의 내용을 가져오기 위한 메소드
    private String getUrl() {
        return mUrlInput.getText().toString();
    }

    // 주소 이동을 위한 메소드(엔터키와 버튼 두가지 방식으로 동작)
    private void goUrl() {
        String url = mUrlInput.getText().toString();
        // 주소창이 비었을 경우
        if (url.equals("")) {
            return;
        }
        // 주소에 http가 포함이 안되어 있을 경우
        if (url.length() < 4 || !url.substring(0, 4).equals("http")) {
            url = "http://" + url;
        }
        changeUrl(url);
        mWebBrowserWebViewFragment.goUrl(url);
    }

    // 주소 변경을 위한 메소드
    private void changeUrl(String url) {
        Timber.d("changeUrl " + url);
        mUrl = url;
        mUrlInput.setText(url);
    }

    // EditText 엔터 후 포커스 없애기
    private void clearEditTextFocusAndKeyboard() {
        // 포커스 제거
        if (mUrlInput.isFocused()) {
            mUrlInput.clearFocus();
        }
        // 키보드 제거
        InputMethodManager mgr =
                (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(mUrlInput.getWindowToken(), 0);
    }

    @Override
    protected void onDestroy() {
        // 옵션 팝업 제거
        mPopupWindow.dismiss();
        super.onDestroy();
    }
}
