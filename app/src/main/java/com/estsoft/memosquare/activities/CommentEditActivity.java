package com.estsoft.memosquare.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageButton;

import com.estsoft.memosquare.MemoSquare;
import com.estsoft.memosquare.R;
import com.estsoft.memosquare.listeners.UpdateCommentListener;
import com.estsoft.memosquare.oldmodels.CommentModel;
import com.estsoft.memosquare.vo.CommentVo;
import com.google.android.gms.analytics.Tracker;

import org.w3c.dom.Comment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class CommentEditActivity extends AppCompatActivity{

    static int RESULT_EDIT_COMMENT = 11;

    @BindView(R.id.et_edit_comment) EditText mEtComment;

    private CommentVo comment;
    private CommentModel mModel;
    private int position;

    private Intent intent;

    private Tracker mTracker;

    public static Intent getIntent(Context context) {
        return new Intent(context, CommentEditActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editcomment);
        ButterKnife.bind(this);

        MemoSquare application = (MemoSquare) getApplication();
        mTracker = application.getDefaultTracker();

        intent = new Intent();

        Intent data = getIntent();
        position = (int)data.getSerializableExtra("position");
        comment = (CommentVo)data.getSerializableExtra("comment");

        mEtComment.setText(comment.getContent());

    }

    @OnClick(R.id.btn_close)
    public void clickBtnClose(){
        onBackPressed();
    }

    @OnClick(R.id.btn_save)
    public void clickBtnSave(){
        mModel.updateComment(comment, mEtComment.getText().toString(), new UpdateCommentListener() {
            @Override
            public void onSuccess(CommentVo comment) {
                intent.putExtra("updated", comment);
                intent.putExtra("position", position);
                setResult(RESULT_EDIT_COMMENT, intent);

                finish();
            }

            @Override
            public void onFailed() {
                Timber.d("update comment failed");
            }
        });

    }

}


