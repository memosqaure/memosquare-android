package com.estsoft.memosquare.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.estsoft.memosquare.MemoSquare;
import com.estsoft.memosquare.R;
import com.estsoft.memosquare.adapters.TutorialPagerAdapter;
import com.estsoft.memosquare.fragments.TutorialMainFragment;
import com.estsoft.memosquare.fragments.TutorialShareFragment;
import com.estsoft.memosquare.fragments.TutorialViewFragment;
import com.estsoft.memosquare.fragments.TutorialWriteFragment;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

public class TutorialActivity extends AppCompatActivity {

    // 위젯 할당
    @BindView(R.id.viewpager) ViewPager mViewPager;
    @BindViews({R.id.page_indicator1, R.id.page_indicator2, R.id.page_indicator3
            , R.id.page_indicator4})
    List<ImageView> mPageIndicators;
    @BindView(R.id.skip_btn) TextView mSkipButton;


    // 리소스 할당
    @BindString(R.string.tutorial_main_title) String mMainTitle;
    @BindString(R.string.tutorial_write_title) String mWriteTitle;
    @BindString(R.string.tutorial_view_title) String mViewTitle;
    @BindString(R.string.tutorial_share_title) String mShareTitle;
    @BindString(R.string.skip_btn) String mSkip;
    @BindString(R.string.start_btn) String mStart;
    @BindDrawable(R.drawable.ic_dot_selected) Drawable mDotSelected;
    @BindDrawable(R.drawable.ic_dot_normal) Drawable mDotNormal;

    private TutorialPagerAdapter mAdapter;

    //Google Analytics Tracker
    Tracker mTracker;

    public static Intent getIntent(Context context) {
        return new Intent(context, TutorialActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        ButterKnife.bind(this);

        MemoSquare application = (MemoSquare) getApplication();
        mTracker = application.getDefaultTracker();

        mAdapter = new TutorialPagerAdapter(getSupportFragmentManager());
        mAdapter.addFragment(new TutorialMainFragment(), mMainTitle);
        mAdapter.addFragment(new TutorialWriteFragment(), mWriteTitle);
        mAdapter.addFragment(new TutorialViewFragment(), mViewTitle);
        mAdapter.addFragment(new TutorialShareFragment(), mShareTitle);

        mViewPager.setAdapter(mAdapter);

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                // 기본 상태로 초기화
                for (ImageView imageView : mPageIndicators) {
                    imageView.setImageDrawable(mDotNormal);
                }
                // 해당 이미지 변경
                mPageIndicators.get(position).setImageDrawable(mDotSelected);

                // 버튼 텍스트 변경
                if (position == mPageIndicators.size() - 1) {
                    mSkipButton.setText(mStart);
                } else if (mSkipButton.getText().toString().equals(mStart)) {
                    mSkipButton.setText(mSkip);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // 스킵 버튼 리스너
        mSkipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Action")
                        .setAction("manual-skip")
                        .build());
                finish();
            }
        });
    }
}
