package com.estsoft.memosquare.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;

import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import com.estsoft.memosquare.MemoSquare;
import com.estsoft.memosquare.R;
import com.estsoft.memosquare.adapters.MainPagerAdapter;
import com.estsoft.memosquare.fragments.MainDrawerFragment;
import com.estsoft.memosquare.fragments.MainTabMyMemoFragment;
import com.estsoft.memosquare.fragments.MainTabClipbookFragment;
import com.estsoft.memosquare.fragments.MainTabSquareFragment;
import com.estsoft.memosquare.listeners.MainDrawerEventListener;
import com.estsoft.memosquare.listeners.MainTabClipbookFragmentEventListener;
import com.estsoft.memosquare.listeners.MainTabMymemoFragmentEventListener;
import com.estsoft.memosquare.preferences.HomepagePreferences;
import com.estsoft.memosquare.preferences.UserPreferences;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;


public class MainActivity extends AppCompatActivity{

    // activity for result 를 위한 코드
    public final static int REQUEST_MEMOVIEW = 23;

    @BindColor(R.color.colorGray) int mGray;
    @BindColor(R.color.colorLightGray) int mLightGray;
    @BindColor(R.color.colorDarkGray) int mDarkGray;
    @BindColor(R.color.colorDarkBlue) int mDarkBlue;
    @BindColor(R.color.colorPrimaryLight) int mPrimaryLight;

    //Widgets
    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.drawer_layout) DrawerLayout mDrawerLayout;
    @BindView(R.id.viewpager) ViewPager mViewPager;
    @BindView(R.id.tabLayout) TabLayout mTabLayout;
    @BindView(R.id.fab) FloatingActionButton mFab;
    @BindView(R.id.up_fab) FloatingActionButton mUpFab;
    
    //Resources
    @BindString(R.string.main_mymemo) String mTabMymemo;
    @BindString(R.string.main_clipbook) String mTabClipbook;
    @BindString(R.string.main_square) String mTabSquare;
    @BindDrawable(R.drawable.ic_menu) Drawable mMenuIcon;

    //Fragments
    private MainDrawerFragment mMainDrawerFragment;
    private MainTabMyMemoFragment mMainTabMyMemoFragment;
    private MainTabClipbookFragment mMainTabClipbookFragment;
    private MainTabSquareFragment mMainTabSquareFragment;

    //Event listeners
    private MainDrawerEventListener mMainDrawerEventListener;
    private MainTabMymemoFragmentEventListener mMainTabMyMemoFragmentEventListener;
    private MainTabClipbookFragmentEventListener mMainTabClipbookFragmentEventListener;

    //etc
    private MainPagerAdapter adapter;

    //Google Analytics Tracker
    Tracker mTracker;

    public static Intent getIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        MemoSquare application = (MemoSquare) getApplication();
        mTracker = application.getDefaultTracker();

        // onCreate() has these following steps:
        // 1. Setup Toolbar
        // 2. Setup Drawer
        // 3. Setup View Pager
        // 4. Setup Floating Action Button

        // 1. Setup Toolbar
        setSupportActionBar(mToolbar);
        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(mMenuIcon);
        ab.setDisplayHomeAsUpEnabled(true);

        // 2. Setup Drawer
        mMainDrawerFragment = new MainDrawerFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.drawer_container, mMainDrawerFragment)
                .commit();

        // 3. Setup View Pager
        //3.1 Creating Fragments
        mMainTabMyMemoFragment = new MainTabMyMemoFragment();
        mMainTabClipbookFragment = new MainTabClipbookFragment();
        mMainTabSquareFragment = new MainTabSquareFragment();

        //3.2 event listener
        setUpEventListeners();
        mMainDrawerFragment.setMainDrawerEventListener(mMainDrawerEventListener);
        mMainTabMyMemoFragment.setMainTabMymemoFragmentEventListener(
                mMainTabMyMemoFragmentEventListener);
        mMainTabClipbookFragment.setMainTabClipbookFragmentEventListener(
                mMainTabClipbookFragmentEventListener);

        //3.3. Creating PagerAdapter
        adapter = new MainPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(mMainTabMyMemoFragment, mTabMymemo);
        adapter.addFragment(mMainTabClipbookFragment, mTabClipbook);
        adapter.addFragment(mMainTabSquareFragment, mTabSquare);
        mViewPager.setAdapter(adapter);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        // 마이메모 리스트
                        mTracker.setScreenName("Main-MyMemo");
                        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                        if (mMainTabMyMemoFragment.IsTop()) {
                            mUpFab.setVisibility(View.GONE);
                        } else {
                            mUpFab.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 1:
                        // 클립북 리스트
                        mTracker.setScreenName("Main-ClipBook");
                        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                        if (mMainTabClipbookFragment.isTop()) {
                            mUpFab.setVisibility(View.GONE);
                        } else {
                            mUpFab.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 2:
                        // 스퀘어
                        mTracker.setScreenName("Main-Square");
                        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

                        mUpFab.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //3.4. Connecting mTabLayout, ViewPager
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setOffscreenPageLimit(2);
    }

    // 4. Setup Web Floating Action Button
    @OnClick(R.id.fab)
    void fab(){
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("main-click 'create memo' button")
                .build());
        startActivity(WebBrowserActivity.getIntent(MainActivity.this));
    }

    // 5. Setup Up Floating Action Button
    @OnClick(R.id.up_fab)
    void upFab() {
        // 페이저의 위치에 따라 다르게 작동
        switch (mViewPager.getCurrentItem()) {
            case 0:
                // 마이 메모
                mMainTabMyMemoFragment.goUp();
                break;
            case 1:
                // 클립북
                mMainTabClipbookFragment.goUp();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_MEMOVIEW) {
            Timber.d("requestCode");
            if (resultCode == MemoViewActivity.RESULT_REMOVE) {
               mMainTabMyMemoFragment.removeMemo(data.getIntExtra("index", -1));
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: // handle click event on home icon(drawer icon)
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(mDrawerLayout.isDrawerOpen(Gravity.LEFT)){
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        }else{
            super.onBackPressed();
        }
    }

    private void setUpEventListeners() {
        // 드로어 프래그먼트 이벤트 리스너
        mMainDrawerEventListener = new MainDrawerEventListener() {
            @Override
            public void closeDrawer() {
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            }

            @Override
            public void startSuggestion() {
                MainActivity.this.startSuggestion();
            }

            @Override
            public void signOut() {
                MainActivity.this.signOut();
            }
        };
        // 마이메모 프래그먼트 이벤트 리스너
        mMainTabMyMemoFragmentEventListener = new MainTabMymemoFragmentEventListener() {
            @Override
            public void setUpButtonVisibility(int visibility) {
                mUpFab.setVisibility(visibility);
            }
        };
        // 클립북 프래그먼트 이벤트 리스너
        mMainTabClipbookFragmentEventListener = new MainTabClipbookFragmentEventListener() {
            @Override
            public void setUpButtonVisibility(int visibility) {
                mUpFab.setVisibility(visibility);
            }
        };
    }

    //일반 methods
    private void signOut(){
        UserPreferences.clearCurrentUser(MainActivity.this);
        LoginManager.getInstance().logOut();
        // 홈페이지 삭제
        HomepagePreferences.clearCurrentHomepage(MainActivity.this);
        startActivity(WelcomeActivity.getIntent(MainActivity.this));
        finish();
    }

    // 건의사항 액티비티 실행
    private void startSuggestion() {
        startActivity(SuggestionActivity.getIntent(this));
    }
    }