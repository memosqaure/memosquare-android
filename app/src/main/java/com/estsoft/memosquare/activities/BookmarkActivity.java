package com.estsoft.memosquare.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.estsoft.memosquare.R;
import com.estsoft.memosquare.fragments.BookmarkFragment;
import com.estsoft.memosquare.listeners.BookmarkListEventListener;
import com.estsoft.memosquare.vo.BookmarkVo;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BookmarkActivity extends AppCompatActivity {

    // 위젯 할당
    @BindView(R.id.bookmark_toolbar) Toolbar mToolbar;
    @BindView(R.id.bookmark_container) FrameLayout mContainer;

    // 리소스 할당
    @BindString(R.string.bookmark_title) String mTitle;

    private BookmarkFragment mBookmarkFragment;

    public static Intent getInstance(Context context) {
        return new Intent(context, BookmarkActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark);
        ButterKnife.bind(this);

        // 툴바 설정
        setSupportActionBar(mToolbar);
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(mTitle);

        // 프래그먼트 생성
        mBookmarkFragment = new BookmarkFragment();
        mBookmarkFragment.setBookmarkListEventListener(new BookmarkListEventListener() {
            @Override
            public void onClickBookmark(BookmarkVo bookmarkVo) {
                Intent intent = new Intent();
                intent.putExtra("url", bookmarkVo.getUrl());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        getSupportFragmentManager().beginTransaction()
                .add(R.id.bookmark_container, mBookmarkFragment)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
