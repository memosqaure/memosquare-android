package com.estsoft.memosquare.activities;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.estsoft.memosquare.MemoSquare;
import com.estsoft.memosquare.R;
import com.estsoft.memosquare.fragments.CommentFragment;
import com.estsoft.memosquare.fragments.MemoViewFragment;
import com.estsoft.memosquare.oldmodels.MemoViewModel;
import com.estsoft.memosquare.utils.HtmlTagUtil;
import com.estsoft.memosquare.utils.ShareUtil;
import com.estsoft.memosquare.vo.MemoVo;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class MemoViewActivity extends AppCompatActivity {

    // 위젯 할당
    @BindView(R.id.memoview_toolbar) Toolbar mToolbar;
    @BindView(R.id.fab) FloatingActionButton mFab;

    // 리소스 할당
    @BindDrawable(R.drawable.ic_bookmark_border) Drawable mBookMarkBorder;
    @BindDrawable(R.drawable.ic_bookmark_clipped) Drawable mBookMarkClipped;
    @BindDrawable(R.drawable.ic_like) Drawable mLike;
    @BindDrawable(R.drawable.ic_like_border) Drawable mLikeBorder;
    @BindString(R.string.fb_share_imageuri) String mFbShareImgUri;

    //facebook share
    CallbackManager mCallbackManager;
    ShareDialog mShareDialog;

    // 메모 뷰 프래그먼트
    private MemoViewFragment mMemoViewFragment;

    // 메모 Vo
    private MemoVo mMemoVo;
    private MemoViewModel mModel;

    // activity result code
    public final static int RESULT_REMOVE = 32;

    //Google Analytics Tracker
    Tracker mTracker;


    //댓글 프레그먼트
    private CommentFragment mCommentFragment;

    // 타 액티비티에서 이 액티비티를 시작할때
    public static Intent getIntent(Context context) {
        return new Intent(context, MemoViewActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_memoview);
        ButterKnife.bind(this);

        MemoSquare application = (MemoSquare) MemoSquare.getContext();
        mTracker = application.getDefaultTracker();

        //Facebook sharing
        mCallbackManager = CallbackManager.Factory.create();

        mShareDialog = new ShareDialog(this);
        mShareDialog.registerCallback(mCallbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Timber.d("onSuccess");
            }

            @Override
            public void onCancel() {
                Timber.d("onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Timber.d("onError");
                Timber.d(error.getMessage());
            }
        });

        // 툴바 설정
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // 컨테이너에 메모 프래그먼트 넣기
        mMemoViewFragment = new MemoViewFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.memoview_container, mMemoViewFragment)
                .commit();

        // 컨테이너에 댓글 프래그먼트 넣기
        mCommentFragment = new CommentFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.comment_container, mCommentFragment)
                .commit();

        // 모델 할당
        mModel = MemoViewModel.getInstance();
        mMemoVo = (MemoVo) getIntent().getSerializableExtra("memo");
        Timber.d(mMemoVo.toString());

        //좋아요 설정
        if (mMemoVo.is_like()) {
            mFab.setImageDrawable(mLike);
        } else {
            mFab.setImageDrawable(mLikeBorder);
        }
    }

    // 메뉴 할당
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Timber.d("onCreateOptionsMenu");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_memo_view, menu);

        if (mMemoVo.is_clipped()) {
            menu.getItem(1).setIcon(mBookMarkClipped);
        }

        return true;
    }

    // 메뉴 클릭시
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            // 뒤로가기 화살표
            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.action_fb_share:
                ShareUtil.shareToFacebook(mShareDialog, mMemoVo);
                break;

            case R.id.action_clip:
                mMemoVo.setIs_clipped(!mMemoVo.is_clipped());
                if (mMemoVo.is_clipped()) {
                    item.setIcon(mBookMarkClipped);
                    mModel.clipMemo(mMemoVo);

                    if(mMemoVo.is_private())
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("Action")
                                .setAction("memo detail-clip(my memo)")
                                .build());
                    else
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("Action")
                                .setAction("memo detail-clip(other memo)")
                                .build());
                } else {
                    item.setIcon(mBookMarkBorder);
                    mModel.notClipMemo(mMemoVo);

                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("memo detail-not clip")
                            .build());
                }
                break;

            case R.id.action_go_web:
                goWeb();

                if(mMemoVo.is_private())
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("memo detail-go web(my memo)")
                            .build());
                else
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("memo detail-go web(other memo)")
                            .build());

                break;
            default:
                Timber.i("wrong option item selected");
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == CommentEditActivity.RESULT_EDIT_COMMENT)
            mCommentFragment.onActivityResult(requestCode, resultCode, data);
        else
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    // 웹에서 보기 버튼 리스너
    private void goWeb() {
        Intent intent = WebBrowserActivity.getIntent(MemoViewActivity.this);
        intent.putExtra(Intent.EXTRA_TEXT, mMemoViewFragment.getMemoVo().getPage());
        startActivity(intent);
        finish();
    }

    // TODO: 2017-01-13 선영: 좋아요 버튼에 onClick Listener 달기
    @OnClick(R.id.fab)
    void fab(){
                if (mMemoVo.is_like()) {
            mFab.setImageDrawable(mLikeBorder);
            mModel.notLikeMemo(mMemoVo);
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction("memoview-click 'not-like' button")
                    .build());
        } else {
            mFab.setImageDrawable(mLike);
            mModel.likeMemo(mMemoVo);
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Action")
                    .setAction("memoview-click 'like' button")
                    .build());
        }
        mMemoVo.setIs_liked(!mMemoVo.is_like());
    }
}
