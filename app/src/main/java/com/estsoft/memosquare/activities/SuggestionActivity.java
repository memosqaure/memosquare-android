package com.estsoft.memosquare.activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.estsoft.memosquare.R;
import com.estsoft.memosquare.fragments.SuggestionFragment;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SuggestionActivity extends AppCompatActivity {

    // 위젯 할당
    @BindView(R.id.suggestion_appbar) AppBarLayout mAppBarLayout;
    @BindView(R.id.suggestion_toolbar) Toolbar mToolbar;
    @BindView(R.id.suggestion_container) FrameLayout mContainer;

    // 리소스 할당
    @BindString(R.string.suggestion_title) String mTitle;

    // 제안 프래그먼트
    private SuggestionFragment mSuggestionFragment;

    public static Intent getIntent(Context context) {
        return new Intent(context, SuggestionActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestion);
        ButterKnife.bind(this);

        // 툴바 설정
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(mTitle);

        // 프래그먼트 할당
        mSuggestionFragment = new SuggestionFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.suggestion_container, mSuggestionFragment)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
