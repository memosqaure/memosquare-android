package com.estsoft.memosquare.mvp.models;

import com.estsoft.memosquare.listeners.MemoSaveEventListener;
import com.estsoft.memosquare.vo.MemoVo;

/**
 * Created by hokyung on 2016. 12. 21..
 */

public interface WebBrowserMemoWriteModel {
    void createMemo(MemoVo memoVo, MemoSaveEventListener listener);
    void updateMemo(MemoVo memoVo, MemoSaveEventListener listener);
}
