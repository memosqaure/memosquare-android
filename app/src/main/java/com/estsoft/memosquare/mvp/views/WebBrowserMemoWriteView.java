package com.estsoft.memosquare.mvp.views;

import com.estsoft.memosquare.vo.MemoVo;

/**
 * Created by hokyung on 2016. 12. 21..
 */

public interface WebBrowserMemoWriteView {
    boolean toggleHighlight();
    void setHighlightImageDisabled();
    void setHighlightImageNormal();
    void setHighlightImageActivated();
    void setSecretImageOpen();
    void setSecretImageClose();
    MemoVo getMemoVo();
    String getPageUrl();
    void showProgressBar();
    void hideProgressBar();
    void setHighlightButtonEnable(boolean enable);
    void onSaveSuccess(MemoVo memoVo);
    void onSaveFail();
}
