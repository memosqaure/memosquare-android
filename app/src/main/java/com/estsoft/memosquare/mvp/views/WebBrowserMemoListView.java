package com.estsoft.memosquare.mvp.views;

import com.estsoft.memosquare.vo.MemoVo;

import java.util.ArrayList;

/**
 * Created by hokyung on 2016. 12. 15..
 */

public interface WebBrowserMemoListView {

    void generateAdapterAndSetToRecView(ArrayList<MemoVo> items);

    void setIsTopAsTrue();

    boolean IsTop();

    String getUrl();

    void setMemoListToAdapter(ArrayList<MemoVo> items);

    void showRecyclerView();

    void showNetworkErrorMessage();

    void showNoMemoMessage();

    void showLoadingMessage();

    void goUp();

    void notifyDataSetChanged();

    void setButtonVisibility(int visibility);
}
