package com.estsoft.memosquare.mvp.presenters;

import android.text.Spanned;

import com.estsoft.memosquare.utils.PicassoImageGetter;

/**
 * Created by hokyung on 2016. 12. 21..
 */

public interface WebBrowserMemoWritePresenter {
    void onCreateView();
    String getDate();
    void setSecret(boolean isSecret);
    void setModify(boolean isModify);
    void setHighlightState(boolean enable);
    void switchHighlight();
    Spanned getOriginalMemoHtmlSpan(String content, PicassoImageGetter picassoImageGetter);
    Spanned getHtmlSpan(String content, PicassoImageGetter picassoImageGetter);
    void setSecretButtonImage();
    void saveMemo();
    String getMemoForSave(Spanned memo);
    void offHighlight(boolean isHighlightAvailable);
}
