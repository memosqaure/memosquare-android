package com.estsoft.memosquare.mvp.presenters;

/**
 * Created by hokyung on 2016. 12. 20..
 */

public interface CategoryPresenter {

    void onCreateView();

    void initializeOffset();

    void onDestroy();

    void removeCategorisedMemo(int index);

    void loadCategorisedMemos();

    void setUpButtonVisibility(boolean isTop);
}
