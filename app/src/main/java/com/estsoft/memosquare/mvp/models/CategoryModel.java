package com.estsoft.memosquare.mvp.models;

import com.estsoft.memosquare.listeners.GetMemoListListener;

/**
 * Created by hokyung on 2016. 12. 20..
 */

public interface CategoryModel {
    void getCategorisedMemoList(int offset, int categoryPk, GetMemoListListener listener);
}
