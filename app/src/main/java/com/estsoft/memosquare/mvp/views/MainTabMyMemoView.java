package com.estsoft.memosquare.mvp.views;

import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.estsoft.memosquare.vo.MemoVo;

import java.util.ArrayList;

public interface MainTabMyMemoView {

    void generateAdapterAndSetToRecView(ArrayList<MemoVo> items);

    void setIsTopAsTrue();

    boolean IsTop();

    void showProgress();

    void hideProgress();

    void setMemoListToAdapter(ArrayList<MemoVo> items);

    void showRecyclerView();

    void showNetworkErrorMessage();

    void showNoMemoMessage();

    void goUp();

    void notifyDataChangedToAdapter();

    void setButtonVisibility(int visibility);

    void removeMemo(int index);
}
