package com.estsoft.memosquare.mvp.presenters;

import android.view.View;

import com.estsoft.memosquare.listeners.GetMemoListListener;
import com.estsoft.memosquare.mvp.models.WebBrowserMemoListModel;
import com.estsoft.memosquare.mvp.models.WebBrowserMemoListModelImpl;
import com.estsoft.memosquare.mvp.views.WebBrowserMemoListView;
import com.estsoft.memosquare.vo.MemoVo;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by hokyung on 2016. 12. 15..
 */

public class WebBrowserMemoListPresenterImpl
        implements WebBrowserMemoListPresenter, GetMemoListListener {

    private WebBrowserMemoListView mWebBrowserMemoListView;
    private WebBrowserMemoListModel mWebBrowserMemoListModel;

    private int mOffset;

    private ArrayList<MemoVo> mList;

    public WebBrowserMemoListPresenterImpl(WebBrowserMemoListView webBrowserMemoListView) {
        this(webBrowserMemoListView, new  WebBrowserMemoListModelImpl());
    }

    public WebBrowserMemoListPresenterImpl(WebBrowserMemoListView webBrowserMemoListView,
                                           WebBrowserMemoListModel webBrowserMemoListModel) {
        mWebBrowserMemoListView = webBrowserMemoListView;
        mWebBrowserMemoListModel = webBrowserMemoListModel;
    }

    @Override
    public void onCreateView() {
        mList = new ArrayList<>();
        initializeOffset();

        mWebBrowserMemoListView.generateAdapterAndSetToRecView(mList);
    }

    @Override
    public void initializeOffset() {
        mOffset = 0;
    }

    @Override
    public void onDestroy() {
        mWebBrowserMemoListView = null;
        mWebBrowserMemoListModel = null;
    }

    @Override
    public void loadPageMemos() {
        if (mOffset >= 0) {
            mWebBrowserMemoListModel.getPageMemos(mOffset, mWebBrowserMemoListView.getUrl(), this);
        }
    }

    @Override
    public void clearMemoList() {
        mWebBrowserMemoListView.showLoadingMessage();
        mList = new ArrayList<>();
        Timber.d("테 프 클리어 리스트 " + mList);
        mWebBrowserMemoListView.setMemoListToAdapter(mList);
    }

    @Override
    public void setUpButtonVisibility(boolean isTop) {
        if (isTop) {
            mWebBrowserMemoListView.setButtonVisibility(View.GONE);
        } else {
            mWebBrowserMemoListView.setButtonVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onGetMemoListSuccess(ArrayList<MemoVo> mMemoList, int offset) {
        mList.addAll(mMemoList);
        mOffset = offset;

        if (mList.size() == 0) {
            mWebBrowserMemoListView.showNoMemoMessage();
            mWebBrowserMemoListView.setIsTopAsTrue();
        } else {
            Timber.d("테 프 온 겟 메모리스트 석세스 " + mList);
            mWebBrowserMemoListView.showRecyclerView();
            mWebBrowserMemoListView.setMemoListToAdapter(mList);
            mWebBrowserMemoListView.notifyDataSetChanged();
        }
    }

    @Override
    public void onGetMemoListFail() {
        mWebBrowserMemoListView.showNetworkErrorMessage();
    }
}
