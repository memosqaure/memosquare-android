package com.estsoft.memosquare.mvp.models;

import com.estsoft.memosquare.vo.CategoryVo;

import java.util.ArrayList;

public interface MainDrawerModel {
    void addCategory(String name, MainDrawerModelListener listener);
    void getCategoryList(MainDrawerModelListener listener);
    void updateCategory(CategoryVo categoryVo, MainDrawerModelListener listener);
    void removeCategory(CategoryVo categoryVo, MainDrawerModelListener listener);

    interface MainDrawerModelListener {
        void onGetCategoryListSuccess(ArrayList<CategoryVo> categoryList);
        void onSaveSuccess(CategoryVo categoryVo);
        void onUpdateSuccess(CategoryVo categoryVo);
        void onRemoveSuccess();
        void onFailed();
    }
}
