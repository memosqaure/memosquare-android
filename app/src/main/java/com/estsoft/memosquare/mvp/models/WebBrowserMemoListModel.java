package com.estsoft.memosquare.mvp.models;

import com.estsoft.memosquare.listeners.GetMemoListListener;

/**
 * Created by hokyung on 2016. 12. 15..
 */

public interface WebBrowserMemoListModel {
    void getPageMemos(int offset, String url, GetMemoListListener mCallback);
}
