package com.estsoft.memosquare.mvp.models;

import com.estsoft.memosquare.listeners.GetMemoListListener;
import com.estsoft.memosquare.vo.MemoVo;

import java.util.ArrayList;

public interface MainTabMyMemoModel {

    void getMyMemoList(int offset, GetMemoListListener listener);
}
