package com.estsoft.memosquare.mvp.presenters;

import com.estsoft.memosquare.vo.CategoryVo;

/**
 * Created by hokyung on 2016. 12. 19..
 */

public interface MainDrawerPresenter {

    void setDefaultCategoryName(String defaultCategoryName);

    void onCreateView();

    void onResume();

    void addCategoryToServer(String name);

    void loadCategories();
}
