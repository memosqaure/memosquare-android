package com.estsoft.memosquare.mvp.models;

import com.estsoft.memosquare.database.Repository.CategoryRepository;
import com.estsoft.memosquare.vo.CategoryVo;


public class MainDrawerModelImpl implements MainDrawerModel {
    private CategoryRepository mCategoryRepository;

    public MainDrawerModelImpl() {
        mCategoryRepository = CategoryRepository.getInstance();
    }

    @Override
    public void addCategory(String name, MainDrawerModelListener listener) {
        CategoryVo categoryVo = new CategoryVo();
        categoryVo.setName(name);
        mCategoryRepository.addCategory(categoryVo, listener);
    }

    @Override
    public void getCategoryList(MainDrawerModelListener listener) {
        mCategoryRepository.getList(listener);
    }

    @Override
    public void updateCategory(CategoryVo categoryVo, MainDrawerModelListener listener) {
        mCategoryRepository.updateCategory(categoryVo, listener);
    }

    @Override
    public void removeCategory(CategoryVo categoryVo, MainDrawerModelListener listener) {
        mCategoryRepository.removeCategory(categoryVo, listener);
    }
}
