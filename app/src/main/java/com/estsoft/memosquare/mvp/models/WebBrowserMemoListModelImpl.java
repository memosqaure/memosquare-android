package com.estsoft.memosquare.mvp.models;

import com.estsoft.memosquare.database.Repository.MemoRepository;
import com.estsoft.memosquare.enums.MemoListType;
import com.estsoft.memosquare.listeners.GetMemoListListener;


public class WebBrowserMemoListModelImpl implements WebBrowserMemoListModel {

    private static MemoRepository mRepository;

    public WebBrowserMemoListModelImpl() {
        mRepository = MemoRepository.getInstance();
    }

    @Override
    public void getPageMemos(int offset, String url, GetMemoListListener mCallback) {
        MemoRepository.getMemoList(MemoListType.PAGE, offset, url,  mCallback);
    }
}
