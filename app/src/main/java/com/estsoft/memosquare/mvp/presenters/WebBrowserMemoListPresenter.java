package com.estsoft.memosquare.mvp.presenters;

/**
 * Created by hokyung on 2016. 12. 15..
 */

public interface WebBrowserMemoListPresenter {

    void onCreateView();

    void initializeOffset();

    void onDestroy();

    void loadPageMemos();

    void clearMemoList();

    void setUpButtonVisibility(boolean isTop);
}
