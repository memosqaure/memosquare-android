package com.estsoft.memosquare.mvp.presenters;

import android.text.Html;
import android.text.Spanned;

import com.estsoft.memosquare.listeners.MemoSaveEventListener;
import com.estsoft.memosquare.mvp.models.WebBrowserMemoWriteModel;
import com.estsoft.memosquare.mvp.models.WebBrowserMemoWriteModelImpl;
import com.estsoft.memosquare.mvp.views.WebBrowserMemoWriteView;
import com.estsoft.memosquare.utils.HtmlTagUtil;
import com.estsoft.memosquare.utils.PicassoImageGetter;
import com.estsoft.memosquare.vo.MemoVo;

import org.apache.commons.lang3.StringEscapeUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import timber.log.Timber;

/**
 * Created by hokyung on 2016. 12. 21..
 */

public class WebBrowserMemoWritePresenterImpl implements WebBrowserMemoWritePresenter, MemoSaveEventListener {

    // 시스템 언어 확인용 상수
    private final static String KOREAN = "한국어";

    private WebBrowserMemoWriteView mView;
    private WebBrowserMemoWriteModel mModel;

    private boolean mIsHighlighting;    // 현재 하이라이팅 중인지 채크하는 변수
    private boolean mIsSecret;
    private boolean mIsModify;
    private String mDate;

    public WebBrowserMemoWritePresenterImpl(WebBrowserMemoWriteView view) {
        mView = view;
        mModel = WebBrowserMemoWriteModelImpl.getInstance();
    }

    @Override
    public void onCreateView() {
        mIsHighlighting = false;
        mIsSecret = true;
        setSecretButtonImage();

        // 실행 언어를 확인한다
        String language = Locale.getDefault().getDisplayLanguage();

        // 날짜 변수 설정
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format;
        if (language.equals(KOREAN)) {
            format = new SimpleDateFormat("LLL dd일, yyyy");
        } else {
            format = new SimpleDateFormat("LLL dd, yyyy");
        }
        mDate = format.format(calendar.getTime());
    }

    @Override
    public String getDate() {
        return mDate;
    }

    @Override
    public void setSecret(boolean isSecret) {
        mIsSecret = isSecret;
        if (mIsSecret)
            mView.setSecretImageClose();
        else
            mView.setSecretImageOpen();
    }

    @Override
    public void setModify(boolean isModify) {
        mIsModify = isModify;
    }

    @Override
    public void setHighlightState(boolean enable) {
        // 하이라이트 상태 보존
        if (enable && mIsHighlighting) {
            mView.setHighlightImageActivated();
        }
        else if (enable && !mIsHighlighting) {
            mView.setHighlightImageNormal();
        }
        // 불가능하면 아예 흐리게
        else {
            mView.setHighlightImageDisabled();
        }
    }

    @Override
    public void switchHighlight() {
        boolean result = mView.toggleHighlight();
        if (!result) {
            return;
        }

        if (mIsHighlighting)
            mView.setHighlightImageNormal();
        else
            mView.setHighlightImageActivated();

        mIsHighlighting = !mIsHighlighting;
    }

    @Override
    public Spanned getOriginalMemoHtmlSpan(String content, PicassoImageGetter picassoImageGetter) {
        return Html.fromHtml(content, picassoImageGetter, null);
    }

    @Override
    public Spanned getHtmlSpan(String content, PicassoImageGetter picassoImageGetter) {
        // content 앞뒤 공백 제거
        content = content.trim();

        // 컨텐츠 태그 치환
        content = HtmlTagUtil.convertBrToNewLine(content);
        content = HtmlTagUtil.sealImgTags(content);
        content = HtmlTagUtil.removeAllTags(content);
        content = HtmlTagUtil.unsealImgTags(content);
        content = HtmlTagUtil.removeEncodedAngleBrackets(content);
        content = HtmlTagUtil.convertNewLineToBr(content);
        Timber.d("치환 후 컨텐츠\n" + content);

        content = content.trim() + "<br>";

        return Html.fromHtml(content, picassoImageGetter, null);
    }

    private String getDecodedHtml(Spanned text) {
        String htmlEncodedString = Html.toHtml(text);
        return StringEscapeUtils.unescapeHtml4(htmlEncodedString);
    }

    @Override
    public void setSecretButtonImage() {
        // 비공개 상태일경우
        if (mIsSecret) {
            mView.setSecretImageOpen();
            mIsSecret = false;
        }
        // 공개 상태일경우
        else {
            mView.setSecretImageClose();
            mIsSecret = true;
        }
    }

    @Override
    public void saveMemo() {
        mView.showProgressBar();

        MemoVo memoVo = mView.getMemoVo();
        memoVo.setPage(mView.getPageUrl());
        memoVo.setIs_private(mIsSecret);

        Timber.d("save memoVo " + memoVo);

        if (mIsModify)
            mModel.updateMemo(memoVo, this);
        else
            mModel.createMemo(memoVo, this);
    }

    @Override
    public String getMemoForSave(Spanned memo) {
        String content = getDecodedHtml(memo);

        // 연속된 br과 p일경우 br 을 제거한다
        content = HtmlTagUtil.trimSerialBrP(content);
        // 밑줄을 지운다
        content = HtmlTagUtil.removeUnderLine(content);

        Timber.d("before substring " + content);

        // 가장자리의 <p> 를 없앤다
        int begin = 13;
        int end = content.length() - 5;
        String result = content.substring(begin, end);

        Timber.d("after substring " + result);

        // 맨뒤에 \n 가 있을경우 제거한다
        result = result.trim();

        return result;
    }

    @Override
    public void offHighlight(boolean isHighlightAvailable) {
        if (mIsHighlighting) {
            mIsHighlighting = false;
            mView.setHighlightButtonEnable(isHighlightAvailable);
            mView.toggleHighlight();
        }
    }

    @Override
    public void onMemoSaveSuccess(MemoVo mMemoVo) {
        mView.onSaveSuccess(mMemoVo);
        mView.hideProgressBar();
    }

    @Override
    public void onMemoSaveFail() {
        mView.onSaveFail();
        mView.hideProgressBar();
    }
}
