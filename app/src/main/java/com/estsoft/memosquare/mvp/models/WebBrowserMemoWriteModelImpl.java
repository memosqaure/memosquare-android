package com.estsoft.memosquare.mvp.models;

import com.estsoft.memosquare.database.Repository.MemoRepository;
import com.estsoft.memosquare.listeners.MemoSaveEventListener;
import com.estsoft.memosquare.vo.MemoVo;

/**
 * Created by hokyung on 2016. 12. 21..
 */

public class WebBrowserMemoWriteModelImpl implements WebBrowserMemoWriteModel {

    private static WebBrowserMemoWriteModel sWebBrowserMemoWriteModel;
    private static MemoRepository sRepository;

    private WebBrowserMemoWriteModelImpl() {
        sRepository = MemoRepository.getInstance();
    }

    public static WebBrowserMemoWriteModel getInstance() {
        if (sWebBrowserMemoWriteModel == null) {
            sWebBrowserMemoWriteModel = new WebBrowserMemoWriteModelImpl();
        }
        return sWebBrowserMemoWriteModel;
    }

    @Override
    public void createMemo(MemoVo memoVo, MemoSaveEventListener listener) {
        sRepository.createMemo(memoVo, listener);
    }

    @Override
    public void updateMemo(MemoVo memoVo, MemoSaveEventListener listener) {
        sRepository.updateMemo(memoVo, listener);
    }
}
