package com.estsoft.memosquare.mvp.models;

import com.estsoft.memosquare.database.Repository.MemoRepository;
import com.estsoft.memosquare.enums.MemoListType;
import com.estsoft.memosquare.listeners.GetMemoListListener;

/**
 * Created by hokyung on 2016. 12. 20..
 */

public class CategoryModelImpl implements CategoryModel {

    private MemoRepository mRepository;

    public CategoryModelImpl() {
        mRepository = MemoRepository.getInstance();
    }

    @Override
    public void getCategorisedMemoList(int offset, int categoryPk, GetMemoListListener listener) {
        mRepository.getMemoList(MemoListType.MY, offset, categoryPk, listener);
    }
}
