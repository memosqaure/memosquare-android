package com.estsoft.memosquare.mvp.presenters;

import android.view.View;

import com.estsoft.memosquare.listeners.GetMemoListListener;
import com.estsoft.memosquare.mvp.models.CategoryModel;
import com.estsoft.memosquare.mvp.models.CategoryModelImpl;
import com.estsoft.memosquare.mvp.views.CategorisedMemoView;
import com.estsoft.memosquare.mvp.views.MainTabMyMemoView;
import com.estsoft.memosquare.vo.MemoVo;

import java.util.ArrayList;

/**
 * Created by hokyung on 2016. 12. 20..
 */

public class CategoryPresenterImpl implements CategoryPresenter, GetMemoListListener {

    private CategorisedMemoView mCategorisedMemoView;
    private CategoryModel mCategoryModel;

    private int mOffset;

    private ArrayList<MemoVo> mList;

    public CategoryPresenterImpl(CategorisedMemoView categorisedMemoView) {
        mCategorisedMemoView = categorisedMemoView;
        mCategoryModel = new CategoryModelImpl();
    }

    @Override
    public void onCreateView() {
        mCategorisedMemoView.showProgress();

        mList = new ArrayList<>();
        mCategorisedMemoView.generateAdapterAndSetToRecView(mList);

        initializeOffset();

        loadCategorisedMemos();
    }

    @Override
    public void initializeOffset() {
        mOffset = 0;
    }

    @Override
    public void onDestroy() {
        mCategorisedMemoView = null;
    }

    @Override
    public void removeCategorisedMemo(int index) {
        mList.remove(index);
        mCategorisedMemoView.notifyDataChangedToAdapter();
    }

    @Override
    public void loadCategorisedMemos() {
        if (mOffset >= 0) {
            mCategoryModel.getCategorisedMemoList(mOffset, mCategorisedMemoView.getCategoryPk(), this);
        }
    }

    @Override
    public void setUpButtonVisibility(boolean isTop) {
        if (isTop) {
            mCategorisedMemoView.setButtonVisibility(View.GONE);
        } else {
            mCategorisedMemoView.setButtonVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onGetMemoListSuccess(ArrayList<MemoVo> mMemoList, int offset) {
        mCategorisedMemoView.hideProgress();
        mList.addAll(mMemoList);
        mOffset = offset;

        if (mList.size() == 0) {
            mCategorisedMemoView.showNoMemoMessage();
            mCategorisedMemoView.setIsTopAsTrue();
        } else {
            mCategorisedMemoView.showRecyclerView();
            mCategorisedMemoView.setMemoListToAdapter(mList);
            mCategorisedMemoView.notifyDataChangedToAdapter();
        }
    }

    @Override
    public void onGetMemoListFail() {
        mCategorisedMemoView.hideProgress();
        mCategorisedMemoView.showNetworkErrorMessage();
    }
}
