package com.estsoft.memosquare.mvp.views;

import com.estsoft.memosquare.vo.MemoVo;

import java.util.ArrayList;

public interface CategorisedMemoView {

    int getCategoryPk();

    void generateAdapterAndSetToRecView(ArrayList<MemoVo> items);

    void setIsTopAsTrue();

    boolean IsTop();

    void showProgress();

    void hideProgress();

    void setMemoListToAdapter(ArrayList<MemoVo> items);

    void showRecyclerView();

    void showNetworkErrorMessage();

    void showNoMemoMessage();

    void goUp();

    void notifyDataChangedToAdapter();

    void setButtonVisibility(int visibility);

    void removeMemo(int index);
}
