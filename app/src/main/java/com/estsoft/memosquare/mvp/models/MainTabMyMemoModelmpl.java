package com.estsoft.memosquare.mvp.models;

import com.estsoft.memosquare.database.Repository.MemoRepository;
import com.estsoft.memosquare.enums.MemoListType;
import com.estsoft.memosquare.listeners.GetMemoListListener;

public class MainTabMyMemoModelmpl implements MainTabMyMemoModel {

    private MemoRepository mRepository;

    public MainTabMyMemoModelmpl() {
        mRepository = MemoRepository.getInstance();
    }

    @Override
    public void getMyMemoList(int offset, final GetMemoListListener listener) {
        mRepository.getMemoList(MemoListType.MY, offset, listener);
    }
}
