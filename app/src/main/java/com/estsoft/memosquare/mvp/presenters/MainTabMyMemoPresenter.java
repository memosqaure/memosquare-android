package com.estsoft.memosquare.mvp.presenters;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;

import com.estsoft.memosquare.utils.EndlessRecyclerOnScrollListener;

public interface MainTabMyMemoPresenter {

    void onCreateView();

    void initializeOffset();

    void onDestroy();

    void removeMymemo(int index);

    void loadMyMemos();

    void setUpButtonVisibility(boolean isTop);
}