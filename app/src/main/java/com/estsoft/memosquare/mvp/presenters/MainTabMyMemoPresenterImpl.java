package com.estsoft.memosquare.mvp.presenters;

import android.view.View;

import com.estsoft.memosquare.listeners.GetMemoListListener;
import com.estsoft.memosquare.mvp.views.MainTabMyMemoView;
import com.estsoft.memosquare.mvp.models.MainTabMyMemoModel;
import com.estsoft.memosquare.mvp.models.MainTabMyMemoModelmpl;
import com.estsoft.memosquare.utils.EndlessRecyclerOnScrollListener;
import com.estsoft.memosquare.vo.MemoVo;

import java.util.ArrayList;

public class MainTabMyMemoPresenterImpl implements MainTabMyMemoPresenter, GetMemoListListener {

    private MainTabMyMemoView mMainTabMyMemoView;
    private MainTabMyMemoModel mMainTabMyMemoModel;

    private int mOffset;

    private ArrayList<MemoVo> mList;

    public MainTabMyMemoPresenterImpl(MainTabMyMemoView mainTabMyMemoView) {
        mMainTabMyMemoView = mainTabMyMemoView;
        mMainTabMyMemoModel = new MainTabMyMemoModelmpl();
    }

    @Override
    public void onCreateView() {

        mMainTabMyMemoView.showProgress();

        mList = new ArrayList<MemoVo>();
        mMainTabMyMemoView.generateAdapterAndSetToRecView(mList);

        initializeOffset();

        loadMyMemos();
    }

    @Override
    public void initializeOffset() {
        mOffset = 0;
    }

    @Override
    public void onDestroy() {
        mMainTabMyMemoView = null;
    }

    @Override
    public void removeMymemo(int index) {
        mList.remove(index);
        mMainTabMyMemoView.setMemoListToAdapter(mList);
        mMainTabMyMemoView.notifyDataChangedToAdapter();
    }

    @Override
    public void loadMyMemos() {
        if (mOffset >= 0) {
            mMainTabMyMemoModel.getMyMemoList(mOffset, this);

        }
    }

    //onGetMemoList Listener
    @Override
    public void onGetMemoListSuccess(ArrayList<MemoVo> mMemoList, int newOffset) {

        mMainTabMyMemoView.hideProgress();
        mList.addAll(mMemoList);
        mOffset = newOffset;

        if (mList.size() == 0) {
            mMainTabMyMemoView.showNoMemoMessage();
            mMainTabMyMemoView.setIsTopAsTrue();
        } else {
            mMainTabMyMemoView.showRecyclerView();
            mMainTabMyMemoView.setMemoListToAdapter(mList);
            mMainTabMyMemoView.notifyDataChangedToAdapter();
        }
    }

    @Override
    public void onGetMemoListFail() {
        mMainTabMyMemoView.hideProgress();
        mMainTabMyMemoView.showNetworkErrorMessage();
    }


    @Override
    public void setUpButtonVisibility(boolean isTop) {
//        if (mList.isEmpty()) {
//            mIsTop = true;
//        } else {
//            mIsTop = isTop;
//        } --> 74로 옮김

        if (isTop) {
            mMainTabMyMemoView.setButtonVisibility(View.GONE);
        } else {
            mMainTabMyMemoView.setButtonVisibility(View.VISIBLE);
        }
    }
}


