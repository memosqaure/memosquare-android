package com.estsoft.memosquare.mvp.views;

import com.estsoft.memosquare.vo.CategoryVo;

import java.util.ArrayList;

/**
 * Created by hokyung on 2016. 12. 19..
 */

public interface MainDrawerView {
    void generateAdapterAndSetToRecView(ArrayList<CategoryVo> items);

    void setCategoryListToAdapter(ArrayList<CategoryVo> items);

    void addCategoryToRecycler(CategoryVo categoryVo);

    void showNetworkErrorMessage();

    void resetLocalCategoryDb(ArrayList<CategoryVo> list);
}
