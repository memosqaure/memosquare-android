package com.estsoft.memosquare.mvp.presenters;

import android.content.Context;

import com.estsoft.memosquare.R;
import com.estsoft.memosquare.mvp.models.MainDrawerModel;
import com.estsoft.memosquare.mvp.models.MainDrawerModelImpl;
import com.estsoft.memosquare.mvp.views.MainDrawerView;
import com.estsoft.memosquare.vo.CategoryVo;

import java.util.ArrayList;

public class MainDrawerPresenterImpl implements MainDrawerPresenter, MainDrawerModel.MainDrawerModelListener {

    private MainDrawerView mMainDrawerView;
    private MainDrawerModel mMainDrawerModel;

    private ArrayList<CategoryVo> mList;
    private String mDefaultCategoryName;

    public MainDrawerPresenterImpl(MainDrawerView view) {
        mMainDrawerView = view;
        mMainDrawerModel = new MainDrawerModelImpl();
    }

    @Override
    public void setDefaultCategoryName(String defaultCategoryName) {
        mDefaultCategoryName = defaultCategoryName;
    }

    @Override
    public void onCreateView() {
        mList = new ArrayList<>();
        mMainDrawerView.generateAdapterAndSetToRecView(mList);

        loadCategories();
    }

    @Override
    public void onResume() {
        loadCategories();
    }

    @Override
    public void addCategoryToServer(String name) {
        mMainDrawerModel.addCategory(name, this);
    }

    @Override
    public void loadCategories() {
        mMainDrawerModel.getCategoryList(this);
    }

    @Override
    public void onGetCategoryListSuccess(ArrayList<CategoryVo> categoryList) {
        // 뷰에 적용
        mList.clear();

        CategoryVo defaultCategory = new CategoryVo();
        defaultCategory.setPk(0);
        defaultCategory.setName(mDefaultCategoryName);
        mList.add(defaultCategory);

        mList.addAll(categoryList);
        mMainDrawerView.setCategoryListToAdapter(mList);

        // sqlite에 적용
        mMainDrawerView.resetLocalCategoryDb(categoryList);
    }

    @Override
    public void onSaveSuccess(CategoryVo categoryVo) {
        mMainDrawerView.addCategoryToRecycler(categoryVo);
    }

    @Override
    public void onUpdateSuccess(CategoryVo categoryVo) {
        // 카테고리 설정에서 쓰이는 메소드
    }

    @Override
    public void onRemoveSuccess() {
        mList.clear();
        loadCategories();
    }

    @Override
    public void onFailed() {
        mMainDrawerView.showNetworkErrorMessage();
    }
}
