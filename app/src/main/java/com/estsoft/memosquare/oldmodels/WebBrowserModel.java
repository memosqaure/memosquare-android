package com.estsoft.memosquare.oldmodels;

import com.estsoft.memosquare.database.Repository.CategoryRepository;
import com.estsoft.memosquare.database.Repository.MemoRepository;
import com.estsoft.memosquare.enums.MemoListType;
import com.estsoft.memosquare.listeners.GetMemoListListener;
import com.estsoft.memosquare.listeners.MainDrawerEventListener;
import com.estsoft.memosquare.listeners.MemoSaveEventListener;
import com.estsoft.memosquare.mvp.models.MainDrawerModel;
import com.estsoft.memosquare.vo.MemoVo;

/**
 * Created by hokyung on 2016. 11. 11..
 * 웹브라우저 액티비티 및 프래그먼트를 위한 모델
 */

public class WebBrowserModel {

    private static MemoRepository mRepository = MemoRepository.getInstance();
    private static CategoryRepository categoryRepository = CategoryRepository.getInstance();

    private static WebBrowserModel sWebBrowserModel;

    private WebBrowserModel() {

    }

    public static WebBrowserModel getInstance() {
        if (sWebBrowserModel == null) {
            sWebBrowserModel = new WebBrowserModel();
        }
        return sWebBrowserModel;
    }

    //WebBrowserMemoListFragment
    public static void getPageMemos(int offset, String url, GetMemoListListener mCallback){
        MemoRepository.getMemoList(MemoListType.PAGE, offset, url,  mCallback);
    }


    /**메모 저장 시*/
    public static void updateMemo(MemoVo memo, MemoSaveEventListener mCallback){
        MemoRepository.updateMemo(memo, mCallback);
    }

    public static void createMemo(MemoVo memo, MemoSaveEventListener mCallback){
        MemoRepository.createMemo(memo, mCallback);
    }
}
