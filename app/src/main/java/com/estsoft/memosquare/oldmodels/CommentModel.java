package com.estsoft.memosquare.oldmodels;

import com.estsoft.memosquare.database.Repository.CommentRepository;
import com.estsoft.memosquare.enums.MemoAction;
import com.estsoft.memosquare.listeners.AddCommentListener;
import com.estsoft.memosquare.listeners.DeleteCommentListener;
import com.estsoft.memosquare.listeners.GetCommentsListener;
import com.estsoft.memosquare.listeners.UpdateCommentListener;
import com.estsoft.memosquare.vo.CommentVo;
import com.estsoft.memosquare.vo.MemoVo;

public class CommentModel {

    private static CommentRepository mCommentRepository = CommentRepository.getInstance();

    private static CommentModel sCommentModel;
    private CommentModel(){
    }
    public static CommentModel getInstance() {
        if (sCommentModel == null) {
            sCommentModel = new CommentModel();
        }
        return sCommentModel;
    }


    public static void createComment(MemoVo memo, String content, AddCommentListener listener){
        mCommentRepository.addComment(memo, content, listener);
    }

    public static void getCommentList(MemoVo memo, GetCommentsListener listener){
        mCommentRepository.getCommentList(memo, listener);
    }

    public static void updateComment(CommentVo comment, String content, UpdateCommentListener listener){
        mCommentRepository.updateComment(comment, content, listener);
    }

    public static void removeComment(CommentVo comment, DeleteCommentListener listener){
        mCommentRepository.removeComment(comment, listener);
    }

}
