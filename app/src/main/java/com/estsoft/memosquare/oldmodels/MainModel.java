package com.estsoft.memosquare.oldmodels;

import com.estsoft.memosquare.database.Repository.MemoRepository;
import com.estsoft.memosquare.database.Repository.UtilRepository;
import com.estsoft.memosquare.enums.MemoAction;
import com.estsoft.memosquare.enums.MemoListType;
import com.estsoft.memosquare.listeners.GetMemoListListener;
import com.estsoft.memosquare.listeners.SendSuggestionEventListener;
import com.estsoft.memosquare.vo.MemoVo;

public class MainModel {
    private static MemoRepository mRepository = MemoRepository.getInstance();
    private static UtilRepository mUtilRepository = UtilRepository.getInstance();

    private static MainModel sMainModel;
    private MainModel(){ }
    public static MainModel getInstance() {
        if (sMainModel == null) {
            sMainModel = new MainModel();
        }
        return sMainModel;
    }

    public static void getMyMemos(int offset, GetMemoListListener mCallback){
        mRepository.getMemoList(MemoListType.MY, offset, mCallback);
    }

    public static void getClippedMemos(int offset, GetMemoListListener mCallback){
        mRepository.getMemoList(MemoListType.CLIPPED, offset, mCallback);
    }

    public static void clipMemo(MemoVo memo){
        mRepository.requestOnResponseEmpty(memo, MemoAction.CLIP);
    }

    public static void notClipMemo(MemoVo memo){
        mRepository.requestOnResponseEmpty(memo, MemoAction.NOTCLIP);
    }

    public static void likeMemo(MemoVo memoVo) {
        mRepository.requestOnResponseEmpty(memoVo, MemoAction.LIKE);
    }

    public static void notLikeMemo(MemoVo memoVo) {
        mRepository.requestOnResponseEmpty(memoVo, MemoAction.NOTLIKE);
    }

    public static void sendSuggestion(String content, SendSuggestionEventListener mCallback){
        mUtilRepository.sendSuggestion(content, mCallback);
    }

}
