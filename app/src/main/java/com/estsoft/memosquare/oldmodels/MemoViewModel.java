package com.estsoft.memosquare.oldmodels;

import com.estsoft.memosquare.database.Repository.CommentRepository;
import com.estsoft.memosquare.database.Repository.MemoRepository;
import com.estsoft.memosquare.enums.MemoAction;
import com.estsoft.memosquare.listeners.AddCommentListener;
import com.estsoft.memosquare.listeners.GetCommentsListener;
import com.estsoft.memosquare.vo.MemoVo;

import org.w3c.dom.Comment;

/**
 * Created by hokyung on 2016. 11. 11..
 * 메모 보기 액티비티와 그 프래그먼트에서 사용될 모델
 */

public class MemoViewModel {
    private static MemoRepository mRepository = MemoRepository.getInstance();
    private static CommentRepository mCommentRepository = CommentRepository.getInstance();

    private static MemoViewModel sMemoViewModel;
    private MemoViewModel(){
    }
    public static MemoViewModel getInstance() {
        if (sMemoViewModel == null) {
            sMemoViewModel = new MemoViewModel();
        }
        return sMemoViewModel;
    }


    // 1. MemoViewActivity
    // clip 메소드

    public static void clipMemo(MemoVo memoVo) {
        MemoRepository.requestOnResponseEmpty(memoVo, MemoAction.CLIP);
    }

    // not clip 메소드
    public static void notClipMemo(MemoVo memoVo) {
        MemoRepository.requestOnResponseEmpty(memoVo,MemoAction.NOTCLIP);
    }

    //2.MemoViewFragment
    public static void lockToggle(MemoVo memoVo) {
        MemoRepository.requestOnResponseEmpty(memoVo,MemoAction.LOCKTOGGLE);
    }

    public static void deleteMemo(MemoVo memoVo) {
        MemoRepository.requestOnResponseEmpty(memoVo,MemoAction.DELETE);

    }
    //좋아요
    public static void likeMemo(MemoVo memoVo) {
        MemoRepository.requestOnResponseEmpty(memoVo, MemoAction.LIKE);
    }

    public static void notLikeMemo(MemoVo memoVo) {
        MemoRepository.requestOnResponseEmpty(memoVo, MemoAction.NOTLIKE);
    }

}
