package com.estsoft.memosquare.oldmodels;

import android.content.Context;
import android.os.Bundle;

import com.estsoft.memosquare.database.Repository.UserRepository;
import com.estsoft.memosquare.listeners.GetFbUserInfoFromFbServerListener;
import com.estsoft.memosquare.listeners.SignInServerListener;
import com.estsoft.memosquare.preferences.UserPreferences;
import com.estsoft.memosquare.vo.UserVo;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import org.json.JSONException;
import org.json.JSONObject;

import timber.log.Timber;

public class WelcomeModel {

    private static WelcomeModel sWelcomeModel;
    private WelcomeModel(){ }
    public static WelcomeModel getInstance() {
        if (sWelcomeModel == null) {
            sWelcomeModel = new WelcomeModel();
        }
        return sWelcomeModel;
    }

    public static void getFbUserInfoFromFbServer(LoginResult result, final GetFbUserInfoFromFbServerListener mCallback){
        GraphRequest request = GraphRequest.newMeRequest(
                result.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback(){
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Timber.i("response: "+response);
                            mCallback.onGetFbUserInfoFromFbServerSuccess(object);
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    public static void setFbUserInPreference(JSONObject object, Context context) throws JSONException{
        UserVo mUserVo = new UserVo();
        mUserVo.setId(object.getString("id"));
        mUserVo.setEmail(object.getString("email"));
        mUserVo.setName(object.getString("name"));
        mUserVo.setPicture_url("https://graph.facebook.com/" + mUserVo.getId() + "/picture?width=300&amp;height=300");

        UserPreferences.setCurrentUser(mUserVo, context);
    }

    public static void setGlUserInPreference(GoogleSignInAccount acct, Context context) throws JSONException{
        UserVo mUserVo = new UserVo();
        mUserVo.setId(acct.getId());
        mUserVo.setEmail(acct.getEmail());
        mUserVo.setName(acct.getDisplayName());
        mUserVo.setPicture_url(acct.getPhotoUrl().toString());

        UserPreferences.setCurrentUser(mUserVo, context);
    }

    public static void signInServer(String token, SignInServerListener callback){
        UserRepository.signInServer(token, callback);
    }
}
