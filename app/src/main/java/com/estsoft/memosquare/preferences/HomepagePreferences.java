package com.estsoft.memosquare.preferences;

import android.content.Context;

/**
 * Created by hokyung on 2016. 11. 30..
 */

public class HomepagePreferences {
    public static void setHomepage(String homepage, Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "home_prefs", 0);
        complexPreferences.putObject("current_homepage", homepage);
        complexPreferences.commit();
    }

    public static String getCurrentHomepage(Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "home_prefs", 0);
        String homepage = complexPreferences.getObject("current_homepage", String.class);
        return homepage;
    }

    public static void clearCurrentHomepage(Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "home_prefs", 0);
        complexPreferences.clearObject();
        complexPreferences.commit();
    }
}
