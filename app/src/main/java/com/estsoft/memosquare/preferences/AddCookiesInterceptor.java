package com.estsoft.memosquare.preferences;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.estsoft.memosquare.MemoSquare;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by sun on 2016-11-04.
 * ReceivedCookiesIntercepter를 먼저 읽으시오.
 * AddCookiesInterceptor: 수행되는 Request마다 SharedPreferences에서 쿠키를 가져와서 Header에 추가
 *
 * 테스트 시:
 * 방법1. getSharedPreferences함수를 사용하는 클래스를 상속받은 테스트용 클래스를 만들고, getSharedPreferences함수를 가짜로 구현하여 테스트
 * 방법2. 더 예쁘게 하려면 getSharedPreferences를 바로 접근해서 쓰지 말고, 유틸리티 함수를 만들어 거기서 받아서 쓴다. 테스트할때 그 유틸리티 객체를 Mock으로 만들어 테스트한다.
 */

public class AddCookiesInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();

        // Preference에서 cookies를 가져오는 작업을 수행
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MemoSquare.getContext());
        //HashSet<String> preferences = (HashSet<String>) sharedPreferences.getStringSet("SHARED_PREF_COOKIES", new HashSet<String>());
        String cookies = sharedPreferences.getString("SHARED_PREF_COOKIE", "");
        String csrftokenvalue = sharedPreferences.getString("CSFTTOKEN", "");

        builder.addHeader("COOKIE", cookies);
        builder.addHeader("X-CSRFTOKEN", csrftokenvalue);
        // Web,Android,iOS 구분을 위해 User-Agent세팅
        builder.removeHeader("User-Agent").addHeader("User-Agent", "Android");

        return chain.proceed(builder.build());
    }
}
