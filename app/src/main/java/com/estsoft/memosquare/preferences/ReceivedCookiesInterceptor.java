package com.estsoft.memosquare.preferences;

import android.preference.PreferenceManager;

import com.estsoft.memosquare.MemoSquare;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

/**
 * Created by sun on 2016-11-04.
 * 받식1: CookieStore-앱을 실행할 때마다 새로운 cookie생성. 매번 실행시마다 로그인해주어야 한다.
 *
 * 방식2: Interceptor-그래서 이걸 쓰겠다.
 *  1.로그인한뒤 받은 Response에서 쿠키정보를 가져온다.(ReceivedCookiesInterceptor)
 *  2.가져온 쿠키정보를 안드로이드의 SharedPreferences에 저장해둔다.(ReceivedCookiesInterceptor)
 *  3.이후 수행되는 Request마다 SharedPreferences에서 쿠키를 가져와서 Header에 추가해서 보낸다.
 *    (AddCookiesInterceptor)
 *
 * 테스트 시:
 * 방법1. getSharedPreferences함수를 사용하는 클래스를 상속받은 테스트용 클래스를 만들고, getSharedPreferences함수를 가짜로 구현하여 테스트
 * 방법2. 더 예쁘게 하려면 getSharedPreferences를 바로 접근해서 쓰지 말고, 유틸리티 함수를 만들어 거기서 받아서 쓴다. 테스트할때 그 유틸리티 객체를 Mock으로 만들어 테스트한다.
 */

public class ReceivedCookiesInterceptor implements Interceptor {

    String csrftokenvalue;

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response originalResponse = chain.proceed(chain.request());

        if (!originalResponse.headers("Set-Cookie").isEmpty()) {
            String cookie = "";
            for (String header : originalResponse.headers("Set-Cookie")) {
                cookie += header+";";
                if(header.contains("csrftoken")){
                    String[] SplitedBySemicolon = header.split("; ");
                    String[] SplitedByEquals = SplitedBySemicolon[0].split("=");
                    csrftokenvalue = SplitedByEquals[1];
                }
            }

            PreferenceManager.getDefaultSharedPreferences(MemoSquare.getContext()).edit()
                    .putString("SHARED_PREF_COOKIE",cookie)
                    .putString("CSFTTOKEN", csrftokenvalue)
                    .apply();
        }

        //Timber.d("57: "+originalResponse.headers());

        return originalResponse;
    }
}
