package com.estsoft.memosquare.preferences;

import android.content.Context;

import com.estsoft.memosquare.vo.UserVo;

/**
 * Created by sun on 2016-10-31.
 * Used for storing class object in sharedpreference
 */

public class UserPreferences {
    public static void setCurrentUser(UserVo currentUser, Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_prefs", 0);
        complexPreferences.putObject("current_user_value", currentUser);
        complexPreferences.commit();
    }

    public static UserVo getCurrentUser(Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_prefs", 0);
        UserVo currentUser = complexPreferences.getObject("current_user_value", UserVo.class);
        return currentUser;
    }

    public static void clearCurrentUser(Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_prefs", 0);
        complexPreferences.clearObject();
        complexPreferences.commit();
    }
}
