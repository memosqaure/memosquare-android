# Begin: Proguard rules for common

# 프로가드가 적용된 앱에서 에러시 익셉션이난 위치가 "Unknown Source"로 나올 경우 파일명과 라인수를 표시하는 옵션
-keepattributes SourceFile,LineNumberTable

-keep class com.estsoft.memosquare.vo.** {*;}
-keepclasseswithmember class com.estsoft.memosquare.vo.** {*;}

# 자바스크립트 인터페이스 난독화 안되도록
-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

# End: Proguard rules for common